/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import pt.pd.leilao.server.model.enums.ItemStateType;
import pt.pd.leilao.server.services.utils.Utils;

/**
 *
 * @author dfortunato
 */
public class ItemEntity implements Serializable, IEntityToString{
    
    private int id;
    private UserEntity userSeller;
    private UserEntity userBuyer;
    private CategoryEntity category;
    private float initialPrice;
    private float lastPrice;
    private float buyNowPrice;
    private long endDate;
    private ItemStateType itemStateType;
    private String description;
    private long lastUpdate;
    
    public ItemEntity()
    {
        id = -1;
        initialPrice = 0;
        buyNowPrice = 0;
        lastPrice = 0;
        endDate = 0;
        itemStateType = ItemStateType.ACTIVE_SELL;
        description = "";
        lastUpdate = Calendar.getInstance().getTimeInMillis();
    }
    
    public boolean isInitialDataValidToSell()
    {
        return userSeller != null && userBuyer == null && (buyNowPrice == 0 || buyNowPrice > initialPrice) && initialPrice > 0 && endDate > Calendar.getInstance().getTimeInMillis() && itemStateType == ItemStateType.ACTIVE_SELL;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserEntity getUserSeller() {
        return userSeller;
    }

    public void setUserSeller(UserEntity userSeller) {
        this.userSeller = userSeller;
    }

    public UserEntity getUserBuyer() {
        return userBuyer;
    }

    public String getUserBuyerUsername() {
        if (getUserBuyer() != null) 
        {
            return userBuyer.getUserName();
        }
        else
        {
            return "--";
        }
    }
    
    public void setUserBuyer(UserEntity userBuyer) {
        this.userBuyer = userBuyer;
    }

    public CategoryEntity getCategory() {
        return category;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
    }

    public float getInitialPrice() {
        return initialPrice;
    }
    
    public String getInitialPriceMoney()
    {
        return Utils.getValorEuros(getInitialPrice());
    }

    public void setInitialPrice(float initialPrice) {
        this.initialPrice = initialPrice;
    }

    public float getBuyNowPrice() {
        return buyNowPrice;
    }
    
    public String getBuyNowPriceMoney()
    {
        if (getBuyNowPrice() > 0) 
        {
            return Utils.getValorEuros(getBuyNowPrice());
        }
        else
        {
            return "--";
        }
    }

    public void setBuyNowPrice(float buyNowPrice) {
        this.buyNowPrice = buyNowPrice;
    }
    
    public boolean isBuyNowAllowed()
    {
        return getBuyNowPrice() > 0;
    }

    public ItemStateType getItemStateType() {
        return itemStateType;
    }

    public void setItemStateType(ItemStateType itemStateType) {
        this.itemStateType = itemStateType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getEndDate() {
        return endDate;
    }
    
    public boolean isDateValid()
    {
        return endDate >= Calendar.getInstance().getTimeInMillis();
    }
    
    public String getEndDateString() {
        return Utils.getDefaultStringDate(new Date(endDate));
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public void setEndDateInMinutes(int minutes) {
        this.endDate = Calendar.getInstance().getTimeInMillis() + (minutes * 60000);
    }
    
    public long getLastUpdate() {
        return lastUpdate;
    }
    
    public String getLastUpdateString() {
        return Utils.getDefaultStringDate(new Date(getLastUpdate()));
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public float getLastPrice() {
        return lastPrice;
    }

    public String getLastPriceMoney()
    {
        return Utils.getValorEuros(getLastPrice());
    }
    
    public void setLastPrice(float lastPrice) {
        this.lastPrice = lastPrice;
    }

    public boolean isItemSold()
    {
        return getUserBuyer() != null && (getItemStateType() == ItemStateType.SOLD || getItemStateType() == ItemStateType.SOLD_BUYNOW);
    }
    
    public float getMinimumBidValue()
    {
        if (getLastPrice() > 0) {
            return (getLastPrice() + 1);
        }
        else
        {
            return getInitialPrice();
        }
    }
    
    public String generateListLineDescriptionGuest() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Descricao: " + getDescription())
                .append(" | Vendido por: " + Utils.getValorEuros(getLastPrice()))
                .append(" | Data da venda: " + Utils.getDefaultStringDate(new Date(getLastUpdate())));
        
        return stringBuilder.toString();
    }
    
    @Override
    public String generateListLineDescription() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Id: " + getId())
                .append(" | Vendedor: " + getUserSeller().getUserName())
                .append(getUserBuyer() != null ? " | Comprador: " + getUserBuyer().getUserName() : "")
                .append(" | Categoria: " + getCategory().getName())
                .append(" | Preco Inicial: " + Utils.getValorEuros(getInitialPrice()))
                .append(" | Valor Ultima Licitacao: " + Utils.getValorEuros(getLastPrice()))
                .append(getBuyNowPrice() > 0 ? " | Preco Compra Ja: " + Utils.getValorEuros(getBuyNowPrice()) : "")
                .append(" | Data Final: " + Utils.getDefaultStringDate(new Date(getEndDate())))
                .append(" | Ultima Actualizacao: " + Utils.getDefaultStringDate(new Date(getLastUpdate())))
                .append(" | Descricao: " + getDescription());
        
        return stringBuilder.toString();
    }

    @Override
    public String generateEntityDetailsDescription() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Id: " + getId())
                .append("\nVendedor: " + getUserSeller().getUserName())
                .append(getUserBuyer() != null ? "\nComprador: " + getUserBuyer().getUserName() : "")
                .append("\nCategoria: " + getCategory().getName())
                .append("\nPreco Inicial: " + Utils.getValorEuros(getInitialPrice()))
                .append("\nValor Ultima Licitacao: " + Utils.getValorEuros(getLastPrice()))
                .append(getBuyNowPrice() > 0 ? "\nPreco Compra Ja: " + Utils.getValorEuros(getBuyNowPrice()) : "")
                .append("\nData Final: " + Utils.getDefaultStringDate(new Date(getEndDate())))
                .append("\nUltima Actualizacao: " + Utils.getDefaultStringDate(new Date(getLastUpdate())))
                .append("\nEstado: " + getItemStateType().name())
                .append("\nDescricao: " + getDescription());
        
        return stringBuilder.toString();
    }
    
    public static final Comparator<ItemEntity> COMPARATOR_BY_CATEGORY = new Comparator<ItemEntity>() {
        @Override
        public int compare(ItemEntity o1, ItemEntity o2) {
            return o1.getCategory().getName().compareTo(o2.getCategory().getName());
        }
    };
    
    public static final Comparator<ItemEntity> COMPARATOR_BY_DESCRIPTION = new Comparator<ItemEntity>() {
        @Override
        public int compare(ItemEntity o1, ItemEntity o2) {
            return o1.getDescription().compareTo(o2.getDescription());
        }
    };
    
    public static final Comparator<ItemEntity> COMPARATOR_BY_PRICE = new Comparator<ItemEntity>() {
        @Override
        public int compare(ItemEntity o1, ItemEntity o2) {
            return (int) (o1.getInitialPrice() - o2.getInitialPrice());
        }
    };
    
    public static final Comparator<ItemEntity> COMPARATOR_BY_BUYNOW_PRICE = new Comparator<ItemEntity>() {
        @Override
        public int compare(ItemEntity o1, ItemEntity o2) {
            return (int) (o1.getBuyNowPrice()- o2.getBuyNowPrice());
        }
    };
    
    public static final Comparator<ItemEntity> COMPARATOR_BY_ENDDATE = new Comparator<ItemEntity>() {
        @Override
        public int compare(ItemEntity o1, ItemEntity o2) {
            return (int) (o1.getEndDate() - o2.getEndDate());
        }
    };
    
    public static final Comparator<ItemEntity> COMPARATOR_BY_LASTUPDATED = new Comparator<ItemEntity>() {
        @Override
        public int compare(ItemEntity o1, ItemEntity o2) {
            return (int) (o2.getLastUpdate() - o1.getLastUpdate());
        }
    };
    
    public static final Comparator<ItemEntity> COMPARATOR_BY_ID = new Comparator<ItemEntity>() {
        @Override
        public int compare(ItemEntity o1, ItemEntity o2) {
            return (int) (o1.getId() - o2.getId());
        }
    };
    
}
