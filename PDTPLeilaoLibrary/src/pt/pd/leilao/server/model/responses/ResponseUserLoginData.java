/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model.responses;

import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.model.enums.responses.ResponseUserLoginType;

/**
 *
 * @author dfortunato
 */
public class ResponseUserLoginData extends ResponseBaseData{
    
    private UserEntity userData;
    private ResponseUserLoginType responseUserLoginType;

    
    public ResponseUserLoginData() {
    }

    public UserEntity getUserData() {
        return userData;
    }

    public void setUserData(UserEntity userData) {
        this.userData = userData;
    }

    public ResponseUserLoginType getResponseUserLoginType() {
        return responseUserLoginType;
    }

    public void setResponseUserLoginType(ResponseUserLoginType responseUserLoginType) {
        this.responseUserLoginType = responseUserLoginType;
    }
    
    
    
}
