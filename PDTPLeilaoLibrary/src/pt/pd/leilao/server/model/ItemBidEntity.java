/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import pt.pd.leilao.server.services.utils.Utils;

/**
 *
 * @author dfortunato
 */
public class ItemBidEntity implements Serializable{
    
    private int id;
    private ItemEntity itemTarget;
    private UserEntity userBuyer;
    private float valueBid;
    private long bidDate;

    public ItemBidEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ItemEntity getItemTarget() {
        return itemTarget;
    }

    public void setItemTarget(ItemEntity itemTarget) {
        this.itemTarget = itemTarget;
    }

    public UserEntity getUserBuyer() {
        return userBuyer;
    }

    public void setUserBuyer(UserEntity userBuyer) {
        this.userBuyer = userBuyer;
    }

    public float getValueBid() {
        return valueBid;
    }
    
    public String getValueBidMoney()
    {
        return Utils.getValorEuros(getValueBid());
    }

    public boolean isSoldBuyNow()
    {
        return getItemTarget().isBuyNowAllowed() && getValueBid() >= getItemTarget().getBuyNowPrice();
    }
    
    public void setValueBid(float valueBid) {
        this.valueBid = valueBid;
    }

    public long getBidDate() {
        return bidDate;
    }

    public void setBidDate(long bidDate) {
        this.bidDate = bidDate;
    }
    
    public String getBidDateFormatted()
    {
        return Utils.getDefaultStringDate(new Date(getBidDate()));
    }
    
}
