/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model;

/**
 *
 * @author dfortunato
 */
public class TransactionEntity {
    private int id;
    private UserEntity userReceiver;
    private UserEntity userSender;
    private ItemEntity itemSold;
    private float value;
    private long date;

    public TransactionEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserEntity getUserReceiver() {
        return userReceiver;
    }

    public void setUserReceiver(UserEntity userReceiver) {
        this.userReceiver = userReceiver;
    }

    public UserEntity getUserSender() {
        return userSender;
    }

    public void setUserSender(UserEntity userSender) {
        this.userSender = userSender;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public ItemEntity getItemSold() {
        return itemSold;
    }

    public void setItemSold(ItemEntity itemSold) {
        this.itemSold = itemSold;
    }
    
}
