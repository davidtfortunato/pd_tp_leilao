/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import pt.pd.leilao.server.services.utils.Utils;

/**
 *
 * @author dfortunato
 */
public class UserMessageEntity implements Serializable, IEntityToString{
    
    private int id;
    private UserEntity userSender;
    private UserEntity userReceive;
    private String message;
    private long dateSent;

    public UserMessageEntity() {
        this.dateSent = Calendar.getInstance().getTimeInMillis();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserEntity getUserSender() {
        return userSender;
    }

    public void setUserSender(UserEntity userSender) {
        this.userSender = userSender;
    }

    public UserEntity getUserReceive() {
        return userReceive;
    }

    public void setUserReceive(UserEntity userReceive) {
        this.userReceive = userReceive;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getTimeSent() {
        return dateSent;
    }
    
    public String getTimeSentFormatted() {
        return Utils.getDefaultStringDate(new Date(getTimeSent()));
    }
    
    public void setTimeSent(long timeSent) {
        this.dateSent = timeSent;
    }
    
    public static UserMessageEntity generateNewMessage(UserEntity userSender, UserEntity userReceive, String message)
    {
        if (userReceive != null && userSender != null && message != null && !message.isEmpty()) {
            UserMessageEntity userMessageEntity = new UserMessageEntity();
            userMessageEntity.setUserSender(userSender);
            userMessageEntity.setUserReceive(userReceive);
            userMessageEntity.setMessage(message);
            return userMessageEntity;
        }
        else
        {
            return null;
        }
        
    }
    
    @Override
    public String generateListLineDescription() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ID: " + getId())
                .append(" | De: " + getUserSender().getUserName())
                .append(" | Para: " + getUserReceive().getUserName())
                .append(" | Data: " + Utils.getDefaultStringDate(new Date(getTimeSent())))
                .append(" | Mensagem: " + getMessage());
        
        return stringBuilder.toString();
    }

    @Override
    public String generateEntityDetailsDescription() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ID: " + getId())
                .append("\nDe: " + getUserSender().getUserName())
                .append("\nPara: " + getUserReceive().getUserName())
                .append("\nData: " + Utils.getDefaultStringDate(new Date(getTimeSent())))
                .append("\nMensagem: " + getMessage());
        
        return stringBuilder.toString();
    }
    
    
    
}
