/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model;

import java.io.Serializable;

/**
 *
 * @author dfortunato
 */
public class ResponseService<T extends Object> implements Serializable{
    
    public static final int RESPONSE_CODE_SUCCESS = 1;
    public static final int RESPONSE_CODE_NOT_FOUND = -2;
    public static final int RESPONSE_CODE_INVALID_DATA = -3;
    public static final int RESPONSE_CODE_ERROR = -4;
    public static final int RESPONSE_CODE_ERROR_NO_PERMISSIONS = -5;
    public static final int RESPONSE_CODE_ERROR_ALREADY_EXISTS= -6;
    public static final int RESPONSE_CODE_ERROR_NOT_LOGGEDIN= -7;
    public static final int RESPONSE_CODE_ERROR_MONEY_NOT_AVAILABLE= -8;
    public static final int RESPONSE_CODE_ERROR_EXPIRED= -9;
    public static final int RESPONSE_CODE_NOT_GENERATED = 0;
    
    private T dataObject;
    private int responseCode;
    
    public ResponseService()
    {
        responseCode = RESPONSE_CODE_SUCCESS;
    }

    public ResponseService(boolean isSuccess)
    {
        setSuccess(isSuccess);
        this.dataObject = null;
    }
    
    public ResponseService(int responseCode)
    {
        this.responseCode = responseCode;
        this.dataObject = null;
    }
    
    public ResponseService(T dataObject, boolean isSuccess)
    {
        setSuccess(isSuccess);
        this.dataObject = dataObject;
    }
    
    public ResponseService(T dataObject, int responseCode)
    {
        this.responseCode = responseCode;
        this.dataObject = dataObject;
    }
    
    public ResponseService(T dataObject)
    {
        this.dataObject = dataObject;
        this.responseCode = (dataObject != null) ? RESPONSE_CODE_SUCCESS : RESPONSE_CODE_NOT_FOUND;
    }
    
    public T getDataObject() {
        return dataObject;
    }

    public void setDataObject(T dataObject) {
        this.dataObject = dataObject;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }
    
    public String getResponseMessage()
    {
        switch(getResponseCode())
        {
            case RESPONSE_CODE_ERROR:
                return "RESPONSE_CODE_ERROR";
            case RESPONSE_CODE_ERROR_ALREADY_EXISTS:
                return "RESPONSE_CODE_ERROR_ALREADY_EXISTS";
            case RESPONSE_CODE_ERROR_NOT_LOGGEDIN:
                return "RESPONSE_CODE_ERROR_NOT_LOGGEDIN";
            case RESPONSE_CODE_ERROR_NO_PERMISSIONS:
                return "RESPONSE_CODE_ERROR_NO_PERMISSIONS";
            case RESPONSE_CODE_INVALID_DATA:
                return "RESPONSE_CODE_INVALID_DATA";
            case RESPONSE_CODE_NOT_FOUND:
                return "RESPONSE_CODE_NOT_FOUND";
            case RESPONSE_CODE_NOT_GENERATED:
                return "RESPONSE_CODE_NOT_GENERATED";
            case RESPONSE_CODE_SUCCESS:
                return "RESPONSE_CODE_SUCCESS";
            case RESPONSE_CODE_ERROR_MONEY_NOT_AVAILABLE:
                return "RESPONSE_CODE_ERROR_MONEY_NOT_AVAILABLE";
            case RESPONSE_CODE_ERROR_EXPIRED:
                return "RESPONSE_CODE_ERROR_EXPIRED";
        }
        return "UNKNOWN_ERROR";
    }
    
    public void setSuccess(boolean isSuccess)
    {
        if (isSuccess) {
            setResponseCode(RESPONSE_CODE_SUCCESS);
        }
        else
        {
            setResponseCode(RESPONSE_CODE_ERROR);
        }
    }
    
    public boolean isSuccess()
    {
        return responseCode > 0;
    }
    
}
