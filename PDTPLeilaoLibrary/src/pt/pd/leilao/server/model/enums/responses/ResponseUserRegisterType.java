/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model.enums.responses;

/**
 *
 * @author dfortunato
 */
public enum ResponseUserRegisterType {
    USER_REGISTERED, USERNAME_ALREADY_EXISTS, USER_REGISTER_PENDING_APPROVAL, USERDATA_NOT_VALID;
    
    
}
