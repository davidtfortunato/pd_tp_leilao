/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model.enums;

/**
 *
 * @author dfortunato
 */
public enum AdminOrderResponseType {
    PENDING("pending"), DECLINED("declined"), APPROVED("approved");
    
    private String typeValue;

    private AdminOrderResponseType(String typeValue) {
        this.typeValue = typeValue;
    }

    public String getTypeValue() {
        return typeValue;
    }

    public void setTypeValue(String typeValue) {
        this.typeValue = typeValue;
    }
    
    public static AdminOrderResponseType getType(String typeValue)
    {
        if (typeValue.equals(PENDING.getTypeValue())) {
            return PENDING;
        } else if (typeValue.equals(DECLINED.getTypeValue())) {
            return DECLINED;
        } else if (APPROVED.getTypeValue().equals(typeValue)) {
            return APPROVED;
        }
        return PENDING;
    }
    
}
