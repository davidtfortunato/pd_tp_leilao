/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model.enums;

/**
 *
 * @author dfortunato
 */
public enum AdminOrderType {
    REGISTER("register"), REACTIVATION("reactivation"), USER_SUSPEND("user_suspend"), UNKNOWN("unknown");
    
    private String typeValue;

    private AdminOrderType(String typeValue) {
        this.typeValue = typeValue;
    }

    public String getTypeValue() {
        return typeValue;
    }

    public void setTypeValue(String typeValue) {
        this.typeValue = typeValue;
    }
    
    public static AdminOrderType getType(String typeValue)
    {
        if (typeValue.equals(REGISTER.getTypeValue())) {
            return REGISTER;
        } else if (typeValue.equals(REACTIVATION.getTypeValue())) {
            return REACTIVATION;
        } else if (typeValue.equals(USER_SUSPEND.getTypeValue())) {
            return USER_SUSPEND;
        }
        return UNKNOWN;
    }
    
}
