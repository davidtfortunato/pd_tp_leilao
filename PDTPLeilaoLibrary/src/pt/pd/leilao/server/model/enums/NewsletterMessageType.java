/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model.enums;

/**
 *
 * @author dfortunato
 */
public enum NewsletterMessageType {
    USER_APPROVED("user_approved"),
    USER_NOT_APPROVED("user_not_approved"),
    USER_SUSPENDED_BY_ADMIN("user_suspended_by_admin"), 
    USER_SUSPENDED_BY_USER("user_suspended_by_user"), 
    USER_REACTIVATED("user_reactivated"),
    ITEM_ADDED_NEW_ITEM("item_added_new_item"),
    ITEM_CANCELLED_BY_ADMIN("item_cancelled_by_admin"),
    ITEM_SOLD("item_bought_sold"),
    UNKNOWN("unknown");

    private String typeValue;

    private NewsletterMessageType(String typeValue) {
        this.typeValue = typeValue;
    }

    public String getTypeValue() {
        return typeValue;
    }

    public void setTypeValue(String typeValue) {
        this.typeValue = typeValue;
    }

    public static NewsletterMessageType getType(String typeValue) {
        if (typeValue.equals(ITEM_ADDED_NEW_ITEM.getTypeValue())) {
            return ITEM_ADDED_NEW_ITEM;
        } else if (typeValue.equals(ITEM_SOLD.getTypeValue())) {
            return ITEM_SOLD;
        } else if (typeValue.equals(ITEM_CANCELLED_BY_ADMIN.getTypeValue())) {
            return ITEM_CANCELLED_BY_ADMIN;
        } else if (typeValue.equals(USER_APPROVED.getTypeValue())) {
            return USER_APPROVED;
        } else if (typeValue.equals(USER_NOT_APPROVED.getTypeValue())) {
            return USER_NOT_APPROVED;
        } else if (typeValue.equals(USER_REACTIVATED.getTypeValue())) {
            return USER_REACTIVATED;
        } else if (typeValue.equals(USER_SUSPENDED_BY_ADMIN.getTypeValue())) {
            return USER_SUSPENDED_BY_ADMIN;
        } else if (typeValue.equals(USER_SUSPENDED_BY_USER.getTypeValue())) {
            return USER_SUSPENDED_BY_USER;
        } 
        return UNKNOWN;
    }

}
