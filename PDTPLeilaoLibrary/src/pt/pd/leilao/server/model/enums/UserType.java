/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model.enums;

/**
 *
 * @author dfortunato
 */
public enum UserType {
    REGISTERED("registered"), GUEST("guest"), ADMIN("admin"), UNKNOWN("unknown");
    
    private String typeValue;
    
    UserType(String typeValue)
    {
        this.typeValue = typeValue;
    }

    public String getTypeValue() {
        return typeValue;
    }

    public void setTypeValue(String typeValue) {
        this.typeValue = typeValue;
    }
    
    public static UserType getType(String typeValue)
    {
        if (typeValue.equals(REGISTERED.getTypeValue())) {
            return REGISTERED;
        } else if (typeValue.equals(GUEST.getTypeValue()))
        {
            return GUEST;
        } else if(typeValue.equals(ADMIN.getTypeValue()))
        {
            return ADMIN;
        }
        return UNKNOWN;
    }
    
}
