/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model.enums;

/**
 *
 * @author dfortunato
 */
public enum ReportStateType {
    ACCEPTED("accepted"), REJECTED("rejected"), PENDING("pending"), UNKNOWN("unknown");
    
    private String typeValue;

    private ReportStateType(String typeValue) {
        this.typeValue = typeValue;
    }

    public String getTypeValue() {
        return typeValue;
    }
    
    public static ReportStateType getType(String typeValue)
    {
        if (typeValue != null) {
            if (typeValue.equals(ACCEPTED.getTypeValue())) {
                return ACCEPTED;
            } else if (typeValue.equals(REJECTED.getTypeValue())) {
                return REJECTED;
            } else if (typeValue.equals(PENDING.getTypeValue())) {
                return PENDING;
            }
        }
        return UNKNOWN;
    }
    
}
