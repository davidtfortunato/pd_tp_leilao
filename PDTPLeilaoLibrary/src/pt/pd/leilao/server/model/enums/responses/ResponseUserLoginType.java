/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model.enums.responses;

/**
 *
 * @author dfortunato
 */
public enum ResponseUserLoginType {
    LOGIN_SUCCESS, FAILED_USERNAME_NOT_FOUND, FAILED_PASSWORD_WRONG, FAILED_NOT_ACTIVE, ALREADY_LOGGED_IN;
}
