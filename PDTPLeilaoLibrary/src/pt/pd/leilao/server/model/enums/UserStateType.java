/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model.enums;

/**
 *
 * @author dfortunato
 */
public enum UserStateType {
    ACTIVE("active"), SUSPENDED_USER("suspended_user"), SUSPENDED_ADMIN("suspended_admin"), PENDING_APPROVE("pending_approve"), UNKNOWN("unknown");

    private String typeValue;

    UserStateType(String typeValue) {
        this.typeValue = typeValue;
    }

    public String getTypeValue() {
        return typeValue;
    }

    public void setTypeValue(String typeValue) {
        this.typeValue = typeValue;
    }

    public static UserStateType getType(String typeValue) {
        if (typeValue.equalsIgnoreCase(ACTIVE.getTypeValue())) {
            return ACTIVE;
        } else if (typeValue.equals(SUSPENDED_USER.getTypeValue())) {
            return SUSPENDED_USER;
        } else if (typeValue.equals(SUSPENDED_ADMIN.getTypeValue())) {
            return SUSPENDED_ADMIN;
        } else if (typeValue.equals(PENDING_APPROVE.getTypeValue())) {
            return PENDING_APPROVE;
        }
        return UNKNOWN;
    }

}
