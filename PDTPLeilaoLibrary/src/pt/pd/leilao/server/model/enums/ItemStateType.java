/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model.enums;

/**
 *
 * @author dfortunato
 */
public enum ItemStateType {
    ACTIVE_SELL("active_sell"), CANCELLED("cancelled"), SOLD("sold"), SOLD_BUYNOW("sold_buynow"), DATE_EXPIRED("date_expired"), UNKNOWN("unknown");

    private String typeValue;

    ItemStateType(String typeValue) {
        this.typeValue = typeValue;
    }

    public String getTypeValue() {
        return typeValue;
    }

    public void setTypeValue(String typeValue) {
        this.typeValue = typeValue;
    }

    public static ItemStateType getType(String typeValue) {
        if (typeValue.equals(ACTIVE_SELL.getTypeValue())) {
            return ACTIVE_SELL;
        } else if (typeValue.equals(CANCELLED.getTypeValue())) {
            return CANCELLED;
        } else if (typeValue.equals(SOLD.getTypeValue())) {
            return SOLD;
        } else if (typeValue.equals(SOLD_BUYNOW.getTypeValue())) {
            return SOLD_BUYNOW;
        } else if (typeValue.equals(DATE_EXPIRED.getTypeValue())) {
            return DATE_EXPIRED;
        }
        return UNKNOWN;
    }

}
