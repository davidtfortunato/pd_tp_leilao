/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model;

import java.io.Serializable;
import java.util.List;
import pt.pd.leilao.server.model.enums.UserStateType;
import pt.pd.leilao.server.model.enums.UserType;
import pt.pd.leilao.server.services.utils.Utils;

/**
 *
 * @author dfortunato
 */
public class UserEntity implements Serializable, IEntityToString {

    private int id;
    private String userName;
    private String password;
    private String name;
    private String address;
    private float balance;
    private UserStateType userStateType;
    private UserType userType;
    private String suspensionReason;

    public UserEntity() {
        this.balance = 0;
        this.userStateType = UserStateType.UNKNOWN;
        this.userType = UserType.GUEST;
        this.id = -1;
        this.name = "";
        this.address = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNome() {
        return name;
    }

    public void setNome(String nome) {
        this.name = nome;
    }

    public String getMorada() {
        return address;
    }

    public void setMorada(String morada) {
        this.address = morada;
    }

    public float getSaldo() {
        return balance;
    }

    public float appendSaldo(float value)
    {
        this.balance += value;
        return this.balance;
    }
    
    public String getSaldoString() {
        return Utils.getValorEuros(getSaldo());
    }

    public void setSaldo(float saldo) {
        this.balance = saldo;
    }

    public UserStateType getUserStateType() {
        return userStateType;
    }
    
    public boolean isUserStateSuspended()
    {
        return getUserStateType() != UserStateType.ACTIVE; 
    }

    public void setUserStateType(UserStateType userStateType) {
        this.userStateType = userStateType;
        if (userStateType == UserStateType.ACTIVE) {
            setSuspensionReason(null);
        }
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSuspensionReason() {
        return suspensionReason;
    }

    public void setSuspensionReason(String suspensionReason) {
        this.suspensionReason = suspensionReason;
    }

    public boolean isUserLoggedIn() {
        return isUserDataValid() && userType != UserType.GUEST && userStateType == UserStateType.ACTIVE;
    }

    public boolean isUserDataValid() {
        return userType != null && userStateType != null && !Utils.isStringOnlySpacesOrEmpty(userName) && !Utils.isStringOnlySpacesOrEmpty(password);
    }

    public boolean validateCredentials(String password) {
        return password.equals(this.password);
    }

    @Override
    public String generateListLineDescription() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ID: " + getId())
                .append(" | UserName: " + getUserName())
                .append(" | Nome: " + getNome())
                .append(" | Morada: " + getMorada())
                .append(" | Saldo: " + getSaldoString())
                .append(" | Estado: " + getUserStateType().name())
                .append((!Utils.isStringOnlySpacesOrEmpty(getSuspensionReason()) && getUserStateType() != UserStateType.ACTIVE) ? " | Motivo da Suspensao: " + getSuspensionReason() : "");

        return stringBuilder.toString();
    }

    @Override
    public String generateEntityDetailsDescription() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ID: " + getId())
                .append("\nUserName: " + getUserName())
                .append("\nNome: " + getNome())
                .append("\nMorada: " + getMorada())
                .append("\nSaldo: " + getSaldoString())
                .append("\nUserType: " + getUserType().name())
                .append("\nUser Status: " + getUserStateType().name())
                .append((!Utils.isStringOnlySpacesOrEmpty(getSuspensionReason()) && getUserStateType() != UserStateType.ACTIVE) ? "\nMotivo da Suspensao: " + getSuspensionReason() : "");

        return stringBuilder.toString();
    }

}
