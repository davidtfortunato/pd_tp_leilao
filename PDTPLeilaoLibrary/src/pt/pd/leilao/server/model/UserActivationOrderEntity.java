/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model;

import java.io.Serializable;
import pt.pd.leilao.server.model.enums.AdminOrderResponseType;
import pt.pd.leilao.server.model.enums.AdminOrderType;

/**
 *  Dados utilizados pelo admin para gerir os pedidos de registo ou reactivacao ao administrador
 * @author dfortunato
 */
public class UserActivationOrderEntity implements Serializable, IEntityToString{
    
    private int id;
    private AdminOrderType adminOrderType;
    private AdminOrderResponseType adminOrderResponseType;
    private String description;
    private UserEntity targetUser;

    public UserActivationOrderEntity() {
        adminOrderResponseType = AdminOrderResponseType.PENDING;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public AdminOrderType getAdminOrderType() {
        return adminOrderType;
    }

    public void setAdminOrderType(AdminOrderType adminOrderType) {
        this.adminOrderType = adminOrderType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UserEntity getTargetUser() {
        return targetUser;
    }

    public void setTargetUser(UserEntity targetUser) {
        this.targetUser = targetUser;
    }

    public AdminOrderResponseType getAdminOrderResponseType() {
        return adminOrderResponseType;
    }

    public void setAdminOrderResponseType(AdminOrderResponseType adminOrderResponseType) {
        this.adminOrderResponseType = adminOrderResponseType;
    }

    @Override
    public String generateListLineDescription() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ID: " + getId())
                .append(" | Order Type: " + getAdminOrderType().name())
                .append(" | UserName: " + getTargetUser().getUserName())
                .append(" | Description: " + getDescription())
                .append(" | Admin Response: " + getAdminOrderResponseType().name());
        
        return stringBuilder.toString();
    }

    @Override
    public String generateEntityDetailsDescription() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ID: " + getId())
                .append("\nOrder Type: " + getAdminOrderType().name())
                .append("\nUserName: " + getTargetUser().getUserName())
                .append("\nDescription: " + getDescription())
                .append("\nAdmin Response: " + getAdminOrderResponseType().name());
        
        return stringBuilder.toString();
    }
    
    public static UserActivationOrderEntity generateOrder(AdminOrderType adminOrderType, UserEntity userTarget, String description)
    {
        UserActivationOrderEntity order = new UserActivationOrderEntity();
        order.setAdminOrderResponseType(AdminOrderResponseType.PENDING);
        order.setAdminOrderType(adminOrderType);
        order.setDescription(description);
        order.setTargetUser(userTarget);
        return order;
    }
}
