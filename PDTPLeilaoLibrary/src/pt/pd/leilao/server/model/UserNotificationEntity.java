/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model;

import java.util.Date;
import pt.pd.leilao.server.services.utils.Utils;

/**
 *
 * @author dfortunato
 */
public class UserNotificationEntity {
    
    private int id;
    private UserEntity userTargetNotified;
    private ItemEntity itemReference;
    private String message;
    private long dateSend;

    public UserNotificationEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserEntity getUserTargetNotified() {
        return userTargetNotified;
    }

    public void setUserTargetNotified(UserEntity userTargetNotified) {
        this.userTargetNotified = userTargetNotified;
    }

    public ItemEntity getItemReference() {
        return itemReference;
    }

    public void setItemReference(ItemEntity itemReference) {
        this.itemReference = itemReference;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getDateSend() {
        return dateSend;
    }

    public void setDateSend(long dateSend) {
        this.dateSend = dateSend;
    }
    
    public String getDateSendFormatted()
    {
        return Utils.getDefaultStringDate(new Date(getDateSend()));
    }
    
}
