/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model;

/**
 *
 * @author dfortunato
 */
public interface IEntityToString {
    
    public String generateListLineDescription();
    
    public String generateEntityDetailsDescription();
    
}
