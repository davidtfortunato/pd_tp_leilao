/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model;

import java.io.Serializable;
import pt.pd.leilao.server.model.enums.ReportStateType;

/**
 *
 * @author dfortunato
 */
public class ReportItemEntity implements Serializable{
    private int id;
    private ItemEntity idItemTarget;
    private UserEntity idUserSentReport;
    private String message;
    private ReportStateType currentStateType;
    
    public ReportItemEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserEntity getIdUserSentReport() {
        return idUserSentReport;
    }

    public void setIdUserSentReport(UserEntity idUserSentReport) {
        this.idUserSentReport = idUserSentReport;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ItemEntity getIdItemTarget() {
        return idItemTarget;
    }

    public void setIdItemTarget(ItemEntity idItemTarget) {
        this.idItemTarget = idItemTarget;
    }

    public ReportStateType getCurrentStateType() {
        return currentStateType;
    }

    public void setCurrentStateType(ReportStateType currentStateType) {
        this.currentStateType = currentStateType;
    }
}
