/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import pt.pd.leilao.server.model.enums.NewsletterMessageType;

/**
 *
 * @author dfortunato
 */
public class NewsletterMessageEntity implements Serializable, IEntityToString{
    
    private static final SimpleDateFormat DATE_FORMAT_DATE_CREATED = new SimpleDateFormat("HH:mm dd/MM/yyyy");
    
    private int id;
    private NewsletterMessageType newsletterMessageType;
    private String message;
    private int targetId;
    private long dateCreated;
    
    public NewsletterMessageEntity()
    {
        dateCreated = Calendar.getInstance().getTimeInMillis();
        newsletterMessageType = NewsletterMessageType.UNKNOWN;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public NewsletterMessageType getNewsletterMessageType() {
        return newsletterMessageType;
    }

    public void setNewsletterMessageType(NewsletterMessageType newsletterMessageType) {
        this.newsletterMessageType = newsletterMessageType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getTargetId() {
        return targetId;
    }

    public void setTargetId(int targetId) {
        this.targetId = targetId;
    }

    public long getTimeCreated() {
        return dateCreated;
    }

    public void setTimeCreated(long timeCreated) {
        this.dateCreated = timeCreated;
    }
    
    public String getTimeCreatedString()
    {
        return DATE_FORMAT_DATE_CREATED.format(new Date(getTimeCreated()));
    }
    
    @Override
    public String generateListLineDescription() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("#" + getId())
                .append(" | " + getTimeCreatedString())
                .append(" | " + getMessage());
        
        return stringBuilder.toString();
    }

    @Override
    public String generateEntityDetailsDescription() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("#" + getId())
                .append("\nCriada em: " + getTimeCreatedString())
                .append("\nMensagem: " + getMessage())
                .append("\nTipo Newsletter: " + getNewsletterMessageType().name())
                .append("\nId Target: " + getTargetId());
        
        return stringBuilder.toString();
    }
    
}
