/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model;

import java.io.Serializable;
import pt.pd.leilao.server.model.enums.ReportStateType;

/**
 *
 * @author dfortunato
 */
public class ReportUserEntity implements Serializable{
    private int id;
    private UserEntity idUserTarget;
    private UserEntity idUserSentReport;
    private String message;
    private ReportStateType currentStateType;

    public ReportUserEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserEntity getIdUserTarget() {
        return idUserTarget;
    }

    public void setIdUserTarget(UserEntity idUserTarget) {
        this.idUserTarget = idUserTarget;
    }

    public UserEntity getIdUserSentReport() {
        return idUserSentReport;
    }

    public void setIdUserSentReport(UserEntity idUserSentReport) {
        this.idUserSentReport = idUserSentReport;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ReportStateType getCurrentStateType() {
        return currentStateType;
    }

    public void setCurrentStateType(ReportStateType currentStateType) {
        this.currentStateType = currentStateType;
    }
    
}
