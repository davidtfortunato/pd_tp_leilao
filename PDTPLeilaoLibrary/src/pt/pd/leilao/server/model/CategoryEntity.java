/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import pt.pd.leilao.server.services.utils.Utils;

/**
 *
 * @author dfortunato
 */
public class CategoryEntity implements Serializable, IEntityToString{
    
    private int id;
    private String name;
    
    public CategoryEntity()
    {
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public String generateListLineDescription() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Id: " + getId())
                .append(" | Nome: " + getName());
        
        return stringBuilder.toString();
    }
    
    @Override
    public String generateEntityDetailsDescription() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Id: " + getId())
                .append("\nNome: " + getName());
        
        return stringBuilder.toString();
    }
    
}
