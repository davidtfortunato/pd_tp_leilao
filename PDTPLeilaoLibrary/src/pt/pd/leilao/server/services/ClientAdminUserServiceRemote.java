/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services;

import java.util.List;
import javax.ejb.Remote;
import pt.pd.leilao.server.model.CategoryEntity;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ReportItemEntity;
import pt.pd.leilao.server.model.ReportUserEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.UserActivationOrderEntity;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.model.enums.AdminOrderResponseType;
import pt.pd.leilao.server.model.enums.ItemStateType;
import pt.pd.leilao.server.model.enums.ReportStateType;

/**
 *
 * @author dfortunato
 */
@Remote
public interface ClientAdminUserServiceRemote {
    
    public ResponseService<List<UserActivationOrderEntity>> getListUserActivationOrderEntity();
    
    public ResponseService replyUserActivationOrder(int orderId, AdminOrderResponseType adminOrderResponseType);
    
    public ResponseService<List<UserEntity>> getListUsers();
    
    public ResponseService suspendUser(String username, String reasonSuspension);
    
    public ResponseService reactivateUser(String username);
    
    public ResponseService<UserEntity> findUser(String userName);
    
    public ResponseService<List<ItemEntity>> getListItems();
    
    public ResponseService<ItemEntity> findItemById(int itemId);
    
    public ResponseService updateItemState(int itemId, ItemStateType itemStateType);
    
    public ResponseService cancelItem(int itemId);
    
    public ResponseService<List<CategoryEntity>> getListCategories();
    
    public ResponseService addCategory(String categoryName);
    
    public ResponseService changeCategoryName(int categoryId, String newCategoryName);
    
    public List<ReportItemEntity> getListItemsReport();
    
    public List<ReportUserEntity> getListUsersReport();
    
    public ResponseService replyUserReport(int reportId, ReportStateType newReportStateType);
    
    public ResponseService replyItemReport(int reportId, ReportStateType newReportStateType);
    
}
