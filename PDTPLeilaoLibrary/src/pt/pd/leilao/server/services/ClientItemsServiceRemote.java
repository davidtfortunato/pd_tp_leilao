/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services;

import java.util.List;
import javax.ejb.Remote;
import pt.pd.leilao.server.model.CategoryEntity;
import pt.pd.leilao.server.model.ItemBidEntity;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ResponseService;

/**
 *
 * @author dfortunato
 */
@Remote
public interface ClientItemsServiceRemote {
    
    
    public ResponseService<List<ItemEntity>> getListItems();
    
    public ResponseService<List<ItemEntity>> getListItemsByCategory();
    
    public ResponseService<List<ItemEntity>> getListItemsByDescription();
    
    public ResponseService<List<ItemEntity>> getListItemsByPrice();
    
    public ResponseService<List<ItemEntity>> getListItemsByBuyNowPrice();
    
    public ResponseService<List<ItemEntity>> getListItemsByEndDate();
    
    public ResponseService<List<ItemEntity>> searchListItemsByText(String textToSearch);
    
    public ResponseService<ItemEntity> findItemEntityById(int itemId);
    
    public List<CategoryEntity> getListCategories();
    
    public CategoryEntity findCategoryByName(String categoryName);
    
    public List<ItemBidEntity> getListItemBids(int itemId);
    
}
