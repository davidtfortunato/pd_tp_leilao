/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services;

import java.util.List;
import javax.ejb.Remote;
import pt.pd.leilao.server.model.NewsletterMessageEntity;

/**
 *
 * @author dfortunato
 */
@Remote
public interface ClientNewsletterServiceRemote {
    
    public List<NewsletterMessageEntity> getListNewsletters();
    
}
