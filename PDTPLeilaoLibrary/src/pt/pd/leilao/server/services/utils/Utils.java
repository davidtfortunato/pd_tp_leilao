/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.utils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author dfortunato
 */
public class Utils {
    
    public static final SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");
    
    public static String getValorEuros(float valor)
    {
        DecimalFormat df = new DecimalFormat("#.##");
        
        return df.format(valor) + "€";
    }
    
    public static String getDefaultStringDate(Date date)
    {
        return DEFAULT_DATE_FORMAT.format(date);
    }
    
    public static boolean isStringOnlySpacesOrEmpty(String str)
    {
        if (str != null && !str.isEmpty()) {
            str = str.replaceAll(" ", "");
            return str.isEmpty();
        } else
        {
            return true;
        }
    }
}
