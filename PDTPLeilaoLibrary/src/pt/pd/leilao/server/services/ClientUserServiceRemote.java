/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services;

import java.util.List;
import javax.ejb.Remote;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.model.UserMessageEntity;
import pt.pd.leilao.server.model.UserNotificationEntity;
import pt.pd.leilao.server.model.enums.responses.ResponseUserRegisterType;
import pt.pd.leilao.server.model.responses.ResponseUserLoginData;

/**
 *
 * @author dfortunato
 */
@Remote
public interface ClientUserServiceRemote {
    
    public ResponseUserLoginData loginUser(String username, String password);
    
    public ResponseUserRegisterType registerUser(UserEntity user);
    
    public boolean logout();
    
    public UserEntity getCurrentUser();
    
    public ResponseService changePassword(String oldPassword, String newPassword);
    
    /************** Client User Service from Menu *****************/
    
    public ResponseService updateUserData(UserEntity newUserEntity);
    
    public ResponseService suspendUser(String motivo);
    
    public ResponseService transferMoney(float moneyTransfered);
    
    public ResponseService<List<ItemEntity>> getMyListItems();
    
    public ResponseService addItem(ItemEntity itemEntity);
    
    public ResponseService<List<ItemEntity>> getListItemsBought();
    
    public ResponseService<List<ItemEntity>> getListItemsSold();
    
    public boolean isFollowingItem(int itemId);
    
    public ResponseService toggleFollowItem(int itemId);
    
    public ResponseService<List<ItemEntity>> getListItemsFollowed();
    
    public ResponseService<UserMessageEntity> sendUserMessage(String usernameReceiver, String message);
    
    public ResponseService<List<UserMessageEntity>> getReceivedMessages();
    
    public ResponseService<List<UserMessageEntity>> getSentMessages();
    
    public List<UserEntity> getListUsers();
    
    public ResponseService addReportItem(int itemId, String reason);
    
    public ResponseService addReportUser(int userTargetId, String reason);
    
    public List<UserNotificationEntity> getLastNotifications();
    
    public ResponseService putUserItemBid(int itemId, float valueBid);
    
}
