/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import pt.pd.leilao.server.model.NewsletterMessageEntity;
import pt.pd.leilao.server.services.db.DAOManagerLocal;
import pt.pd.leilao.server.services.db.entities.DBNewsletterEntity;
import pt.pd.leilao.server.services.utils.FileUtils;

/**
 *
 * @author dfortunato
 */
@Singleton
public class NewsletterManagerSingleton implements NewsletterManagerSingletonLocal {
    private static final String TAG = NewsletterManagerSingleton.class.getName();
    
    private static final String FILENAME_NEWSLETTER = "/tmp/newsletter";
    
    private List<NewsletterMessageEntity> listNewsletter;

    @EJB
    DAOManagerLocal dAOManagerLocal;
    
    public NewsletterManagerSingleton() {
        listNewsletter = new ArrayList<>();
    }
    
    
    @Override
    public void addNewsletter(NewsletterMessageEntity newsletterMessageEntity) {
        if (newsletterMessageEntity != null) {
            // Set Id
            newsletterMessageEntity.setId(listNewsletter.size());
            listNewsletter.add(newsletterMessageEntity);
            
            // Add the new element on DB
            dAOManagerLocal.getEntityManager().persist(new DBNewsletterEntity(newsletterMessageEntity));
        }
    }

    @Override
    public List<NewsletterMessageEntity> getListNewsletter() {
        return listNewsletter;
    }
    
    @PostConstruct
    public void loadState() {
        Logger.getLogger(TAG).log(Level.INFO, "init LoadState");
        
        // Load Newsletter
        //listNewsletter = (List<NewsletterMessageEntity>) FileUtils.loadData(FILENAME_NEWSLETTER);
        listNewsletter = new ArrayList<>();
        EntityManager em = dAOManagerLocal.getEntityManager();
        Query q = em.createNamedQuery("DBNewsletterEntity.findAll");
        List<DBNewsletterEntity> dbListNewsletter = q.getResultList();
        for(DBNewsletterEntity entity : dbListNewsletter)
        {
            listNewsletter.add(entity.convertToNewsletterEntity());
        }
        
        // Check if should generate Collections
        //if (listNewsletter == null) {
        //    listNewsletter = new ArrayList<>();
        //}
    }

    @PreDestroy
    public synchronized void saveState() {
        Logger.getLogger(TAG).log(Level.INFO, "init SaveState");
        
        // Save Newsletters
        FileUtils.saveData(FILENAME_NEWSLETTER, listNewsletter);
        
    }
}
