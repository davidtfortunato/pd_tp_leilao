/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.UserActivationOrderEntity;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.model.enums.AdminOrderType;
import pt.pd.leilao.server.model.enums.UserStateType;
import pt.pd.leilao.server.services.singletons.ItemsManagerSingletonLocal;
import pt.pd.leilao.server.services.singletons.UserManagerSingletonLocal;

/**
 *
 * @author dfortunato
 */
@Stateless
public class ClientGuestService implements ClientGuestServiceRemote {

    @EJB
    ItemsManagerSingletonLocal itemsManagerSingletonLocal;

    @EJB
    UserManagerSingletonLocal userManagerSingletonLocal;

    @Override
    public ResponseService<List<ItemEntity>> getLastSoldItems(int numItemsDisplayed) {
        List<ItemEntity> listItems = itemsManagerSingletonLocal.getListItems();

        // Sort by last update
        Collections.sort(listItems, ItemEntity.COMPARATOR_BY_LASTUPDATED);

        List<ItemEntity> lastSoldItems = new ArrayList<>();
        for (ItemEntity itemEntity : listItems) {
            if (itemEntity.isItemSold()) {
                lastSoldItems.add(itemEntity);
                if (lastSoldItems.size() >= numItemsDisplayed) {
                    break;
                }
            }
        }

        return new ResponseService<>(lastSoldItems);
    }

    @Override
    public ResponseService postRequestReactivate(String username, String password, String reasonDescription) {
        UserEntity userEntity = userManagerSingletonLocal.findUserByUsername(username);

        if (userEntity != null) {
            if (userEntity.validateCredentials(password)) {
                UserActivationOrderEntity order = UserActivationOrderEntity.generateOrder(AdminOrderType.REACTIVATION, userEntity, reasonDescription);
                if (userEntity.getUserStateType() != UserStateType.ACTIVE && userManagerSingletonLocal.addAdminOrder(order)) {
                    return new ResponseService(true);
                } else {
                    return new ResponseService(ResponseService.RESPONSE_CODE_ERROR_ALREADY_EXISTS);
                }
            } else {
                return new ResponseService(ResponseService.RESPONSE_CODE_INVALID_DATA);
            }
        } else {
            return new ResponseService(ResponseService.RESPONSE_CODE_NOT_FOUND);
        }

    }

}
