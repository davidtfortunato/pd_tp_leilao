/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.db.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import pt.pd.leilao.server.model.CategoryEntity;
import pt.pd.leilao.server.model.ItemEntity;

/**
 *
 * @author dfortunato
 */
@Entity
@Table(name = "pd_category")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DBCategoryEntity.findAll", query = "SELECT d FROM DBCategoryEntity d ORDER BY d.id ASC")
    , @NamedQuery(name = "DBCategoryEntity.findById", query = "SELECT d FROM DBCategoryEntity d WHERE d.id = :id")
    , @NamedQuery(name = "DBCategoryEntity.findByCatName", query = "SELECT d FROM DBCategoryEntity d WHERE d.catName = :catName")})
public class DBCategoryEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "cat_name")
    private String catName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categoryId")
    private Collection<DBItemEntity> dBItemEntityCollection;

    public DBCategoryEntity() {
    }

    public DBCategoryEntity(Integer id) {
        this.id = id;
    }

    public DBCategoryEntity(Integer id, String catName) {
        this.id = id;
        this.catName = catName;
    }

    public DBCategoryEntity(CategoryEntity categoryEntity)
    {
        this.id = categoryEntity.getId();
        this.catName = categoryEntity.getName();
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    @XmlTransient
    public Collection<DBItemEntity> getDBItemEntityCollection() {
        return dBItemEntityCollection;
    }

    public void setDBItemEntityCollection(Collection<DBItemEntity> dBItemEntityCollection) {
        this.dBItemEntityCollection = dBItemEntityCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DBCategoryEntity)) {
            return false;
        }
        DBCategoryEntity other = (DBCategoryEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.pd.leilao.server.services.db.entities.DBCategoryEntity[ id=" + id + " ]";
    }
    
    public CategoryEntity convertToModelEntity()
    {
        CategoryEntity entity = new CategoryEntity();
        entity.setId(getId());
        entity.setName(getCatName());
        
        return entity;
    }
    
}
