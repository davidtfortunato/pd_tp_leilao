/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.controllers;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.jboss.logging.Logger;
import pt.pd.leilao.server.model.CategoryEntity;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.model.UserMessageEntity;
import pt.pd.leilao.server.model.UserNotificationEntity;
import pt.pd.leilao.server.model.enums.ItemStateType;
import pt.pd.leilao.server.model.enums.UserType;
import pt.pd.leilao.server.model.enums.responses.ResponseUserLoginType;
import pt.pd.leilao.server.model.enums.responses.ResponseUserRegisterType;
import pt.pd.leilao.server.model.responses.ResponseUserLoginData;
import pt.pd.leilao.server.services.ClientItemsServiceRemote;
import pt.pd.leilao.server.services.ClientUserService;
import pt.pd.leilao.server.services.ClientUserServiceRemote;
import pt.pd.leilao.server.services.controllers.types.ListItemsType;
import pt.pd.leilao.server.services.utils.AlertMessagesUtil;
import pt.pd.leilao.server.services.utils.Utils;

/**
 *
 * @author dfortunato
 */
@Named(value = "userController")
@SessionScoped
public class UserController implements Serializable {
    private static final String TAG = UserController.class.getSimpleName();
    
    @EJB
    ClientUserServiceRemote clientUserServiceRemote;
    
    @EJB
    ClientItemsServiceRemote clientItemsServiceRemote;
    
    /**
     * Creates a new instance of UserController
     */
    public UserController() {
        Logger.getLogger(TAG).debug("Constructor new UserController");
    }
    
    public String loginUser()
    {
        // Get Data
        Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String username = (String) params.get("in_username");
        String password = (String) params.get("in_password");
        
        ResponseUserLoginData responseUserLoginData = clientUserServiceRemote.loginUser(username, password);
        
        // Handle response
        FacesContext context = FacesContext.getCurrentInstance();
        switch(responseUserLoginData.getResponseUserLoginType())
        {
            case LOGIN_SUCCESS:
                return getInitialScreen();
            case ALREADY_LOGGED_IN:
                AlertMessagesUtil.displayResponseMessage("O utilizar já se encontra logado");
                break;
            case FAILED_NOT_ACTIVE:
                AlertMessagesUtil.displayResponseMessage("O utilizador não está activo");
                break;
            case FAILED_PASSWORD_WRONG:
                AlertMessagesUtil.displayResponseMessage("Password incorrecta");
                break;
            case FAILED_USERNAME_NOT_FOUND:
                AlertMessagesUtil.displayResponseMessage("O username não existe");
                break;
        }

        return null;
    }
    
    public String updateUser()
    {
        // Get Data
        Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String name = (String) params.get("input_nome");
        String morada = (String) params.get("input_morada");
        UserEntity updatedEntity = new UserEntity();
        updatedEntity.setNome(name);
        updatedEntity.setMorada(morada);
        
        ResponseService responseService = clientUserServiceRemote.updateUserData(updatedEntity);
        if (responseService.isSuccess()) 
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "Dados actualizados com sucesso");
        }
        else
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "Algo falhou na actualização dos dados");
        }
        return "user_manage_account_menu";
    }
    
    /**
     * Post a Suspension order
     * @return 
     */
    public String postSuspensionOrder()
    {
        // Get Data
        Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String motivo = (String) params.get("ta_motivo");
        
        ResponseService responseService = clientUserServiceRemote.suspendUser(motivo);
        
        if (responseService.isSuccess()) 
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "Pedido de suspensão enviado com sucesso");
            return "user_manage_account_menu";
        }
        else
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "Falhou o pedido de suspensao");
            return null;
        }
    }
    
    
    public String changePassword()
    {
        // Get Data
        Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String passwordOld = (String) params.get("input_password_antiga");
        String passwordNew = (String) params.get("input_password_nova");
        
        ResponseService responseService = clientUserServiceRemote.changePassword(passwordOld, passwordNew);
        
        if (responseService.isSuccess()) 
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "A alteração foi efectuada com sucesso!");
            switch(clientUserServiceRemote.getCurrentUser().getUserType())
            {
                case ADMIN:
                    return "admin_user_menu";
                case REGISTERED:
                    return "user_manage_account_menu";
            }
        }
        else
        {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Não foi possivel alterar a sua password - Codigo de erro: " + responseService.getResponseMessage()));
        }
        return null;
    }
    
    public String registerUser()
    {
        // Get Data
        Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String username = (String) params.get("in_username");
        String password = (String) params.get("in_password");
        
        UserEntity userEntity = new UserEntity();
        userEntity.setUserName(username);
        userEntity.setPassword(password);
        
        ResponseUserRegisterType responseUserRegisterType = clientUserServiceRemote.registerUser(userEntity);
        
        // Handle Response
        FacesContext context = FacesContext.getCurrentInstance();
        switch(responseUserRegisterType)
        {
            case USER_REGISTERED:
                AlertMessagesUtil.displayResponseMessage("Utilizador Registado com sucesso");
                return "index";
            case USER_REGISTER_PENDING_APPROVAL:
                AlertMessagesUtil.displayResponseMessage("Utilizador Registado. Está pendente de aprovação");
                return "index";
            case USERDATA_NOT_VALID:
                AlertMessagesUtil.displayResponseMessage("Dados de registo inválidos");
                break;
            case USERNAME_ALREADY_EXISTS:
                AlertMessagesUtil.displayResponseMessage("O nome de utilizador que tentou registar já existe");
                break;
        }
        
        return null;
    }
    
    public String validateUserRegisteredAccess()
    {
        if (isLoggedIn() && getCurrentUser().getUserType() == UserType.REGISTERED || getCurrentUser().getUserType() == UserType.ADMIN) 
        {
            return null;
        }
        else
        {
            // Send it to login
            return "init_login_user";
        }
    }
    
    public String validateUserAdminAccess()
    {
        if (isLoggedIn() && getCurrentUser().getUserType() == UserType.ADMIN) 
        {
            return null;
        }
        else
        {
            // Send it to login
            return "init_login_user";
        }
    }
    
    public void depositMoney()
    {
        // Get Data
        Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String depositMoney = (String) params.get("input_money");
        
        try
        {
          ResponseService responseService = clientUserServiceRemote.transferMoney(Float.parseFloat(depositMoney));
            if (responseService.isSuccess()) 
            {
                AlertMessagesUtil.displayResponseMessage(responseService, "Foram depositados " + depositMoney + "€ com sucesso");
            }
            else
            {
                AlertMessagesUtil.displayResponseMessage(responseService, "Não foi possivel transferir dinheiro para a sua conta");
            }  
        } catch(NumberFormatException e)
        {
            AlertMessagesUtil.displayResponseMessage("Não foi possivel transferir dinheiro para a sua conta. Tem de inserir um valor numérico válido.");
        }
        
    }
    
    public String logout()
    {
        return clientUserServiceRemote.logout() ? "index" : null;
    }
    
    public String getWelcomeMessage()
    {
        switch(getCurrentUser().getUserType())
        {
            case ADMIN:
                return "Bem vindo administrador. ";
            case REGISTERED:
                return "Bem vindo " + getCurrentUser().getUserName();
            case GUEST:
                return "Bem vindo visitante.";
        }
        return "";
    }
    
    public UserEntity getCurrentUser()
    {
        return clientUserServiceRemote.getCurrentUser();
    }
    
    public boolean changePassword(String oldPassword, String newPassword)
    {
        return clientUserServiceRemote.changePassword(oldPassword, newPassword).isSuccess();
    }
    
    public boolean isLoggedIn()
    {
        return getCurrentUser().isUserLoggedIn();
    }

    public String getInitialScreen()
    {
        if (isLoggedIn()) 
        {
            switch(getCurrentUser().getUserType())
            {
                case REGISTERED:
                    return "registered_user_menu";
                case ADMIN:
                    return "admin_user_menu";
            }
        }
        return null;
    }
        
    
    /************* ITEMS MANAGER ******************/
    
    public String addItem()
    {
        try
        {
            Map params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            String descricao = (String) params.get("in_descricao");
            String precoInicial = (String) params.get("in_preco");
            String precoCompraJa = (String) params.get("in_preco_compra_ja");
            String selCategoria = (String) params.get("form_add_item:selector_category");
            String validadeMinutos = (String) params.get("in_data_validade");

            ItemEntity itemEntity = new ItemEntity();
            itemEntity.setDescription(descricao);
            itemEntity.setInitialPrice(Float.valueOf(precoInicial));
            itemEntity.setBuyNowPrice(Float.valueOf(precoCompraJa));
            CategoryEntity categoryEntity = clientItemsServiceRemote.findCategoryByName(selCategoria);
            if (categoryEntity != null) 
            {
                itemEntity.setCategory(categoryEntity);
            }
            else
            {
                AlertMessagesUtil.displayResponseMessage("Falhou a obter a categoria pretendida");
                return null;
            }
            itemEntity.setEndDateInMinutes(Integer.valueOf(validadeMinutos));
            ResponseService responseService = clientUserServiceRemote.addItem(itemEntity);

            if (responseService.isSuccess()) 
            {
                AlertMessagesUtil.displayResponseMessage(responseService, "Item adicionado com sucesso.");
            }
            else
            {
                AlertMessagesUtil.displayResponseMessage(responseService, "Falhou o registo do item");
            }
            return null; 
        } catch(NumberFormatException nfe)
        {
            AlertMessagesUtil.displayResponseMessage("Falhou o registo do item - Adicionar um valor incorrecto em que deveria ser numérico");
        }
        return null;
    }
    
    public List<ItemEntity> getListItemsByListType()
    {
        ListItemsType type = getUserListType();
        switch(type)
        {
            case LIST_USER_ITEMS_SELLING:
                return clientUserServiceRemote.getMyListItems().getDataObject();
            case LIST_USER_ITEMS_FOLLOWING:
                return clientUserServiceRemote.getListItemsFollowed().getDataObject();
            case LIST_USER_ITEMS_BOUGHT:
                return clientUserServiceRemote.getListItemsBought().getDataObject();
            case LIST_USER_ITEMS_SOLD:
                return clientUserServiceRemote.getListItemsSold().getDataObject();
        }
        return new ArrayList<>();
    }
        
    
    public boolean isFollowingItem(int itemId)
    {
        return clientUserServiceRemote.isFollowingItem(itemId);
    }
    
    public boolean isAllowedToBuyItem(ItemEntity itemEntity)
    {
        return itemEntity.getItemStateType() == ItemStateType.ACTIVE_SELL 
                && itemEntity.getUserSeller().getId() != clientUserServiceRemote.getCurrentUser().getId();
    }
    
    public ResponseService buyItem(int itemId, float value)
    {
        return clientUserServiceRemote.putUserItemBid(itemId, value);
    }
    
    public void sendMessageToSeller(String message, ItemEntity itemEntity)
    {
        ResponseService responseService = clientUserServiceRemote.sendUserMessage(itemEntity.getUserSeller().getUserName(), message);
        
        if (responseService.isSuccess()) 
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "Mensagem enviada com sucesso");
        }
        else
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "Não foi possivel enviar a mensagem");
        }
    }
    
    public void reportSellerAbuse(String message, ItemEntity itemEntity)
    {
        ResponseService responseService = clientUserServiceRemote.addReportUser(itemEntity.getUserSeller().getId(), message);
        if (responseService.isSuccess()) 
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "Foi reportado com sucesso");
        }
        else
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "Não foi possivel enviar o report");
        }
    }
    
    public void reportItemAbuse(String message, ItemEntity itemEntity)
    {
        ResponseService responseService = clientUserServiceRemote.addReportItem(itemEntity.getId(), message);
        if (responseService.isSuccess()) 
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "Foi reportado com sucesso");
        }
        else
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "Não foi possivel enviar o report");
        }
    }
    
    public void toggleFollowItem(int itemId)
    {
        clientUserServiceRemote.toggleFollowItem(itemId);
    }
    
    public ItemEntity getItemDetails()
    {
        return clientItemsServiceRemote.findItemEntityById(Integer.valueOf(getParamValue("id"))).getDataObject();
    }
    
    public String getParamValue(String paramId)
    {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext extContext = context.getExternalContext();
        Map<String, String> params = extContext.getRequestParameterMap();
        return params.get(paramId);
    }
    
    public String getId() {
        String id = getParamValue("id");
        return id;
    }
    
    
    public ListItemsType getUserListType() {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext extContext = context.getExternalContext();
        Map<String, String> params = extContext.getRequestParameterMap();
        return ListItemsType.getType(params.get("listType"));
    }
    
    
    
    /******************************* Messages ***************************/
    
    
    public List<UserMessageEntity> getListMessagesReceived()
    {
        return clientUserServiceRemote.getReceivedMessages().getDataObject();
    }
    
    public List<UserMessageEntity> getListMessagesSent()
    {
        return clientUserServiceRemote.getSentMessages().getDataObject();
    }
    
    public String getListMessagesType()
    {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext extContext = context.getExternalContext();
        Map<String, String> params = extContext.getRequestParameterMap();
        return params.get("listType");
    }
    
    public List<UserEntity> getListUsers()
    {
        return clientUserServiceRemote.getListUsers();
    }
    
    public String sendNewMessage()
    {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext extContext = context.getExternalContext();
        Map<String, String> params = extContext.getRequestParameterMap();
        String userNameToSend = params.get("form_send_message:selector_user");
        String message = params.get("ta_message");
        
        ResponseService response = clientUserServiceRemote.sendUserMessage(userNameToSend, message);
        if (response.isSuccess()) 
        {
            AlertMessagesUtil.displayResponseMessage(response, "A mensagem foi enviada com sucesso");
            return "user_messages_menu";
        }
        else
        {
            AlertMessagesUtil.displayResponseMessage(response, "Não foi possivel enviar a mensagem");
        }
        return null;
    }
    
    public List<UserNotificationEntity> getLastNotifications()
    {
        return clientUserServiceRemote.getLastNotifications();
    }
    
    /****************** MENU *****************/
    
    public String getInitialMenu()
    {
        switch(getCurrentUser().getUserType())
        {
            case ADMIN:
                return "admin_user_menu";
            case REGISTERED:
                return "registered_user_menu";
            case GUEST:
                return "guest_menu";
        }
        return "index";
    }
    
    public String getCurrentTime()
    {
        return Utils.getDefaultStringDate(new Date(Calendar.getInstance().getTimeInMillis()));
    }
    
}
