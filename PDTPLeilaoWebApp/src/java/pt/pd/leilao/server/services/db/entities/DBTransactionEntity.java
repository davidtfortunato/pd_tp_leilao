/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.db.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import pt.pd.leilao.server.model.TransactionEntity;

/**
 *
 * @author dfortunato
 */
@Entity
@Table(name = "pd_transaction")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DBTransactionEntity.findAll", query = "SELECT d FROM DBTransactionEntity d")
    , @NamedQuery(name = "DBTransactionEntity.findById", query = "SELECT d FROM DBTransactionEntity d WHERE d.id = :id")
    , @NamedQuery(name = "DBTransactionEntity.findByValue", query = "SELECT d FROM DBTransactionEntity d WHERE d.value = :value")
    , @NamedQuery(name = "DBTransactionEntity.findByDate", query = "SELECT d FROM DBTransactionEntity d WHERE d.date = :date")
    , @NamedQuery(name = "DBTransactionEntity.findByUserReceiver", query = "SELECT d FROM DBTransactionEntity d WHERE d.idUserReceiver.id = :userId")
    , @NamedQuery(name = "DBTransactionEntity.findByUserSender", query = "SELECT d FROM DBTransactionEntity d WHERE d.idUserSender.id = :userId")
    , @NamedQuery(name = "DBTransactionEntity.findByItemSold", query = "SELECT d FROM DBTransactionEntity d WHERE d.idItemSold.id = :itemId")})
public class DBTransactionEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "value")
    private BigDecimal value;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @JoinColumn(name = "id_item_sold", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private DBItemEntity idItemSold;
    @JoinColumn(name = "id_user_receiver", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private DBUserEntity idUserReceiver;
    @JoinColumn(name = "id_user_sender", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private DBUserEntity idUserSender;

    public DBTransactionEntity() {
    }

    public DBTransactionEntity(Integer id) {
        this.id = id;
    }

    public DBTransactionEntity(Integer id, BigDecimal value, Date date) {
        this.id = id;
        this.value = value;
        this.date = date;
    }
    
    public DBTransactionEntity(TransactionEntity transactionEntity)
    {
        this.id = transactionEntity.getId();
        this.date = new Date(transactionEntity.getDate());
        this.idItemSold = new DBItemEntity(transactionEntity.getItemSold());
        this.idUserReceiver = new DBUserEntity(transactionEntity.getUserReceiver());
        this.idUserSender = new DBUserEntity(transactionEntity.getUserSender());
        this.value = new BigDecimal(String.valueOf(transactionEntity.getValue()));
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public DBItemEntity getIdItemSold() {
        return idItemSold;
    }

    public void setIdItemSold(DBItemEntity idItemSold) {
        this.idItemSold = idItemSold;
    }

    public DBUserEntity getIdUserReceiver() {
        return idUserReceiver;
    }

    public void setIdUserReceiver(DBUserEntity idUserReceiver) {
        this.idUserReceiver = idUserReceiver;
    }

    public DBUserEntity getIdUserSender() {
        return idUserSender;
    }

    public void setIdUserSender(DBUserEntity idUserSender) {
        this.idUserSender = idUserSender;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DBTransactionEntity)) {
            return false;
        }
        DBTransactionEntity other = (DBTransactionEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.pd.leilao.server.services.db.entities.DBTransactionEntity[ id=" + id + " ]";
    }
    
    public TransactionEntity convertToModelEntity()
    {
        TransactionEntity transactionEntity = new TransactionEntity();
        transactionEntity.setId(getId());
        transactionEntity.setDate(getDate().getTime());
        transactionEntity.setItemSold(getIdItemSold().convertToModelEntity());
        transactionEntity.setUserReceiver(getIdUserReceiver().convertToModelEntity());
        transactionEntity.setUserSender(getIdUserSender().convertToModelEntity());
        transactionEntity.setValue(getValue().floatValue());
        
        return transactionEntity;
    }
    
}
