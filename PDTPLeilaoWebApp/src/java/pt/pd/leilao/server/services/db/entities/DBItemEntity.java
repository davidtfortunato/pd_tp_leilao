/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.db.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.model.enums.ItemStateType;

/**
 *
 * @author dfortunato
 */
@Entity
@Table(name = "pd_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DBItemEntity.findAll", query = "SELECT d FROM DBItemEntity d ORDER BY d.id ASC")
    , @NamedQuery(name = "DBItemEntity.findById", query = "SELECT d FROM DBItemEntity d WHERE d.id = :id")
    , @NamedQuery(name = "DBItemEntity.findByInitialPrice", query = "SELECT d FROM DBItemEntity d WHERE d.initialPrice = :initialPrice")
    , @NamedQuery(name = "DBItemEntity.findByBuyNowPrice", query = "SELECT d FROM DBItemEntity d WHERE d.buyNowPrice = :buyNowPrice")
    , @NamedQuery(name = "DBItemEntity.findByLastPrice", query = "SELECT d FROM DBItemEntity d WHERE d.lastPrice = :lastPrice")
    , @NamedQuery(name = "DBItemEntity.findByEndDate", query = "SELECT d FROM DBItemEntity d WHERE d.endDate = :endDate")
    , @NamedQuery(name = "DBItemEntity.findByItemStateType", query = "SELECT d FROM DBItemEntity d WHERE d.itemStateType = :itemStateType")
    , @NamedQuery(name = "DBItemEntity.findByDescription", query = "SELECT d FROM DBItemEntity d WHERE d.description = :description")
    , @NamedQuery(name = "DBItemEntity.findByLastUpdate", query = "SELECT d FROM DBItemEntity d WHERE d.lastUpdate = :lastUpdate")
    , @NamedQuery(name = "DBItemEntity.findByUserSeller", query = "SELECT d FROM DBItemEntity d WHERE d.userSellerId.id = :userId")
    , @NamedQuery(name = "DBItemEntity.findByUserBuyer", query = "SELECT d FROM DBItemEntity d WHERE d.userBuyerId.id = :userId")
    , @NamedQuery(name = "DBItemEntity.searchByText", query = "SELECT d FROM DBItemEntity d WHERE d.description LIKE :textToSearch")
    , @NamedQuery(name = "DBItemEntity.findItemsSellingByEndDate", query = "SELECT d FROM DBItemEntity d WHERE d.itemStateType = 'active_sell' ORDER BY d.endDate ASC")})
public class DBItemEntity implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "initial_price")
    private BigDecimal initialPrice;
    @Column(name = "buy_now_price")
    private BigDecimal buyNowPrice;
    @Column(name = "last_price")
    private BigDecimal lastPrice;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idItemReference")
    private Collection<DBUserNotificationEntity> dBUserNotificationEntityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idItemSold")
    private Collection<DBTransactionEntity> dBTransactionEntityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "itemId")
    private Collection<DBItemBid> dBItemBidCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idItemTarget")
    private Collection<DBReportItem> dBReportItemCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "itemId")
    private Collection<DBFollowItemEntity> dBFollowItemEntityCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Size(max = 50)
    @Column(name = "item_state_type")
    private String itemStateType;
    @Size(max = 256)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private DBCategoryEntity categoryId;
    @JoinColumn(name = "user_buyer_id", referencedColumnName = "id")
    @ManyToOne
    private DBUserEntity userBuyerId;
    @JoinColumn(name = "user_seller_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private DBUserEntity userSellerId;
    
    public DBItemEntity() {
    }

    public DBItemEntity(Integer id) {
        this.id = id;
    }

    public DBItemEntity(Integer id, BigDecimal initialPrice, Date endDate, Date lastUpdate) {
        this.id = id;
        this.initialPrice = initialPrice;
        this.endDate = endDate;
        this.lastUpdate = lastUpdate;
    }

    public DBItemEntity(ItemEntity itemEntity)
    {
        this.id = itemEntity.getId();
        this.buyNowPrice = new BigDecimal(String.valueOf(itemEntity.getBuyNowPrice()));
        this.initialPrice = new BigDecimal(String.valueOf(itemEntity.getInitialPrice()));
        this.lastPrice = new BigDecimal(String.valueOf(itemEntity.getLastPrice()));
        this.categoryId = new DBCategoryEntity(itemEntity.getCategory());
        this.description = itemEntity.getDescription();
        this.endDate = new Date(itemEntity.getEndDate());
        this.itemStateType = itemEntity.getItemStateType().getTypeValue();
        this.lastUpdate = new Date(itemEntity.getLastUpdate());
        if (itemEntity.getUserBuyer() != null) {
            this.userBuyerId = new DBUserEntity(itemEntity.getUserBuyer());
        }
        this.userSellerId = new DBUserEntity(itemEntity.getUserSeller());
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getInitialPrice() {
        return initialPrice;
    }

    public void setInitialPrice(BigDecimal initialPrice) {
        this.initialPrice = initialPrice;
    }

    public BigDecimal getBuyNowPrice() {
        return buyNowPrice;
    }

    public void setBuyNowPrice(BigDecimal buyNowPrice) {
        this.buyNowPrice = buyNowPrice;
    }

    public BigDecimal getLastPrice() {
        return lastPrice;
    }

    public void setLastPrice(BigDecimal lastPrice) {
        this.lastPrice = lastPrice;
    }

    
    
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getItemStateType() {
        return itemStateType;
    }

    public void setItemStateType(String itemStateType) {
        this.itemStateType = itemStateType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public DBCategoryEntity getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(DBCategoryEntity categoryId) {
        this.categoryId = categoryId;
    }

    public DBUserEntity getUserBuyerId() {
        return userBuyerId;
    }

    public void setUserBuyerId(DBUserEntity userBuyerId) {
        this.userBuyerId = userBuyerId;
    }

    public DBUserEntity getUserSellerId() {
        return userSellerId;
    }

    public void setUserSellerId(DBUserEntity userSellerId) {
        this.userSellerId = userSellerId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DBItemEntity)) {
            return false;
        }
        DBItemEntity other = (DBItemEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.pd.leilao.server.services.db.entities.DBItemEntity[ id=" + id + " ]";
    }
    
    public ItemEntity convertToModelEntity()
    {
        ItemEntity entity = new ItemEntity();
        entity.setBuyNowPrice(getBuyNowPrice().floatValue());
        entity.setCategory(getCategoryId().convertToModelEntity());
        entity.setDescription(getDescription());
        entity.setEndDate(getEndDate().getTime());
        entity.setId(getId());
        entity.setInitialPrice(getInitialPrice().floatValue());
        entity.setItemStateType(ItemStateType.getType(getItemStateType()));
        entity.setLastPrice(getLastPrice().floatValue());
        entity.setLastUpdate(getLastUpdate().getTime());
        if (getUserBuyerId() != null) {
            entity.setUserBuyer(getUserBuyerId().convertToModelEntity());
        }
        entity.setUserSeller(getUserSellerId().convertToModelEntity());
        
        return entity;
    }
    
    @XmlTransient
    public Collection<DBFollowItemEntity> getDBFollowItemEntityCollection() {
        return dBFollowItemEntityCollection;
    }

    public void setDBFollowItemEntityCollection(Collection<DBFollowItemEntity> dBFollowItemEntityCollection) {
        this.dBFollowItemEntityCollection = dBFollowItemEntityCollection;
    }

    @XmlTransient
    public Collection<DBItemBid> getDBItemBidCollection() {
        return dBItemBidCollection;
    }

    public void setDBItemBidCollection(Collection<DBItemBid> dBItemBidCollection) {
        this.dBItemBidCollection = dBItemBidCollection;
    }

    @XmlTransient
    public Collection<DBReportItem> getDBReportItemCollection() {
        return dBReportItemCollection;
    }

    public void setDBReportItemCollection(Collection<DBReportItem> dBReportItemCollection) {
        this.dBReportItemCollection = dBReportItemCollection;
    }

    @XmlTransient
    public Collection<DBTransactionEntity> getDBTransactionEntityCollection() {
        return dBTransactionEntityCollection;
    }

    public void setDBTransactionEntityCollection(Collection<DBTransactionEntity> dBTransactionEntityCollection) {
        this.dBTransactionEntityCollection = dBTransactionEntityCollection;
    }

    @XmlTransient
    public Collection<DBUserNotificationEntity> getDBUserNotificationEntityCollection() {
        return dBUserNotificationEntityCollection;
    }

    public void setDBUserNotificationEntityCollection(Collection<DBUserNotificationEntity> dBUserNotificationEntityCollection) {
        this.dBUserNotificationEntityCollection = dBUserNotificationEntityCollection;
    }
    
}
