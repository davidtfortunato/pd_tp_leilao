/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.persistence.Query;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.NewsletterMessageEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.services.db.DAOManagerLocal;
import pt.pd.leilao.server.services.db.entities.DBCategoryEntity;
import pt.pd.leilao.server.services.db.entities.DBFollowItemEntity;
import pt.pd.leilao.server.services.db.entities.DBItemEntity;
import pt.pd.leilao.server.services.db.entities.DBUserEntity;
import pt.pd.leilao.server.services.utils.FileUtils;

/**
 *
 * @author dfortunato
 */
@Singleton
public class FollowItemManagerSingleton implements FollowItemManagerSingletonLocal {
    private static final String TAG = NewsletterManagerSingleton.class.getName();
    
    @EJB
    ItemsManagerSingletonLocal itemsManagerSingletonLocal;
    
    @EJB
    DAOManagerLocal dAOManagerLocal;
    
    public FollowItemManagerSingleton() 
    {
    }

    @Override
    public boolean isItemFollowedByUser(int itemId, int userId) {
        return (getItemFollowedByUser(itemId, userId) != null);
    }

    @Override
    public ResponseService toggleItemFollowedByUser(int itemId, int userId) {
        ResponseService responseService = new ResponseService();
        DBFollowItemEntity follwBFollowItemEntity = getItemFollowedByUser(itemId, userId);
        if(follwBFollowItemEntity != null)
        {
            // Is following. Should remove the row
            dAOManagerLocal.getEntityManager().remove(follwBFollowItemEntity);
        }
        else
        {
            DBFollowItemEntity followEntity = new DBFollowItemEntity();
            followEntity.setItemId(dAOManagerLocal.getEntityManager().getReference(DBItemEntity.class, itemId));
            followEntity.setUserId(dAOManagerLocal.getEntityManager().getReference(DBUserEntity.class, userId));
            dAOManagerLocal.getEntityManager().persist(followEntity);
            responseService.setSuccess(true);
        }
        
        return responseService;
    }

    @Override
    public ResponseService<List<ItemEntity>> getListItemsFollowing(int userId) {
        List<DBFollowItemEntity> listFollows = dAOManagerLocal.getEntityManager().createNamedQuery("DBFollowItemEntity.findItemsFollowedByUser").setParameter("userId", userId).getResultList();
        List<ItemEntity> listItems = new ArrayList<>();
        if (listFollows != null && !listFollows.isEmpty()) {
            for (DBFollowItemEntity followEntity : listFollows) {
                listItems.add(followEntity.getItemId().convertToModelEntity());
            }
        }
        return new ResponseService<>(listItems);
    }
    
    private DBFollowItemEntity getItemFollowedByUser(int itemId, int userId)
    {
        Query q = dAOManagerLocal.getEntityManager().createNamedQuery("DBFollowItemEntity.findItemFollowedByUser");
        List<DBFollowItemEntity> listEntities = q.setParameter("itemId", itemId).setParameter("userId", userId).getResultList();
        
        if(listEntities != null && !listEntities.isEmpty())
        {
            return listEntities.get(0);
        }
        else
        {
            return null;
        }
    }

    @Override
    public ResponseService<List<UserEntity>> getListUsersFollowingItem(int itemId) {
        List<DBFollowItemEntity> listFollows = dAOManagerLocal.getEntityManager().createNamedQuery("DBFollowItemEntity.findFollowersByItemId").setParameter("itemId", itemId).getResultList();
        List<UserEntity> listItems = new ArrayList<>();
        if (listFollows != null && !listFollows.isEmpty()) {
            for (DBFollowItemEntity followEntity : listFollows) {
                listItems.add(followEntity.getUserId().convertToModelEntity());
            }
        }
        return new ResponseService<>(listItems);
    }
    
}
