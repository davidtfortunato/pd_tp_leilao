/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.util.List;
import javax.ejb.Local;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.UserEntity;

/**
 *
 * @author dfortunato
 */
@Local
public interface FollowItemManagerSingletonLocal {
    
    public boolean isItemFollowedByUser(int itemId, int userId);
    
    public ResponseService toggleItemFollowedByUser(int itemId, int userId);
    
    public ResponseService<List<ItemEntity>> getListItemsFollowing(int userId);
    
    public ResponseService<List<UserEntity>> getListUsersFollowingItem(int itemId);
}
