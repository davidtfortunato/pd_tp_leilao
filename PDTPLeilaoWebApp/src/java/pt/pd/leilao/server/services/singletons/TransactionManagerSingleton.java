/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import pt.pd.leilao.server.model.ItemBidEntity;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.TransactionEntity;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.model.enums.ItemStateType;
import pt.pd.leilao.server.services.db.DAOManagerLocal;
import pt.pd.leilao.server.services.db.entities.DBItemEntity;
import pt.pd.leilao.server.services.db.entities.DBTransactionEntity;
import pt.pd.leilao.server.services.db.entities.DBUserEntity;

/**
 *
 * @author dfortunato
 */
@Singleton
public class TransactionManagerSingleton implements TransactionManagerSingletonLocal {

    @EJB
    DAOManagerLocal daoManagerLocal;
    
    @EJB
    UserManagerSingletonLocal userManagerSingletonLocal;
    
    @EJB
    ItemsManagerSingletonLocal itemsManagerSingletonLocal;
    
    @Override
    public List<TransactionEntity> getAllTransactions() {
        List<DBTransactionEntity> listDBTransactions = daoManagerLocal.getEntityManager().createNamedQuery("DBTransactionEntity.findAll").getResultList();
        List<TransactionEntity> listTransactions = new ArrayList<>();
        for (DBTransactionEntity listDBTransaction : listDBTransactions) {
            listTransactions.add(listDBTransaction.convertToModelEntity());
        }
        return listTransactions;
    }

    @Override
    public List<TransactionEntity> getUserSentTransactions(int userId) {
        List<DBTransactionEntity> listDBTransactions = daoManagerLocal.getEntityManager().createNamedQuery("DBTransactionEntity.findByUserSender").setParameter("userId", userId).getResultList();
        List<TransactionEntity> listTransactions = new ArrayList<>();
        for (DBTransactionEntity listDBTransaction : listDBTransactions) {
            listTransactions.add(listDBTransaction.convertToModelEntity());
        }
        return listTransactions;
    }

    @Override
    public List<TransactionEntity> getUserReceiveTransactions(int userId) {
        List<DBTransactionEntity> listDBTransactions = daoManagerLocal.getEntityManager().createNamedQuery("DBTransactionEntity.findByUserReceiver").setParameter("userId", userId).getResultList();
        List<TransactionEntity> listTransactions = new ArrayList<>();
        for (DBTransactionEntity listDBTransaction : listDBTransactions) {
            listTransactions.add(listDBTransaction.convertToModelEntity());
        }
        return listTransactions;
    }

    @Override
    public TransactionEntity findItemTransaction(int itemId) {
        try
        {
            DBTransactionEntity dBTransactionEntity = (DBTransactionEntity) daoManagerLocal.getEntityManager().createNamedQuery("DBTransactionEntity.findByItemSold").setParameter("itemId", itemId).getSingleResult();
            return dBTransactionEntity.convertToModelEntity();  
        } catch(Exception e)
        {
            return null;
        }
        
    }

    @Override
    public ResponseService<TransactionEntity> processTransaction(ItemBidEntity itemBidEntity) {
        if (itemBidEntity != null && itemBidEntity.getItemTarget() != null) 
        {
            UserEntity userReceiving = userManagerSingletonLocal.findUserByUserId(itemBidEntity.getItemTarget().getUserSeller().getId());
            UserEntity userSending = userManagerSingletonLocal.findUserByUserId(itemBidEntity.getUserBuyer().getId());
            ItemEntity itemEntity = itemsManagerSingletonLocal.findItemById(itemBidEntity.getItemTarget().getId());
            
            // Validate 
            if (userSending.getSaldo() < itemBidEntity.getValueBid()) 
            {
                // Without money available from the buyer
                return new ResponseService<>(ResponseService.RESPONSE_CODE_ERROR_MONEY_NOT_AVAILABLE);
            }
            else
            {
                userReceiving.appendSaldo(itemBidEntity.getValueBid());
                userSending.appendSaldo(-itemBidEntity.getValueBid());

                // Update item
                itemEntity.setItemStateType(itemBidEntity.isSoldBuyNow() ? ItemStateType.SOLD_BUYNOW : ItemStateType.SOLD);
                itemEntity.setUserBuyer(userSending);
                itemEntity.setLastPrice(itemBidEntity.getValueBid());
                itemEntity.setLastUpdate(Calendar.getInstance().getTimeInMillis());
                
                // Generate Transaction Entity
                TransactionEntity transactionEntity = new TransactionEntity();
                transactionEntity.setDate(Calendar.getInstance().getTimeInMillis());
                transactionEntity.setItemSold(itemEntity);
                transactionEntity.setUserReceiver(userReceiving);
                transactionEntity.setUserSender(userSending);
                transactionEntity.setValue(itemBidEntity.getValueBid());
                
                // Persist
                daoManagerLocal.getEntityManager().merge(new DBItemEntity(itemEntity));
                daoManagerLocal.getEntityManager().merge(new DBUserEntity(userReceiving));
                daoManagerLocal.getEntityManager().merge(new DBUserEntity(userSending));
                daoManagerLocal.getEntityManager().persist(new DBTransactionEntity(transactionEntity));
                
                return new ResponseService<>(true);
            }
            
        }
        else
        {
            return new ResponseService<>(ResponseService.RESPONSE_CODE_INVALID_DATA);
        }
        
        
    }

    
}
