/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import pt.pd.leilao.server.model.CategoryEntity;
import pt.pd.leilao.server.model.ItemBidEntity;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.services.singletons.CategoriesManagerSingletonLocal;
import pt.pd.leilao.server.services.singletons.ItemsManagerSingletonLocal;

/**
 *
 * @author dfortunato
 */
@Stateless
public class ClientItemsService implements ClientItemsServiceRemote {
    
    @EJB
    ItemsManagerSingletonLocal itemsManagerSingletonLocal;
    
    @EJB
    CategoriesManagerSingletonLocal categoriesManagerSingletonLocal;
    
    @Override
    public ResponseService<List<ItemEntity>> getListItems() {
        return new ResponseService<List<ItemEntity>>(itemsManagerSingletonLocal.getListItems());
    }

    private List<ItemEntity> getListItemsSorted(Comparator<ItemEntity> comparator)
    {
        List<ItemEntity> listItems = itemsManagerSingletonLocal.getListItems();
        Collections.sort(listItems, comparator);
        return listItems;
    }
    
    @Override
    public ResponseService<List<ItemEntity>> getListItemsByCategory() {
        return new ResponseService<>(getListItemsSorted(ItemEntity.COMPARATOR_BY_CATEGORY));
    }

    @Override
    public ResponseService<List<ItemEntity>> getListItemsByDescription() {
        return new ResponseService<>(getListItemsSorted(ItemEntity.COMPARATOR_BY_DESCRIPTION));
    }

    @Override
    public ResponseService<List<ItemEntity>> getListItemsByPrice() {
        return new ResponseService<>(getListItemsSorted(ItemEntity.COMPARATOR_BY_PRICE));
    }

    @Override
    public ResponseService<List<ItemEntity>> getListItemsByBuyNowPrice() {
        return new ResponseService<>(getListItemsSorted(ItemEntity.COMPARATOR_BY_BUYNOW_PRICE));
    }

    @Override
    public ResponseService<List<ItemEntity>> getListItemsByEndDate() {
        return new ResponseService<>(getListItemsSorted(ItemEntity.COMPARATOR_BY_ENDDATE));
    }

    @Override
    public ResponseService<ItemEntity> findItemEntityById(int itemId) {
        ItemEntity itemEntity = itemsManagerSingletonLocal.findItemById(itemId);
        
        return new ResponseService<ItemEntity>(itemEntity);
    }

    @Override
    public List<CategoryEntity> getListCategories() {
        return categoriesManagerSingletonLocal.getListCategories();
    }

    @Override
    public CategoryEntity findCategoryByName(String categoryName) {
        return categoriesManagerSingletonLocal.findCategoryByName(categoryName);
    }

    @Override
    public ResponseService<List<ItemEntity>> searchListItemsByText(String textToSearch) {
        return new ResponseService<>(itemsManagerSingletonLocal.searchItemsByText(textToSearch));
    }

    @Override
    public List<ItemBidEntity> getListItemBids(int itemId) {
        return itemsManagerSingletonLocal.getListItemBids(itemId);
    }
    
}
