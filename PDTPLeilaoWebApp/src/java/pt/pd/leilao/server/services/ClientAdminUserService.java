/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import pt.pd.leilao.server.model.CategoryEntity;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ReportItemEntity;
import pt.pd.leilao.server.model.ReportUserEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.UserActivationOrderEntity;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.model.enums.AdminOrderResponseType;
import pt.pd.leilao.server.model.enums.ItemStateType;
import pt.pd.leilao.server.model.enums.NewsletterMessageType;
import pt.pd.leilao.server.model.enums.ReportStateType;
import pt.pd.leilao.server.model.enums.UserStateType;
import pt.pd.leilao.server.model.enums.UserType;
import pt.pd.leilao.server.services.singletons.CategoriesManagerSingletonLocal;
import pt.pd.leilao.server.services.singletons.ItemsManagerSingletonLocal;
import pt.pd.leilao.server.services.singletons.NewsletterManagerSingletonLocal;
import pt.pd.leilao.server.services.singletons.ReportManagerSingletonLocal;
import pt.pd.leilao.server.services.singletons.UserManagerSingleton;
import pt.pd.leilao.server.services.singletons.UserManagerSingletonLocal;
import pt.pd.leilao.server.services.utils.NewsletterUtil;

/**
 *
 * @author dfortunato
 */
@Stateless
public class ClientAdminUserService implements ClientAdminUserServiceRemote {

    @EJB
    UserManagerSingletonLocal userManagerSingletonLocal;

    @EJB
    ItemsManagerSingletonLocal itemsManagerSingletonLocal;

    @EJB
    CategoriesManagerSingletonLocal categoriesManagerSingletonLocal;

    @EJB
    NewsletterManagerSingletonLocal newsletterManagerSingletonLocal;

    @EJB
    ReportManagerSingletonLocal reportManagerSingletonLocal;

    @Override
    public ResponseService<List<UserActivationOrderEntity>> getListUserActivationOrderEntity() {
        List<UserActivationOrderEntity> list = userManagerSingletonLocal.getListAdminUserActivationOrders();
        return new ResponseService<List<UserActivationOrderEntity>>(list);
    }

    @Override
    public ResponseService replyUserActivationOrder(int orderId, AdminOrderResponseType adminOrderResponseType) {
        return new ResponseService(userManagerSingletonLocal.replyAdminUserActivationOrder(orderId, adminOrderResponseType));
    }

    @Override
    public ResponseService<List<UserEntity>> getListUsers() {
        List<UserEntity> list = userManagerSingletonLocal.getListUsers();
        return new ResponseService<List<UserEntity>>(list, list != null);
    }

    @Override
    public ResponseService suspendUser(String username, String reasonSuspension) {
        UserEntity userEntity = userManagerSingletonLocal.findUserByUsername(username);
        ResponseService responseService = new ResponseService(false);
        if (userEntity != null) {
            if (userEntity.getUserType() != UserType.ADMIN && userEntity.getUserStateType() != UserStateType.SUSPENDED_ADMIN) {
                userManagerSingletonLocal.suspendUser(username, reasonSuspension);
                responseService = new ResponseService(true);
            } else if (userEntity.getUserType() == UserType.ADMIN) {
                responseService = new ResponseService(ResponseService.RESPONSE_CODE_ERROR_NO_PERMISSIONS);
            } else if (userEntity.getUserStateType() == UserStateType.SUSPENDED_ADMIN) {
                responseService = new ResponseService(ResponseService.RESPONSE_CODE_INVALID_DATA);
            }
        } else {
            responseService = new ResponseService(ResponseService.RESPONSE_CODE_NOT_FOUND);
        }

        if (responseService.isSuccess()) {
            newsletterManagerSingletonLocal.addNewsletter(NewsletterUtil.generateUserNewsletterEntity(NewsletterMessageType.USER_SUSPENDED_BY_ADMIN, userEntity));
        }

        return responseService;
    }

    @Override
    public ResponseService reactivateUser(String username) {
        return userManagerSingletonLocal.reactivateUser(username);
    }

    @Override
    public ResponseService<UserEntity> findUser(String userName) {
        UserEntity userEntity = userManagerSingletonLocal.findUserByUsername(userName);

        return new ResponseService<UserEntity>(userEntity, userEntity != null ? ResponseService.RESPONSE_CODE_SUCCESS : ResponseService.RESPONSE_CODE_NOT_FOUND);
    }

    @Override
    public ResponseService<List<ItemEntity>> getListItems() {
        return new ResponseService<List<ItemEntity>>(itemsManagerSingletonLocal.getListItems());
    }

    @Override
    public ResponseService<ItemEntity> findItemById(int itemId) {
        ResponseService responseService = new ResponseService<ItemEntity>(itemsManagerSingletonLocal.findItemById(itemId));
        if (!responseService.isSuccess()) {
            responseService.setResponseCode(ResponseService.RESPONSE_CODE_NOT_FOUND);
        }
        return responseService;
    }

    @Override
    public ResponseService updateItemState(int itemId, ItemStateType itemStateType) {
        ResponseService<ItemEntity> responseService = findItemById(itemId);
        ItemEntity itemEntity = responseService.getDataObject();

        if (responseService.isSuccess()) {
            if (responseService.getDataObject().getItemStateType() == ItemStateType.ACTIVE_SELL) {
                responseService.getDataObject().setItemStateType(itemStateType);
                itemsManagerSingletonLocal.putItem(responseService.getDataObject());
                responseService = new ResponseService(true);
            } else {
                responseService = new ResponseService(ResponseService.RESPONSE_CODE_ERROR_NO_PERMISSIONS);
            }
        }

        return responseService;
    }

    @Override
    public ResponseService<List<CategoryEntity>> getListCategories() {
        return new ResponseService<>(categoriesManagerSingletonLocal.getListCategories());
    }

    @Override
    public ResponseService addCategory(String categoryName) {
        return new ResponseService(categoriesManagerSingletonLocal.addCategory(categoryName));
    }

    @Override
    public ResponseService changeCategoryName(int categoryId, String newCategoryName) {
        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setId(categoryId);
        categoryEntity.setName(newCategoryName);

        return categoriesManagerSingletonLocal.updateCategory(categoryEntity);
    }

    @Override
    public List<ReportItemEntity> getListItemsReport() {
        return reportManagerSingletonLocal.getListItemsReport();
    }

    @Override
    public List<ReportUserEntity> getListUsersReport() {
        return reportManagerSingletonLocal.getListUsersReport();
    }

    @Override
    public ResponseService replyUserReport(int reportId, ReportStateType newReportStateType) {
        return reportManagerSingletonLocal.replyUserReport(reportId, newReportStateType);
    }

    @Override
    public ResponseService replyItemReport(int reportId, ReportStateType newReportStateType) {
        return reportManagerSingletonLocal.replyItemReport(reportId, newReportStateType);
    }

    @Override
    public ResponseService cancelItem(int itemId) {
        ResponseService responseService = updateItemState(itemId, ItemStateType.CANCELLED);

        return responseService;
    }

}
