/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.db.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import pt.pd.leilao.server.model.ReportItemEntity;
import pt.pd.leilao.server.model.enums.ReportStateType;

/**
 *
 * @author dfortunato
 */
@Entity
@Table(name = "pd_report_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DBReportItem.findAll", query = "SELECT d FROM DBReportItem d")
    , @NamedQuery(name = "DBReportItem.findById", query = "SELECT d FROM DBReportItem d WHERE d.id = :id")
    , @NamedQuery(name = "DBReportItem.findByMessage", query = "SELECT d FROM DBReportItem d WHERE d.message = :message")
    , @NamedQuery(name = "DBReportItem.findByCurrentStateType", query = "SELECT d FROM DBReportItem d WHERE d.currentStateType = :currentStateType")
    , @NamedQuery(name = "DBReportItem.findByItemId", query = "SELECT d FROM DBReportItem d WHERE d.idItemTarget.id = :itemId")})
public class DBReportItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 256)
    @Column(name = "message")
    private String message;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "current_state_type")
    private String currentStateType;
    @JoinColumn(name = "id_item_target", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private DBItemEntity idItemTarget;
    @JoinColumn(name = "id_user_sent_report", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private DBUserEntity idUserSentReport;

    public DBReportItem() {
    }

    public DBReportItem(Integer id) {
        this.id = id;
    }

    public DBReportItem(Integer id, String message, String currentStateType) {
        this.id = id;
        this.message = message;
        this.currentStateType = currentStateType;
    }

    public DBReportItem(ReportItemEntity reportItemEntity)
    {
        this.id = reportItemEntity.getId();
        this.message = reportItemEntity.getMessage();
        if (reportItemEntity.getIdItemTarget() != null) {
            this.idItemTarget = new DBItemEntity(reportItemEntity.getIdItemTarget());
        }
        if (reportItemEntity.getIdUserSentReport()!= null) {
            this.idUserSentReport = new DBUserEntity(reportItemEntity.getIdUserSentReport());
        }
        this.currentStateType = reportItemEntity.getCurrentStateType().getTypeValue();
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCurrentStateType() {
        return currentStateType;
    }

    public void setCurrentStateType(String currentStateType) {
        this.currentStateType = currentStateType;
    }

    public DBItemEntity getIdItemTarget() {
        return idItemTarget;
    }

    public void setIdItemTarget(DBItemEntity idItemTarget) {
        this.idItemTarget = idItemTarget;
    }

    public DBUserEntity getIdUserSentReport() {
        return idUserSentReport;
    }

    public void setIdUserSentReport(DBUserEntity idUserSentReport) {
        this.idUserSentReport = idUserSentReport;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DBReportItem)) {
            return false;
        }
        DBReportItem other = (DBReportItem) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.pd.leilao.server.services.db.entities.DBReportItem[ id=" + id + " ]";
    }
    
    public ReportItemEntity convertToModelEntity()
    {
        ReportItemEntity reportItemEntity = new ReportItemEntity();
        reportItemEntity.setCurrentStateType(ReportStateType.getType(getCurrentStateType()));
        reportItemEntity.setId(getId());
        reportItemEntity.setIdUserSentReport(getIdUserSentReport().convertToModelEntity());
        reportItemEntity.setIdItemTarget(getIdItemTarget().convertToModelEntity());
        reportItemEntity.setMessage(getMessage());
        
        return reportItemEntity;
    }
    
}
