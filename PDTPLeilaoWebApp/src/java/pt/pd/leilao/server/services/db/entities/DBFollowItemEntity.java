/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.db.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author dfortunato
 */
@Entity
@Table(name = "pd_follow_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DBFollowItemEntity.findAll", query = "SELECT d FROM DBFollowItemEntity d")
    , @NamedQuery(name = "DBFollowItemEntity.findById", query = "SELECT d FROM DBFollowItemEntity d WHERE d.id = :id")
    , @NamedQuery(name = "DBFollowItemEntity.findItemFollowedByUser", query = "SELECT d FROM DBFollowItemEntity d WHERE d.itemId.id = :itemId AND d.userId.id = :userId")
    , @NamedQuery(name = "DBFollowItemEntity.findItemFollows", query = "SELECT d FROM DBFollowItemEntity d WHERE d.itemId.id = :itemId")
    , @NamedQuery(name = "DBFollowItemEntity.findItemsFollowedByUser", query = "SELECT d FROM DBFollowItemEntity d WHERE d.userId.id = :userId")
    , @NamedQuery(name = "DBFollowItemEntity.findFollowersByItemId", query = "SELECT d FROM DBFollowItemEntity d WHERE d.itemId.id = :itemId")})
public class DBFollowItemEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "item_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private DBItemEntity itemId;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private DBUserEntity userId;

    public DBFollowItemEntity() {
    }

    public DBFollowItemEntity(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public DBItemEntity getItemId() {
        return itemId;
    }

    public void setItemId(DBItemEntity itemId) {
        this.itemId = itemId;
    }

    public DBUserEntity getUserId() {
        return userId;
    }

    public void setUserId(DBUserEntity userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DBFollowItemEntity)) {
            return false;
        }
        DBFollowItemEntity other = (DBFollowItemEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.pd.leilao.server.services.db.entities.DBFollowItemEntity[ id=" + id + " ]";
    }
    
}
