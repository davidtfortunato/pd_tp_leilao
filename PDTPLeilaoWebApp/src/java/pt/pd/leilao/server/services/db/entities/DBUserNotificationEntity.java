/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.db.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.model.UserNotificationEntity;

/**
 *
 * @author dfortunato
 */
@Entity
@Table(name = "pd_user_notifs")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DBUserNotificationEntity.findAll", query = "SELECT d FROM DBUserNotificationEntity d")
    , @NamedQuery(name = "DBUserNotificationEntity.findById", query = "SELECT d FROM DBUserNotificationEntity d WHERE d.id = :id")
    , @NamedQuery(name = "DBUserNotificationEntity.findByMessage", query = "SELECT d FROM DBUserNotificationEntity d WHERE d.message = :message")
    , @NamedQuery(name = "DBUserNotificationEntity.findByDateSent", query = "SELECT d FROM DBUserNotificationEntity d WHERE d.dateSent = :dateSent")
    , @NamedQuery(name = "DBUserNotificationEntity.findByUserId", query = "SELECT d FROM DBUserNotificationEntity d WHERE d.idUserNotified.id = :userId ORDER BY d.dateSent DESC")})
public class DBUserNotificationEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 256)
    @Column(name = "message")
    private String message;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_sent")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateSent;
    @JoinColumn(name = "id_item_reference", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private DBItemEntity idItemReference;
    @JoinColumn(name = "id_user_notified", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private DBUserEntity idUserNotified;

    public DBUserNotificationEntity() {
    }

    public DBUserNotificationEntity(Integer id) {
        this.id = id;
    }

    public DBUserNotificationEntity(Integer id, String message, Date dateSent) {
        this.id = id;
        this.message = message;
        this.dateSent = dateSent;
    }

    public DBUserNotificationEntity(UserNotificationEntity userNotificationEntity) {
        this.id = userNotificationEntity.getId();
        this.message = userNotificationEntity.getMessage();
        this.dateSent = new Date(userNotificationEntity.getDateSend());
        if (userNotificationEntity.getItemReference() != null) {
            this.idItemReference = new DBItemEntity(userNotificationEntity.getItemReference());
        }
        if (userNotificationEntity.getUserTargetNotified() != null) {
           this.idUserNotified = new DBUserEntity(userNotificationEntity.getUserTargetNotified()); 
        }
        
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDateSent() {
        return dateSent;
    }

    public void setDateSent(Date dateSent) {
        this.dateSent = dateSent;
    }

    public DBItemEntity getIdItemReference() {
        return idItemReference;
    }

    public void setIdItemReference(DBItemEntity idItemReference) {
        this.idItemReference = idItemReference;
    }

    public DBUserEntity getIdUserNotified() {
        return idUserNotified;
    }

    public void setIdUserNotified(DBUserEntity idUserNotified) {
        this.idUserNotified = idUserNotified;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DBUserNotificationEntity)) {
            return false;
        }
        DBUserNotificationEntity other = (DBUserNotificationEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.pd.leilao.server.services.db.entities.DBUserNotificationEntity[ id=" + id + " ]";
    }
    
    public UserNotificationEntity convertToModelEntity()
    {
        UserNotificationEntity userNotificationEntity = new UserNotificationEntity();
        userNotificationEntity.setDateSend(getDateSent().getTime());
        userNotificationEntity.setId(getId());
        userNotificationEntity.setItemReference(getIdItemReference().convertToModelEntity());
        userNotificationEntity.setMessage(getMessage());
        userNotificationEntity.setUserTargetNotified(getIdUserNotified().convertToModelEntity());
        
        return userNotificationEntity;
    }
    
}
