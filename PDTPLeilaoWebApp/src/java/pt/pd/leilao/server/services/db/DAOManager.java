/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.db;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author dfortunato
 */
@Singleton
public class DAOManager implements DAOManagerLocal {
    
    @PersistenceContext(unitName = "PDTPLeilaoWebAppPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
}
