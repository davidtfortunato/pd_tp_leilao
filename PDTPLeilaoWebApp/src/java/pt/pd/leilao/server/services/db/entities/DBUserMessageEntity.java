/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.db.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import pt.pd.leilao.server.model.UserMessageEntity;

/**
 *
 * @author dfortunato
 */
@Entity
@Table(name = "pd_user_message")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DBUserMessageEntity.findAll", query = "SELECT d FROM DBUserMessageEntity d")
    , @NamedQuery(name = "DBUserMessageEntity.findById", query = "SELECT d FROM DBUserMessageEntity d WHERE d.id = :id")
    , @NamedQuery(name = "DBUserMessageEntity.findByMessage", query = "SELECT d FROM DBUserMessageEntity d WHERE d.message = :message")
    , @NamedQuery(name = "DBUserMessageEntity.findByDateSent", query = "SELECT d FROM DBUserMessageEntity d WHERE d.dateSent = :dateSent")
    , @NamedQuery(name = "DBUserMessageEntity.findUserSentMessages", query = "SELECT d FROM DBUserMessageEntity d WHERE d.userSenderId.username = :username")
    , @NamedQuery(name = "DBUserMessageEntity.findUserReceivedMessages", query = "SELECT d FROM DBUserMessageEntity d WHERE d.userReceiverId.username = :username")})
public class DBUserMessageEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 256)
    @Column(name = "message")
    private String message;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_sent")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateSent;
    @JoinColumn(name = "user_receiver_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private DBUserEntity userReceiverId;
    @JoinColumn(name = "user_sender_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private DBUserEntity userSenderId;

    public DBUserMessageEntity() {
    }

    public DBUserMessageEntity(Integer id) {
        this.id = id;
    }

    public DBUserMessageEntity(Integer id, String message, Date dateSent) {
        this.id = id;
        this.message = message;
        this.dateSent = dateSent;
    }

    public DBUserMessageEntity(UserMessageEntity userMessageEntity)
    {
        this.id = userMessageEntity.getId();
        this.dateSent = new Date(userMessageEntity.getTimeSent());
        this.message = userMessageEntity.getMessage();
        this.userReceiverId = new DBUserEntity(userMessageEntity.getUserReceive());
        this.userSenderId = new DBUserEntity(userMessageEntity.getUserSender());
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDateSent() {
        return dateSent;
    }

    public void setDateSent(Date dateSent) {
        this.dateSent = dateSent;
    }

    public DBUserEntity getUserReceiverId() {
        return userReceiverId;
    }

    public void setUserReceiverId(DBUserEntity userReceiverId) {
        this.userReceiverId = userReceiverId;
    }

    public DBUserEntity getUserSenderId() {
        return userSenderId;
    }

    public void setUserSenderId(DBUserEntity userSenderId) {
        this.userSenderId = userSenderId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DBUserMessageEntity)) {
            return false;
        }
        DBUserMessageEntity other = (DBUserMessageEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.pd.leilao.server.services.db.entities.DBUserMessageEntity[ id=" + id + " ]";
    }
    
    public UserMessageEntity convertToModelEntity()
    {
        UserMessageEntity entity = new UserMessageEntity();
        
        entity.setId(getId());
        entity.setMessage(getMessage());
        entity.setTimeSent(getDateSent().getTime());
        entity.setUserReceive(getUserReceiverId().convertToModelEntity());
        entity.setUserSender(getUserSenderId().convertToModelEntity());
        
        return entity;
    }
    
}
