/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ReportItemEntity;
import pt.pd.leilao.server.model.ReportUserEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.model.enums.ItemStateType;
import pt.pd.leilao.server.model.enums.ReportStateType;
import pt.pd.leilao.server.services.db.DAOManagerLocal;
import pt.pd.leilao.server.services.db.entities.DBItemEntity;
import pt.pd.leilao.server.services.db.entities.DBReportItem;
import pt.pd.leilao.server.services.db.entities.DBReportUser;
import pt.pd.leilao.server.services.db.entities.DBUserEntity;

/**
 *
 * @author dfortunato
 */
@Singleton
public class ReportManagerSingleton implements ReportManagerSingletonLocal {

    @EJB
    DAOManagerLocal dAOManagerLocal;
    
    @EJB
    UserManagerSingletonLocal userManagerSingletonLocal;
    
    @EJB
    ItemsManagerSingletonLocal itemsManagerSingletonLocal;
    
    @Override
    public List<ReportUserEntity> getListUsersReport() {
        List<DBReportUser> listDBReports = dAOManagerLocal.getEntityManager().createNamedQuery("DBReportUser.findAll").getResultList();
        List<ReportUserEntity> listUserEntities = new ArrayList<>();
        for (DBReportUser listDBReport : listDBReports) {
            listUserEntities.add(listDBReport.convertToModelEntity());
        }
        return listUserEntities;
    }

    @Override
    public List<ReportItemEntity> getListItemsReport() {
        List<DBReportItem> listDBReports = dAOManagerLocal.getEntityManager().createNamedQuery("DBReportItem.findAll").getResultList();
        List<ReportItemEntity> listUserEntities = new ArrayList<>();
        for (DBReportItem listDBReport : listDBReports) {
            listUserEntities.add(listDBReport.convertToModelEntity());
        }
        return listUserEntities;
    }

    private List<DBReportItem> getListItemReportsById(int itemId)
    {
        return dAOManagerLocal.getEntityManager().createNamedQuery("DBReportItem.findByItemId").setParameter("itemId", itemId).getResultList();
    }
    
    private List<DBReportUser> getListUserReportsById(int userId)
    {
        return dAOManagerLocal.getEntityManager().createNamedQuery("DBReportUser.findByUserId").setParameter("userId", userId).getResultList();
    }
    
    @Override
    public ResponseService replyUserReport(int reportId, ReportStateType newReportStateType) {
        ReportUserEntity reportUserEntity = findReportUserEntityById(reportId);
        
        if (reportUserEntity.getCurrentStateType() == ReportStateType.PENDING) 
        {
            if (newReportStateType == ReportStateType.ACCEPTED) {
                // Suspend User
                ResponseService responseService = userManagerSingletonLocal.suspendUser(reportUserEntity.getIdUserTarget().getUserName(), reportUserEntity.getMessage());
                
                // Accept others reports for the same user
                List<DBReportUser> listReportUsers = getListUserReportsById(reportUserEntity.getIdUserTarget().getId());
                for (DBReportUser listReportUser : listReportUsers) {
                    // Update state
                    listReportUser.setCurrentStateType(newReportStateType.getTypeValue());
                    dAOManagerLocal.getEntityManager().merge(listReportUser);
                }
            }
            else
            {
                DBReportUser dbReportUser = new DBReportUser(reportUserEntity);
                dAOManagerLocal.getEntityManager().merge(dbReportUser);
            }
            return new ResponseService(true);
        }
        else
        {
            return new ResponseService<>(ResponseService.RESPONSE_CODE_ERROR_ALREADY_EXISTS);
        }
    }

    @Override
    public ResponseService replyItemReport(int reportId, ReportStateType newReportStateType) {
        ReportItemEntity reportItemEntity = findReportItemEntityById(reportId);
        
        if (reportItemEntity.getCurrentStateType() == ReportStateType.PENDING) 
        {
            if (newReportStateType == ReportStateType.ACCEPTED) {
                // Cancel item
                ItemEntity itemEntity = reportItemEntity.getIdItemTarget();
                itemEntity.setItemStateType(ItemStateType.CANCELLED);
                itemsManagerSingletonLocal.putItem(itemEntity);
                
                // Accept others reports for the same user
                List<DBReportItem> listReportUsers = getListItemReportsById(itemEntity.getId());
                for (DBReportItem dBReportItem : listReportUsers) {
                    // Update state
                    dBReportItem.setCurrentStateType(newReportStateType.getTypeValue());
                    dAOManagerLocal.getEntityManager().merge(dBReportItem);
                }
            }
            else
            {
                DBReportItem dbReportItem = new DBReportItem(reportItemEntity);
                dAOManagerLocal.getEntityManager().merge(dbReportItem);
            }
            return new ResponseService(true);
        }
        else
        {
            return new ResponseService<>(ResponseService.RESPONSE_CODE_ERROR_ALREADY_EXISTS);
        }
    }

    @Override
    public ReportUserEntity findReportUserEntityById(int reportId) {
        List<DBReportUser> listDBReports = dAOManagerLocal.getEntityManager().createNamedQuery("DBReportUser.findById").setParameter("id", reportId).getResultList();
        if (listDBReports != null && !listDBReports.isEmpty()) {
            return listDBReports.get(0).convertToModelEntity();
        }
        else
        {
            return null;
        }
    }

    @Override
    public ReportItemEntity findReportItemEntityById(int reportId) {
        List<DBReportItem> listDBReports = dAOManagerLocal.getEntityManager().createNamedQuery("DBReportItem.findById").setParameter("id", reportId).getResultList();
        if (listDBReports != null && !listDBReports.isEmpty()) {
            return listDBReports.get(0).convertToModelEntity();
        }
        else
        {
            return null;
        }
    }

    @Override
    public ResponseService addReportItem(int userReporterId, int itemId, String reason) {
        ItemEntity targetItem = itemsManagerSingletonLocal.findItemById(itemId);
        UserEntity userReporter = userManagerSingletonLocal.findUserByUserId(userReporterId);
        DBReportItem reportItem = new DBReportItem();
        reportItem.setCurrentStateType(ReportStateType.PENDING.getTypeValue());
        reportItem.setIdItemTarget(new DBItemEntity(targetItem));
        reportItem.setIdUserSentReport(new DBUserEntity(userReporter));
        reportItem.setMessage(reason);
        dAOManagerLocal.getEntityManager().persist(reportItem);
        
        return new ResponseService(true);
    }

    @Override
    public ResponseService addReportUser(int userReporterId, int userTargetId, String reason) {
        UserEntity userTarget = userManagerSingletonLocal.findUserByUserId(userTargetId);
        UserEntity userReporter = userManagerSingletonLocal.findUserByUserId(userReporterId);
        DBReportUser reportUser = new DBReportUser();
        reportUser.setCurrentStateType(ReportStateType.PENDING.getTypeValue());
        reportUser.setIdUserTarget(new DBUserEntity(userTarget));
        reportUser.setIdUserSentReport(new DBUserEntity(userReporter));
        reportUser.setMessage(reason);
        dAOManagerLocal.getEntityManager().persist(reportUser);
        
        return new ResponseService(true);
    }

    
    
}
