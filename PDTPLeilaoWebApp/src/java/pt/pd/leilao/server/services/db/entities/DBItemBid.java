/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.db.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import pt.pd.leilao.server.model.ItemBidEntity;

/**
 *
 * @author dfortunato
 */
@Entity
@Table(name = "pd_item_bid")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DBItemBid.findAll", query = "SELECT d FROM DBItemBid d")
    , @NamedQuery(name = "DBItemBid.findById", query = "SELECT d FROM DBItemBid d WHERE d.id = :id")
    , @NamedQuery(name = "DBItemBid.findByValueBid", query = "SELECT d FROM DBItemBid d WHERE d.valueBid = :valueBid")
    , @NamedQuery(name = "DBItemBid.findByBidDate", query = "SELECT d FROM DBItemBid d WHERE d.bidDate = :bidDate")
    , @NamedQuery(name = "DBItemBid.findByItemId", query = "SELECT d FROM DBItemBid d WHERE d.itemId.id = :itemId ORDER BY d.valueBid DESC")})
public class DBItemBid implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "value_bid")
    private BigDecimal valueBid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "bid_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date bidDate;
    @JoinColumn(name = "item_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private DBItemEntity itemId;
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private DBUserEntity userId;

    public DBItemBid() {
    }

    public DBItemBid(Integer id) {
        this.id = id;
    }

    public DBItemBid(Integer id, BigDecimal valueBid, Date bidDate) {
        this.id = id;
        this.valueBid = valueBid;
        this.bidDate = bidDate;
    }

    public DBItemBid(ItemBidEntity itemBidEntity)
    {
        this.id = itemBidEntity.getId();
        this.valueBid = new BigDecimal(String.valueOf(itemBidEntity.getValueBid()));
        this.bidDate = new Date(itemBidEntity.getBidDate());
        this.itemId = new DBItemEntity(itemBidEntity.getItemTarget());
        this.userId = new DBUserEntity(itemBidEntity.getUserBuyer());
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getValueBid() {
        return valueBid;
    }

    public void setValueBid(BigDecimal valueBid) {
        this.valueBid = valueBid;
    }

    public Date getBidDate() {
        return bidDate;
    }

    public void setBidDate(Date bidDate) {
        this.bidDate = bidDate;
    }

    public DBItemEntity getItemId() {
        return itemId;
    }

    public void setItemId(DBItemEntity itemId) {
        this.itemId = itemId;
    }

    public DBUserEntity getUserId() {
        return userId;
    }

    public void setUserId(DBUserEntity userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DBItemBid)) {
            return false;
        }
        DBItemBid other = (DBItemBid) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.pd.leilao.server.services.db.entities.DBItemBid[ id=" + id + " ]";
    }
 
    public ItemBidEntity convertToModelEntity()
    {
        ItemBidEntity itemBidEntity = new ItemBidEntity();
        itemBidEntity.setBidDate(getBidDate().getTime());
        itemBidEntity.setId(getId());
        itemBidEntity.setItemTarget(getItemId().convertToModelEntity());
        itemBidEntity.setUserBuyer(getUserId().convertToModelEntity());
        itemBidEntity.setValueBid(getValueBid().floatValue());
        
        return itemBidEntity;
    }
    
}
