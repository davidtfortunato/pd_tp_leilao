/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.db.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import pt.pd.leilao.server.model.NewsletterMessageEntity;
import pt.pd.leilao.server.model.enums.NewsletterMessageType;

/**
 *
 * @author dfortunato
 */
@Entity
@Table(name = "pd_newsletter")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DBNewsletterEntity.findAll", query = "SELECT d FROM DBNewsletterEntity d ORDER BY d.id DESC")
    , @NamedQuery(name = "DBNewsletterEntity.findById", query = "SELECT d FROM DBNewsletterEntity d WHERE d.id = :id")
    , @NamedQuery(name = "DBNewsletterEntity.findByNewsletterMessageType", query = "SELECT d FROM DBNewsletterEntity d WHERE d.newsletterMessageType = :newsletterMessageType")
    , @NamedQuery(name = "DBNewsletterEntity.findByMessage", query = "SELECT d FROM DBNewsletterEntity d WHERE d.message = :message")
    , @NamedQuery(name = "DBNewsletterEntity.findByTargetId", query = "SELECT d FROM DBNewsletterEntity d WHERE d.targetId = :targetId")
    , @NamedQuery(name = "DBNewsletterEntity.findByDateCreated", query = "SELECT d FROM DBNewsletterEntity d WHERE d.dateCreated = :dateCreated")})
public class DBNewsletterEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "newsletter_message_type")
    private String newsletterMessageType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 256)
    @Column(name = "message")
    private String message;
    @Column(name = "target_id")
    private Integer targetId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_created")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;

    public DBNewsletterEntity() {
    }

    public DBNewsletterEntity(Integer id) {
        this.id = id;
    }

    public DBNewsletterEntity(Integer id, String newsletterMessageType, String message, Date dateCreated) {
        this.id = id;
        this.newsletterMessageType = newsletterMessageType;
        this.message = message;
        this.dateCreated = dateCreated;
    }
    
    public DBNewsletterEntity(NewsletterMessageEntity entity)
    {
        this.setDateCreated(new Date(entity.getTimeCreated()));
        this.setMessage(entity.getMessage());
        this.setNewsletterMessageType(entity.getNewsletterMessageType().getTypeValue());
        this.setTargetId(entity.getTargetId());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNewsletterMessageType() {
        return newsletterMessageType;
    }

    public void setNewsletterMessageType(String newsletterMessageType) {
        this.newsletterMessageType = newsletterMessageType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getTargetId() {
        return targetId;
    }

    public void setTargetId(Integer targetId) {
        this.targetId = targetId;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DBNewsletterEntity)) {
            return false;
        }
        DBNewsletterEntity other = (DBNewsletterEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.pd.leilao.server.services.db.entities.DBNewsletterEntity[ id=" + id + " ]";
    }
 
    /**
     * Convert the original DB Entity into a model data entity
     * @return Entity converted
     */
    public NewsletterMessageEntity convertToNewsletterEntity()
    {
        NewsletterMessageEntity entity = new NewsletterMessageEntity();
        entity.setId(getId());
        entity.setNewsletterMessageType(NewsletterMessageType.getType(getNewsletterMessageType()));
        entity.setTargetId(getTargetId());
        entity.setTimeCreated(getDateCreated().getTime());
        entity.setMessage(getMessage());
        
        return entity;
    }
    
}
