/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.UserActivationOrderEntity;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.model.UserNotificationEntity;
import pt.pd.leilao.server.model.enums.AdminOrderResponseType;
import pt.pd.leilao.server.model.enums.AdminOrderType;
import pt.pd.leilao.server.model.enums.NewsletterMessageType;
import pt.pd.leilao.server.model.enums.UserStateType;
import pt.pd.leilao.server.model.enums.UserType;
import pt.pd.leilao.server.model.enums.responses.ResponseUserLoginType;
import pt.pd.leilao.server.model.enums.responses.ResponseUserRegisterType;
import pt.pd.leilao.server.model.responses.ResponseUserLoginData;
import pt.pd.leilao.server.services.ClientUserServiceRemote;
import pt.pd.leilao.server.services.db.DAOManagerLocal;
import pt.pd.leilao.server.services.db.entities.DBUserActivationOrderEntity;
import pt.pd.leilao.server.services.db.entities.DBUserEntity;
import pt.pd.leilao.server.services.db.entities.DBUserNotificationEntity;
import pt.pd.leilao.server.services.utils.FileUtils;
import pt.pd.leilao.server.services.utils.NewsletterUtil;

/**
 *
 * @author dfortunato
 */
@Singleton
public class UserManagerSingleton implements UserManagerSingletonLocal {

    private static final String TAG = UserManagerSingleton.class.getSimpleName();

    public static final String DEFAULT_ADMIN_USERNAME = "admin";
    private static final String DEFAULT_ADMIN_PASSWORD = "admin";

    // Maps
    private Map<String, ClientUserServiceRemote> mapUsersLoggedin; // Map with the current users loggedin. Key = UserName, ClientUserService instance

    @EJB
    NewsletterManagerSingletonLocal newsletterManagerSingletonLocal;
    
    @EJB
    DAOManagerLocal daoManagerLocal;
    
    public UserManagerSingleton() {
        mapUsersLoggedin = new HashMap<>();
    }

    @Override
    public ResponseUserRegisterType registerUser(UserEntity userData) {
        if (checkUserNameExists(userData.getUserName())) {
            return ResponseUserRegisterType.USERNAME_ALREADY_EXISTS;
        }
        
        // Config user data to be added on the list
        userData.setSaldo(0);
        userData.setUserStateType(UserStateType.PENDING_APPROVE);
        userData.setUserType(UserType.GUEST);

        // Add user
        if (userData.isUserDataValid()) 
        {
            daoManagerLocal.getEntityManager().persist(new DBUserEntity(userData));

            // Generate an UserActivationOrderEntity and register user on the list
            UserActivationOrderEntity adminOrder = new UserActivationOrderEntity();
            adminOrder.setDescription("Register a user");
            adminOrder.setAdminOrderType(AdminOrderType.REGISTER);
            adminOrder.setTargetUser(findUserByUsername(userData.getUserName()));

            return addAdminOrder(adminOrder) ? ResponseUserRegisterType.USER_REGISTER_PENDING_APPROVAL : ResponseUserRegisterType.USERDATA_NOT_VALID;
        }
        else
        {
            return ResponseUserRegisterType.USERDATA_NOT_VALID;
        }
    }

    @Override
    public boolean addAdminOrder(UserActivationOrderEntity adminOrder) {
        
        if (!isUserAlreadyPending(adminOrder.getTargetUser().getUserName())) 
        {
            DBUserActivationOrderEntity orderEntity = new DBUserActivationOrderEntity(adminOrder);
            daoManagerLocal.getEntityManager().persist(orderEntity);
            return true;
        }
        else
        {
            return false;
        }
    }
    
    private boolean isUserAlreadyPending(String username)
    {
        try
        {
            return daoManagerLocal.getEntityManager().createNamedQuery("DBUserActivationOrderEntity.findByUserPending").setParameter("username", username).getFirstResult() > 0;
        } catch(Exception e)
        {
            return false;
        }
    }

    @Override
    public ResponseUserLoginData loginUser(ClientUserServiceRemote clientUserServiceRemote, String username, String password) {
        ResponseUserLoginData responseUserLoginData = new ResponseUserLoginData();
        responseUserLoginData.setUserData(findUserByUsername(username));

        if (responseUserLoginData.getUserData() == null) {
            responseUserLoginData.setResponseUserLoginType(ResponseUserLoginType.FAILED_USERNAME_NOT_FOUND);
        } else {
            if (!responseUserLoginData.getUserData().validateCredentials(password)) {
                responseUserLoginData.setResponseUserLoginType(ResponseUserLoginType.FAILED_PASSWORD_WRONG);
            } else if (responseUserLoginData.getUserData().getUserStateType() != UserStateType.ACTIVE) {
                responseUserLoginData.setResponseUserLoginType(ResponseUserLoginType.FAILED_NOT_ACTIVE);
            } else {
                // Check if should force logout before new login
                if (mapUsersLoggedin.containsKey(username)) {
                    forceLogoutUser(username);
                }

                // Add Client Loggedin on Map
                addClientLoggedinOnMap(clientUserServiceRemote, username);

                responseUserLoginData.setResponseUserLoginType(ResponseUserLoginType.LOGIN_SUCCESS);

                Logger.getLogger(TAG).log(Level.INFO, "User logged in: " + username);
            }

        }

        return responseUserLoginData;
    }

    @Override
    public boolean replyAdminUserActivationOrder(int idAdminOrder, AdminOrderResponseType responseType) {
        UserActivationOrderEntity adminOrder = null;

        // Find Admin Order
        List<DBUserActivationOrderEntity> listDBEntities = daoManagerLocal.getEntityManager()
                .createNamedQuery("DBUserActivationOrderEntity.findById")
                .setParameter("id", idAdminOrder)
                .getResultList();
        
        if (listDBEntities != null && !listDBEntities.isEmpty()) 
        {
            adminOrder = listDBEntities.get(0).convertToModelEntity();
            
            if (adminOrder != null && adminOrder.getAdminOrderResponseType() == AdminOrderResponseType.PENDING) {
                
                UserEntity userEntity = findUserByUsername(adminOrder.getTargetUser().getUserName());
                if (userEntity != null) {
                    switch (responseType) {
                        case APPROVED:
                            switch (adminOrder.getAdminOrderType()) {
                                case REACTIVATION:
                                    reactivateUser(userEntity.getUserName());
                                    break;
                                case REGISTER:
                                    newsletterManagerSingletonLocal.addNewsletter(NewsletterUtil.generateUserNewsletterEntity(NewsletterMessageType.USER_APPROVED, userEntity));
                                    userEntity.setUserStateType(UserStateType.ACTIVE);
                                    userEntity.setUserType(UserType.REGISTERED);
                                    putUserEntity(userEntity);
                                    break;
                                case USER_SUSPEND:
                                    suspendUser(userEntity.getUserName(), adminOrder.getDescription());
                                    break;
                            }
                            break;
                        case DECLINED:
                            switch (adminOrder.getAdminOrderType()) {
                                case REACTIVATION:
                                case REGISTER:
                                    userEntity.setUserStateType(UserStateType.SUSPENDED_ADMIN);
                                    userEntity.setUserType(UserType.REGISTERED);
                                    userEntity.setSuspensionReason("Registo invalido");
                                    putUserEntity(userEntity);
                                    newsletterManagerSingletonLocal.addNewsletter(NewsletterUtil.generateUserNewsletterEntity(NewsletterMessageType.USER_NOT_APPROVED, userEntity));
                                    break;
                            }
                            break;
                    }

                    adminOrder.setAdminOrderResponseType(responseType);
                    
                    // Update Admin Order
                    daoManagerLocal.getEntityManager().merge(new DBUserActivationOrderEntity(adminOrder));
                    return true;
                }
            }
        }
        
        return false;
    }

    /**
     * Force logout
     *
     * @param username
     */
    private void forceLogoutUser(String username) {
        ClientUserServiceRemote currentServiceLoggedIn = mapUsersLoggedin.get(username);
        if (currentServiceLoggedIn != null) {
            // Last Service should logout
            currentServiceLoggedIn.logout();
        }
    }

    /**
     * Logout a user
     *
     * @param username
     * @return
     */
    @Override
    public boolean logoutUser(String username) {
        ClientUserServiceRemote currentServiceLoggedIn = mapUsersLoggedin.get(username);
        if (currentServiceLoggedIn != null) {
            // Last Service should logout
            mapUsersLoggedin.remove(username);

            Logger.getLogger(TAG).log(Level.INFO, "User logout: " + username);

            return true;
        }
        return false;
    }

    @Override
    public List<UserEntity> getListUsers() {
        // Get List Users
        List<DBUserEntity> listDBEntities = daoManagerLocal.getEntityManager().createNamedQuery("DBUserEntity.findAll").getResultList();
        List<UserEntity> listEntities = new ArrayList<>();
        for (DBUserEntity listDBEntity : listDBEntities) 
        {
            listEntities.add(listDBEntity.convertToModelEntity());
        }
        return listEntities;
    }

    @Override
    public List<UserActivationOrderEntity> getListAdminUserActivationOrders() {
        List<DBUserActivationOrderEntity> listDBentities = daoManagerLocal.getEntityManager().createNamedQuery("DBUserActivationOrderEntity.findAll").getResultList();
        List<UserActivationOrderEntity> listEntities = new ArrayList<>();
        for (DBUserActivationOrderEntity listDBentity : listDBentities) {
            listEntities.add(listDBentity.convertToModelEntity());
        }
        
        return listEntities;
    }

    /**
     * Add new logged in on map
     *
     * @param clientUserServiceRemote
     * @param username
     */
    private void addClientLoggedinOnMap(ClientUserServiceRemote clientUserServiceRemote, String username) {
        ClientUserServiceRemote currentServiceLoggedIn = mapUsersLoggedin.get(username);
        if (currentServiceLoggedIn != null) {
            // Last Service should logout
            currentServiceLoggedIn.logout();
            mapUsersLoggedin.remove(username);
        }

        // Put New Loggedin
        mapUsersLoggedin.put(username, clientUserServiceRemote);
    }

    /**
     * Check if a given UserName already exists on the list
     *
     * @param userName UserName to be validated
     * @return True if already exists, false otherwise
     */
    private boolean checkUserNameExists(String userName) {
        UserEntity user = findUserByUsername(userName);

        return user != null;
    }

    /**
     * Find a user by his Username
     *
     * @param username Username to find
     * @return UserName object if found, or null if doesn't exists
     */
    @Override
    public UserEntity findUserByUsername(String username) {
        try
        {
            DBUserEntity dBUserEntity = (DBUserEntity) daoManagerLocal.getEntityManager().createNamedQuery("DBUserEntity.findByUsername").setParameter("username", username).getSingleResult();
            return dBUserEntity.convertToModelEntity();
        } catch(Exception e)
        {
            return null;
        }
    }

    /**
     * Add a new user to the map
     *
     * @param userData
     */
    @Override
    public void putUserEntity(UserEntity userData) {
        // Update User
        if (userData != null && userData.isUserDataValid()) 
        {
            daoManagerLocal.getEntityManager().merge(new DBUserEntity(userData));
        }
    }

    @PostConstruct
    public void loadState() {
        Logger.getLogger(TAG).log(Level.INFO, "Init LoadState!");
        
        // Check if already exists an Admin User
        UserEntity adminUser = findUserByUsername(DEFAULT_ADMIN_USERNAME);
        if (adminUser == null) {
            // Generate an Admin User
            adminUser = new UserEntity();
            adminUser.setUserName(DEFAULT_ADMIN_USERNAME);
            adminUser.setPassword(DEFAULT_ADMIN_PASSWORD);
            adminUser.setUserStateType(UserStateType.ACTIVE);
            adminUser.setUserType(UserType.ADMIN);
            adminUser.setSaldo(0);
            adminUser.setNome(DEFAULT_ADMIN_USERNAME);
            adminUser.setMorada("");
            daoManagerLocal.getEntityManager().persist(new DBUserEntity(adminUser));
        }
    }

    @Override
    public ResponseService suspendUser(String username, String reason) {
        UserEntity userEntity = findUserByUsername(username);
        
        ResponseService responseService = new ResponseService(false);
        if (userEntity != null) {
            if (userEntity.getUserType() != UserType.ADMIN && userEntity.getUserStateType() != UserStateType.SUSPENDED_ADMIN) {
                // Suspend it
                userEntity.setSuspensionReason(reason);
                userEntity.setUserStateType(UserStateType.SUSPENDED_ADMIN);
                newsletterManagerSingletonLocal.addNewsletter(NewsletterUtil.generateUserNewsletterEntity(NewsletterMessageType.USER_SUSPENDED_BY_USER, userEntity));
                forceLogoutUser(userEntity.getUserName());
                putUserEntity(userEntity);
                responseService = new ResponseService(true);
            } else if (userEntity.getUserType() == UserType.ADMIN) {
                responseService = new ResponseService(ResponseService.RESPONSE_CODE_ERROR_NO_PERMISSIONS);
            } else if (userEntity.getUserStateType() == UserStateType.SUSPENDED_ADMIN) {
                responseService = new ResponseService(ResponseService.RESPONSE_CODE_INVALID_DATA);
            } 
        } else {
            responseService = new ResponseService(ResponseService.RESPONSE_CODE_NOT_FOUND);
        }
        
        return responseService;
    }

    @Override
    public ResponseService reactivateUser(String username) {
        UserEntity userEntity = findUserByUsername(username);
        
        ResponseService responseService = new ResponseService(false);
        if (userEntity != null) {
            if (userEntity.getUserType() != UserType.ADMIN && userEntity.getUserStateType() != UserStateType.ACTIVE) {
                newsletterManagerSingletonLocal.addNewsletter(NewsletterUtil.generateUserNewsletterEntity(NewsletterMessageType.USER_REACTIVATED, userEntity));
                userEntity.setUserStateType(UserStateType.ACTIVE);
                userEntity.setSuspensionReason("");
                putUserEntity(userEntity);
                responseService = new ResponseService(true);
            }
            else if (userEntity.getUserType() == UserType.ADMIN) {
                responseService = new ResponseService(ResponseService.RESPONSE_CODE_ERROR_NO_PERMISSIONS);
            } else if (userEntity.getUserStateType() == UserStateType.ACTIVE) {
                responseService = new ResponseService(ResponseService.RESPONSE_CODE_INVALID_DATA);
            }
        }  else {
            responseService = new ResponseService(ResponseService.RESPONSE_CODE_NOT_FOUND);
        }
        
        return responseService;
    }

    @Override
    public UserEntity findUserByUserId(int userId) {
        try
        {
            DBUserEntity dBUserEntity = (DBUserEntity) daoManagerLocal.getEntityManager().createNamedQuery("DBUserEntity.findById").setParameter("id", userId).getSingleResult();
            return dBUserEntity.convertToModelEntity();
        } catch(Exception e)
        {
            return null;
        }
    }

    @Override
    public List<UserNotificationEntity> getUserListNotifications(int userId) {
        List<DBUserNotificationEntity> listDBNotifs = daoManagerLocal.getEntityManager().createNamedQuery("DBUserNotificationEntity.findByUserId").setParameter("userId", userId).getResultList();
        List<UserNotificationEntity> listEntities = new ArrayList<>();
        
        for (DBUserNotificationEntity listDBNotif : listDBNotifs) {
            listEntities.add(listDBNotif.convertToModelEntity());
        }
        return listEntities;
    }
}
