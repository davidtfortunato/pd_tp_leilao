/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import pt.pd.leilao.server.model.CategoryEntity;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.services.db.DAOManagerLocal;
import pt.pd.leilao.server.services.db.entities.DBCategoryEntity;
import pt.pd.leilao.server.services.db.entities.DBNewsletterEntity;
import pt.pd.leilao.server.services.utils.FileUtils;

/**
 *
 * @author dfortunato
 */
@Singleton
public class CategoriesManagerSingleton implements CategoriesManagerSingletonLocal {
    private static final String TAG = CategoriesManagerSingleton.class.getName();

    @EJB
    DAOManagerLocal dAOManagerLocal;

    public CategoriesManagerSingleton() {
        
    }

    @Override
    public List<CategoryEntity> getListCategories() {
        List<CategoryEntity> listCategories = new ArrayList<>();
        EntityManager em = dAOManagerLocal.getEntityManager();
        Query q = em.createNamedQuery("DBCategoryEntity.findAll");
        List<DBCategoryEntity> listEntities = q.getResultList();
        for(DBCategoryEntity entity : listEntities)
        {
            listCategories.add(entity.convertToModelEntity());
        }
        return listCategories;
    }

    @Override
    public boolean addCategory(String name) {
        // Check if already exists
        if (findCategoryByName(name) != null) {
            return false;
        } else {
            CategoryEntity categoryEntity = new CategoryEntity();
            categoryEntity.setName(name);
            dAOManagerLocal.getEntityManager().persist(new DBCategoryEntity(categoryEntity));
            return true;
        }
    }

    @Override
    public ResponseService updateCategory(CategoryEntity categoryEntity) {
        CategoryEntity oldCat = findCategoryById(categoryEntity.getId());
        CategoryEntity searchCatName = findCategoryByName(categoryEntity.getName());
        ResponseService responseService = new ResponseService(false);
        if (oldCat == null) {
            responseService.setResponseCode(ResponseService.RESPONSE_CODE_NOT_FOUND);
        }
        else if(searchCatName != null)
        {
            responseService.setResponseCode(ResponseService.RESPONSE_CODE_ERROR_ALREADY_EXISTS);
        } else {
            dAOManagerLocal.getEntityManager().merge(new DBCategoryEntity(categoryEntity));
            responseService.setResponseCode(ResponseService.RESPONSE_CODE_SUCCESS);
        }
        return responseService;
    }

    @Override
    public CategoryEntity findCategoryByName(String categoryName) {
        Query q = dAOManagerLocal.getEntityManager().createNamedQuery("DBCategoryEntity.findByCatName");
        List<DBCategoryEntity> listEntities = q.setParameter("catName", categoryName).getResultList();
        
        if (listEntities != null && !listEntities.isEmpty()) {
            return listEntities.get(0).convertToModelEntity();
        }
        else
        {
            return null;
        }
    }

    public CategoryEntity findCategoryById(int categoryId) {
        Query q = dAOManagerLocal.getEntityManager().createNamedQuery("DBCategoryEntity.findById");
        List<DBCategoryEntity> listEntities = q.setParameter("id", categoryId).getResultList();
        
        if (listEntities != null && !listEntities.isEmpty()) {
            return listEntities.get(0).convertToModelEntity();
        }
        else
        {
            return null;
        }
    }
}
