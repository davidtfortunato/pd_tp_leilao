/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.util.List;
import javax.ejb.Local;
import pt.pd.leilao.server.model.ReportItemEntity;
import pt.pd.leilao.server.model.ReportUserEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.enums.ReportStateType;

/**
 *
 * @author dfortunato
 */
@Local
public interface ReportManagerSingletonLocal {
    
    public List<ReportUserEntity> getListUsersReport();
    
    public ReportUserEntity findReportUserEntityById(int reportId);
    
    public List<ReportItemEntity> getListItemsReport();
    
    public ReportItemEntity findReportItemEntityById(int reportId);
    
    public ResponseService replyUserReport(int reportId, ReportStateType newReportStateType);
    
    public ResponseService replyItemReport(int reportId, ReportStateType newReportStateType);
    
    public ResponseService addReportItem(int userReporterId, int itemId, String reason);
    
    public ResponseService addReportUser(int userReporterId, int userTargetId, String reason);
    
}
