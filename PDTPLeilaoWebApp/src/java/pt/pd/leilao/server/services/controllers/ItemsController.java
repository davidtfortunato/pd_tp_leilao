/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import pt.pd.leilao.server.model.CategoryEntity;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.model.enums.ItemStateType;
import pt.pd.leilao.server.services.ClientItemsServiceRemote;
import pt.pd.leilao.server.services.ClientUserServiceRemote;

/**
 *
 * @author dfortunato
 */
@Named(value = "itemsController")
@RequestScoped
public class ItemsController {

    @EJB
    ClientItemsServiceRemote clientItemsServiceRemote;
    
    private String textToSearch;
    private String currentItemId;
    
    /**
     * Creates a new instance of ItemsController
     */
    public ItemsController() {
        
    }
    
    public List<CategoryEntity> getListCategories()
    {
        return clientItemsServiceRemote.getListCategories();
    }
    
    public List<ItemEntity> getListAllItemsSelling()
    {
        Map<String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String sortSelection = params.get("sortSelection");
        textToSearch = params.get("in_item_search");
        
        // Check if should search for items
        List<ItemEntity> listItems = new ArrayList<>();
        if (textToSearch != null && !textToSearch.isEmpty()) 
        {
            // From Search
            listItems = clientItemsServiceRemote.searchListItemsByText(textToSearch).getDataObject();
        }
        else
        {
            // All items
            listItems = clientItemsServiceRemote.getListItems().getDataObject();
        }
        
        if (sortSelection != null) 
        {
            switch(sortSelection)
            {
                case "sort_description":
                    Collections.sort(listItems, ItemEntity.COMPARATOR_BY_DESCRIPTION);
                    break;
                case "sort_category":
                    Collections.sort(listItems, ItemEntity.COMPARATOR_BY_CATEGORY);
                    break;
                case "sort_initial_price":
                    Collections.sort(listItems, ItemEntity.COMPARATOR_BY_PRICE);
                    break;
                case "sort_buy_now_price":
                    Collections.sort(listItems, ItemEntity.COMPARATOR_BY_BUYNOW_PRICE);
                    break;
                case "sort_enddate":
                    Collections.sort(listItems, ItemEntity.COMPARATOR_BY_ENDDATE);
                    break;
                default:
                    Collections.sort(listItems, ItemEntity.COMPARATOR_BY_ID);
            }
        }
        else
        {
            Collections.sort(listItems, ItemEntity.COMPARATOR_BY_ID);
        }
        
        return listItems;
    }

    public void onSearchClick()
    {
        Map<String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        textToSearch = params.get("in_item_search");
    }
    
    public String getId() {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext extContext = context.getExternalContext();
        Map<String, String> params = extContext.getRequestParameterMap();
        String id = params.get("id");
        if (id == null) 
        {
            id = currentItemId;
        }
        else
        {
            currentItemId = id;
        }
        return id;
    }
    
    public ItemEntity getItemDetails()
    {   
        try
        {
            return clientItemsServiceRemote.findItemEntityById(Integer.valueOf(getId())).getDataObject();
        } catch(Exception e)
        {
            Logger.global.log(Level.SEVERE, e.getMessage());
            return null;
        }
        
    }
    
}
