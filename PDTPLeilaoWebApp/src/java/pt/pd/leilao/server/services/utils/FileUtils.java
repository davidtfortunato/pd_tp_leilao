/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.pd.leilao.server.model.UserEntity;

/**
 *
 * @author dfortunato
 */
public class FileUtils {
    private static final String TAG = FileUtils.class.getSimpleName();
    
    // Version
    private static final String FILES_VERSION = "v9";
    
    public static Object loadData(String fileName)
    {
        fileName += FILES_VERSION;
        Object readObject = null;
        try {
            File file = new File(fileName);
            if (!file.exists()) {
                Logger.getLogger(TAG).log(Level.INFO, "OnLoadData - File : " + file.getAbsolutePath() + " Doesnt exists. It will be created first");
                file.createNewFile();
            }
            else
            {
                Logger.getLogger(TAG).log(Level.INFO, "OnLoadData - File : " + file.getAbsolutePath() + " Already exists");
            }
            
            ObjectInputStream ois;
            ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)));
            readObject = ois.readObject();
            ois.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileUtils.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } catch (IOException ex) {
            Logger.getLogger(FileUtils.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(FileUtils.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
        
        return readObject;
    }
    
    public static void saveData(String filename, Object data)
    {
        filename += FILES_VERSION;
        try {
            File file = new File(filename);
            if (!file.exists()) {
                Logger.getLogger(TAG).log(Level.INFO, "OnSaveData - File : " + file.getAbsolutePath() + " Doesnt exists. It will be created first");
                file.createNewFile();
            }
            else
            {
                Logger.getLogger(TAG).log(Level.INFO, "OnSaveData - File : " + file.getAbsolutePath() + " Already exists");
            }

            ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
            oos.writeObject(data);
            oos.flush();
            oos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileUtils.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } catch (IOException ex) {
            Logger.getLogger(FileUtils.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }
    
}
