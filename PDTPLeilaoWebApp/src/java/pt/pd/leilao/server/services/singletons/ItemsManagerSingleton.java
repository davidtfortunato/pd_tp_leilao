/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import pt.pd.leilao.server.model.ItemBidEntity;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.NewsletterMessageEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.TransactionEntity;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.model.UserNotificationEntity;
import pt.pd.leilao.server.model.enums.ItemStateType;
import pt.pd.leilao.server.model.enums.NewsletterMessageType;
import pt.pd.leilao.server.services.db.DAOManagerLocal;
import pt.pd.leilao.server.services.db.entities.DBItemBid;
import pt.pd.leilao.server.services.db.entities.DBItemEntity;
import pt.pd.leilao.server.services.db.entities.DBUserNotificationEntity;
import pt.pd.leilao.server.services.utils.FileUtils;
import pt.pd.leilao.server.services.utils.NewsletterUtil;

/**
 *
 * @author dfortunato
 */
@Singleton
@Startup
public class ItemsManagerSingleton implements ItemsManagerSingletonLocal {

    private static final String TAG = ItemsManagerSingleton.class.getName();

    private static final long INTERVAL_TIMER = 10000; // each 10 seconds

    @EJB
    NewsletterManagerSingletonLocal newsletterManagerSingletonLocal;

    @EJB
    UserManagerSingletonLocal userManagerSingletonLocal;

    @EJB
    TransactionManagerSingletonLocal transactionManagerSingletonLocal;

    @EJB
    FollowItemManagerSingletonLocal followItemManagerSingletonLocal;

    @EJB
    DAOManagerLocal dAOManagerLocal;

    @Resource
    private TimerService timerService;

    public ItemsManagerSingleton() {
    }

    @PostConstruct
    public void init() {
        timerService.createIntervalTimer(0L,
                INTERVAL_TIMER, new TimerConfig(null, false));
    }

    @Timeout
    public void process(Timer timer) {
        // Validate all items
        validateAllPendentBidItems();
    }

    @Override
    public void putItem(ItemEntity itemEntity) {

        // Check if can be updated
        ItemEntity lastItemEntity = findItemById(itemEntity.getId());
        if (lastItemEntity != null
                && lastItemEntity.getUserBuyer() != null
                && lastItemEntity.getItemStateType() != ItemStateType.ACTIVE_SELL) {
            Logger.getLogger(TAG).log(Level.WARNING, "Item " + itemEntity.getId() + " cannot be edited anymore");
        } else if (lastItemEntity != null) {
            // Update
            itemEntity.setLastUpdate(Calendar.getInstance().getTimeInMillis());

            // Update 
            DBItemEntity dBItemEntity = new DBItemEntity(itemEntity);
            dAOManagerLocal.getEntityManager().merge(dBItemEntity);

            // Notify if was cancelled
            if (lastItemEntity.getItemStateType() != itemEntity.getItemStateType() && itemEntity.getItemStateType() == ItemStateType.CANCELLED) {
                notifItemCancelled(itemEntity);
            }

        } else {
            // Add it
            DBItemEntity dBItemEntity = new DBItemEntity(itemEntity);
            dAOManagerLocal.getEntityManager().persist(dBItemEntity);

            // Notify Newsletter
            newsletterManagerSingletonLocal.addNewsletter(NewsletterUtil.generateItemNewsletterEntity(NewsletterMessageType.ITEM_ADDED_NEW_ITEM, itemEntity));
        }
    }

    @Override
    public List<ItemEntity> getListItems() {
        List<ItemEntity> listItems = new ArrayList<>();
        List<DBItemEntity> listDBItems = dAOManagerLocal.getEntityManager().createNamedQuery("DBItemEntity.findAll").getResultList();

        for (DBItemEntity listDBItem : listDBItems) {
            ItemEntity itemEntity = listDBItem.convertToModelEntity();
            if (itemEntity.getItemStateType() != ItemStateType.CANCELLED) {
                listItems.add(itemEntity);
            }
        }

        return listItems;
    }

    @Override
    public List<ItemEntity> getUserSellerItems(int userId) {
        List<DBItemEntity> listDBItems = dAOManagerLocal.getEntityManager()
                .createNamedQuery("DBItemEntity.findByUserSeller")
                .setParameter("userId", userId)
                .getResultList();
        List<ItemEntity> listUserItems = new ArrayList<>();

        for (DBItemEntity listDBItem : listDBItems) {
            listUserItems.add(listDBItem.convertToModelEntity());
        }
        return listUserItems;
    }

    @Override
    public List<ItemEntity> getUserBoughtItems(int userId) {
        List<DBItemEntity> listDBItems = dAOManagerLocal.getEntityManager()
                .createNamedQuery("DBItemEntity.findByUserBuyer")
                .setParameter("userId", userId)
                .getResultList();
        List<ItemEntity> listUserItems = new ArrayList<>();

        for (DBItemEntity listDBItem : listDBItems) {
            listUserItems.add(listDBItem.convertToModelEntity());
        }
        return listUserItems;
    }

    @Override
    public ItemEntity findItemById(int itemId) {
        List<DBItemEntity> listDBItems = dAOManagerLocal.getEntityManager().createNamedQuery("DBItemEntity.findById").setParameter("id", itemId).getResultList();

        if (listDBItems != null && !listDBItems.isEmpty()) {
            return listDBItems.get(0).convertToModelEntity();
        } else {
            return null;
        }
    }

    @Override
    public List<ItemEntity> searchItemsByText(String textToSearch) {
        List<DBItemEntity> listDBItems = dAOManagerLocal.getEntityManager()
                .createNamedQuery("DBItemEntity.searchByText")
                .setParameter("textToSearch", "%" + textToSearch + "%")
                .getResultList();
        List<ItemEntity> listItems = new ArrayList<>();
        for (DBItemEntity listDBItem : listDBItems) {
            listItems.add(listDBItem.convertToModelEntity());
        }
        return listItems;
    }

    @Override
    public ResponseService<ItemBidEntity> putItemBid(int itemId, int userId, float valueBid) {

        ItemEntity itemEntity = findItemById(itemId);
        UserEntity userBuyer = userManagerSingletonLocal.findUserByUserId(userId);

        // Check if have money
        if (userBuyer.getSaldo() < valueBid) {
            return new ResponseService<>(ResponseService.RESPONSE_CODE_ERROR_MONEY_NOT_AVAILABLE);
        } else if (!itemEntity.isDateValid()) {
            return new ResponseService<>(ResponseService.RESPONSE_CODE_ERROR_EXPIRED);
        } else if (itemEntity.getMinimumBidValue() > valueBid) {
            return new ResponseService<>(ResponseService.RESPONSE_CODE_INVALID_DATA);
        } else {
            // Init item bid entity
            ItemBidEntity itemBidEntity = new ItemBidEntity();
            itemBidEntity.setItemTarget(itemEntity);
            itemBidEntity.setBidDate(Calendar.getInstance().getTimeInMillis());
            itemBidEntity.setValueBid(valueBid);
            itemBidEntity.setUserBuyer(userBuyer);

            // Check if value bid is the buynow value
            if (itemEntity.getBuyNowPrice() > 0 && valueBid >= itemEntity.getBuyNowPrice()) {
                // Process Sell Buy Now
                ResponseService responseTransaction = processItemSell(itemBidEntity);
                if (responseTransaction.isSuccess()) {
                    return new ResponseService<>(itemBidEntity);
                } else {
                    return new ResponseService<>(responseTransaction.getResponseCode());
                }
            } else {
                // Add update last price
                itemEntity.setLastPrice(valueBid);
                itemEntity.setLastUpdate(Calendar.getInstance().getTimeInMillis());

                // Persist data
                dAOManagerLocal.getEntityManager().persist(new DBItemBid(itemBidEntity));
                dAOManagerLocal.getEntityManager().merge(new DBItemEntity(itemEntity));

                // Notify all users
                notifItemNewBid(itemBidEntity);

                return new ResponseService<>(itemBidEntity);
            }
        }

    }

    private ResponseService<TransactionEntity> processItemSell(ItemBidEntity itemBidEntity) {
        ResponseService<TransactionEntity> responseTransaction = transactionManagerSingletonLocal.processTransaction(itemBidEntity);
        if (responseTransaction.isSuccess()) {
            ItemEntity itemEntity = findItemById(itemBidEntity.getItemTarget().getId());
            notifItemSold(itemEntity);
            newsletterManagerSingletonLocal.addNewsletter(NewsletterUtil.generateItemNewsletterEntity(NewsletterMessageType.ITEM_SOLD, itemEntity));
        }

        return responseTransaction;
    }

    @Override
    public List<ItemBidEntity> getListItemBids(int itemId) {
        List<DBItemBid> listDBItemBids = dAOManagerLocal.getEntityManager().createNamedQuery("DBItemBid.findByItemId").setParameter("itemId", itemId).getResultList();
        List<ItemBidEntity> listBids = new ArrayList<>();
        for (DBItemBid listDBItemBid : listDBItemBids) {
            listBids.add(listDBItemBid.convertToModelEntity());
        }
        return listBids;
    }

    @Override
    public ItemBidEntity getHighestItemBid(int itemId) {
        List<ItemBidEntity> listBids = getListItemBids(itemId);
        if (listBids != null && !listBids.isEmpty()) {
            return listBids.get(0);
        }

        return null;
    }

    @Override
    public void validateAllPendentBidItems() {
        List<DBItemEntity> listDBItemsSelling = dAOManagerLocal.getEntityManager().createNamedQuery("DBItemEntity.findByItemStateType").setParameter("itemStateType", ItemStateType.ACTIVE_SELL.getTypeValue()).getResultList();

        for (DBItemEntity dBItemEntity : listDBItemsSelling) {
            ItemEntity itemEntity = dBItemEntity.convertToModelEntity();

            if (!itemEntity.isDateValid()) {
                // Process sell
                ItemBidEntity highestItemBid = getHighestItemBid(itemEntity.getId());
                if (highestItemBid != null) {
                    ResponseService responseService = processItemSell(highestItemBid);
                    Logger.getGlobal().log(Level.INFO, "Item sold - ID: " + highestItemBid.getItemTarget().getId());
                } else {
                    // Close Item - Expired
                    itemEntity.setItemStateType(ItemStateType.DATE_EXPIRED);
                    itemEntity.setLastUpdate(Calendar.getInstance().getTimeInMillis());
                    dAOManagerLocal.getEntityManager().merge(new DBItemEntity(itemEntity));

                    // Notify item expired
                    notifItemExpired(itemEntity);
                }
            }
        }
    }

    private List<UserEntity> findListItemUsersToNotify(int itemId) {
        Map<Integer, UserEntity> mapUsers = new HashMap<>();

        // Item Followers
        List<UserEntity> listFollowers = followItemManagerSingletonLocal.getListUsersFollowingItem(itemId).getDataObject();
        for (UserEntity userFollower : listFollowers) {
            mapUsers.put(userFollower.getId(), userFollower);
        }

        // users that already put a bid on the item
        List<ItemBidEntity> listBids = getListItemBids(itemId);
        for (ItemBidEntity itemBid : listBids) {
            mapUsers.put(itemBid.getUserBuyer().getId(), itemBid.getUserBuyer());
        }

        return new ArrayList<>(mapUsers.values());
    }

    @Override
    public void notifItemSold(ItemEntity itemEntity) {
        if (itemEntity.getUserBuyer() != null) {
            List<UserEntity> listUsersToNotify = findListItemUsersToNotify(itemEntity.getId());
            // Also notify seller
            listUsersToNotify.add(itemEntity.getUserSeller());
            for (UserEntity userEntity : listUsersToNotify) {
                UserNotificationEntity notifEntity = new UserNotificationEntity();
                notifEntity.setDateSend(Calendar.getInstance().getTimeInMillis());
                notifEntity.setItemReference(itemEntity);
                notifEntity.setMessage("O item #" + itemEntity.getId() + " foi vendido por " + itemEntity.getLastPriceMoney() + ". Foi adquirido por: " + itemEntity.getUserBuyerUsername());
                notifEntity.setUserTargetNotified(userEntity);
                dAOManagerLocal.getEntityManager().persist(new DBUserNotificationEntity(notifEntity));
            }
        }
    }

    @Override
    public void notifItemNewBid(ItemBidEntity itemBidEntity) {
        if (itemBidEntity.getUserBuyer() != null) {
            List<UserEntity> listUsersToNotify = findListItemUsersToNotify(itemBidEntity.getItemTarget().getId());
            // Also notify seller
            listUsersToNotify.add(itemBidEntity.getItemTarget().getUserSeller());
            for (UserEntity userEntity : listUsersToNotify) {
                if (userEntity.getId() != itemBidEntity.getUserBuyer().getId()) {
                    UserNotificationEntity notifEntity = new UserNotificationEntity();
                    notifEntity.setDateSend(Calendar.getInstance().getTimeInMillis());
                    notifEntity.setItemReference(itemBidEntity.getItemTarget());
                    notifEntity.setMessage("Foi feita uma nova oferta pelo item #" + itemBidEntity.getItemTarget().getId() + " no valor de " + itemBidEntity.getValueBid() + ". A oferta foi feito por: " + itemBidEntity.getUserBuyer().getUserName());
                    notifEntity.setUserTargetNotified(userEntity);
                    dAOManagerLocal.getEntityManager().persist(new DBUserNotificationEntity(notifEntity));
                }
            }
        }
    }

    @Override
    public void notifItemExpired(ItemEntity itemEntity) {
        List<UserEntity> listUsersToNotify = findListItemUsersToNotify(itemEntity.getId());
        // Also notify seller
        listUsersToNotify.add(itemEntity.getUserSeller());

        for (UserEntity userEntity : listUsersToNotify) {
            UserNotificationEntity notifEntity = new UserNotificationEntity();
            notifEntity.setDateSend(Calendar.getInstance().getTimeInMillis());
            notifEntity.setItemReference(itemEntity);
            notifEntity.setMessage("O item #" + itemEntity.getId() + " expirou sem nenhuma oferta.");
            notifEntity.setUserTargetNotified(userEntity);
            dAOManagerLocal.getEntityManager().persist(new DBUserNotificationEntity(notifEntity));
        }
    }

    public void notifItemCancelled(ItemEntity itemEntity) {
        List<UserEntity> listUsersToNotify = findListItemUsersToNotify(itemEntity.getId());
        // Also notify seller
        listUsersToNotify.add(itemEntity.getUserSeller());

        for (UserEntity userEntity : listUsersToNotify) {
            UserNotificationEntity notifEntity = new UserNotificationEntity();
            notifEntity.setDateSend(Calendar.getInstance().getTimeInMillis());
            notifEntity.setItemReference(itemEntity);
            notifEntity.setMessage("O item #" + itemEntity.getId() + " foi cancelado pelo administrador");
            notifEntity.setUserTargetNotified(userEntity);
            dAOManagerLocal.getEntityManager().persist(new DBUserNotificationEntity(notifEntity));
        }
        newsletterManagerSingletonLocal.addNewsletter(NewsletterUtil.generateItemNewsletterEntity(NewsletterMessageType.ITEM_CANCELLED_BY_ADMIN, itemEntity));
    }
}
