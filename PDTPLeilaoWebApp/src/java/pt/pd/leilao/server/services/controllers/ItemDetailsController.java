/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.controllers;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import pt.pd.leilao.server.model.ItemBidEntity;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.services.ClientItemsServiceRemote;
import pt.pd.leilao.server.services.utils.AlertMessagesUtil;

/**
 *
 * @author dfortunato
 */
@Named(value = "itemDetailsController")
@RequestScoped
public class ItemDetailsController implements Serializable{

    @Inject
    private UserController userController;
    
    @EJB
    private ClientItemsServiceRemote clientItemsServiceRemote;
        
    private int itemId = -1;
    
    
    /**
     * Creates a new instance of ItemDetailsController
     */
    public ItemDetailsController() {
    }
    
    public ItemEntity getItemDetails()
    {
        if (itemId == -1) {
            Map<String,String> mapParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            String id = mapParams.get("id");
            itemId = Integer.valueOf(id);
        }
        return clientItemsServiceRemote.findItemEntityById(itemId).getDataObject();
    }
    
    public boolean isAllowedToBuy()
    {
        return userController.isAllowedToBuyItem(getItemDetails());
    }
    
    public boolean isAllowedToBuyNow()
    {
        return userController.isAllowedToBuyItem(getItemDetails()) && getItemDetails().isBuyNowAllowed();
    }

    public void setUserController(UserController userController) {
        this.userController = userController;
    }
    
    public String getFollowButtonTitle()
    {
        if (userController.isFollowingItem(getItemDetails().getId())) 
        {
            return "Deixar de seguir";
        }
        else
        {
            return "Seguir item";
        }
    }
    
    public void toggleFollowingItem()
    {
        userController.toggleFollowItem(getItemDetails().getId());
    }
    
    public void sendMessageToSeller()
    {
        Map<String,String> mapParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String message = mapParams.get("form_item_details:form_input_text:item_text_area");
        userController.sendMessageToSeller(message, getItemDetails());
    }
    
    public void reportUserSeller()
    {
        Map<String,String> mapParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String message = mapParams.get("form_item_details:form_input_text:item_text_area");
        userController.reportSellerAbuse(message, getItemDetails());
    }
    
    public void reportItemSeller()
    {
        Map<String,String> mapParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String message = mapParams.get("form_item_details:form_input_text:item_text_area");
        userController.reportItemAbuse(message, getItemDetails());
    }
    
    public void buyNowItem()
    {
        ResponseService responseService = userController.buyItem(getItemDetails().getId(), getItemDetails().getBuyNowPrice());
        
        if (responseService.isSuccess()) 
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "O item foi adquirido com sucesso!");
        }
        else
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "Não foi possivel adquirir o item");
        }
    }
    
    public String putNewItemBid()
    {
        String valueBid = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("in_licitacao");
        
        ResponseService responseService = userController.buyItem(getItemDetails().getId(), Float.valueOf(valueBid));
        
        if (responseService.isSuccess()) 
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "A sua licitacao foi feita com sucesso");
        }
        else
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "Não foi possivel licitar o item");
        }
        
        return null;
    }
    
    public List<ItemBidEntity> getListItemBids()
    {
        List<ItemBidEntity> listBids = clientItemsServiceRemote.getListItemBids(itemId);
        
        return listBids;
    }
    
}
