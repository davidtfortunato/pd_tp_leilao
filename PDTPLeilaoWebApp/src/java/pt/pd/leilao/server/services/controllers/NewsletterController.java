/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.controllers;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import pt.pd.leilao.server.model.NewsletterMessageEntity;
import pt.pd.leilao.server.services.ClientNewsletterServiceRemote;
import pt.pd.leilao.server.services.singletons.NewsletterManagerSingletonLocal;

/**
 *
 * @author dfortunato
 */
@ManagedBean(name = "newsletterController")
@RequestScoped
public class NewsletterController implements Serializable {

    @EJB
    ClientNewsletterServiceRemote clientNewsletterServiceRemote;
    
    /**
     * Creates a new instance of NewsletterController
     */
    public NewsletterController() {
    }
    
    public String getTestMessage()
    {
        return "This message is a test from " + NewsletterController.class.getSimpleName();
    }
    
    public List<NewsletterMessageEntity> getListNewsletter()
    {
        return clientNewsletterServiceRemote.getListNewsletters();
    }
    
}
