/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.controllers;

import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.services.ClientGuestServiceRemote;
import pt.pd.leilao.server.services.utils.AlertMessagesUtil;

/**
 *
 * @author dfortunato
 */
@Named(value = "guestController")
@RequestScoped
public class GuestController {

    @EJB
    ClientGuestServiceRemote clientGuestServiceRemote;
    
    // Data
    private String reactivateUsername;
    private String reactivatePassword;
    private String reactivateReason;
    
    /**
     * Creates a new instance of GuestController
     */
    public GuestController() {
    }
    
    public List<ItemEntity> getLastSoldItems()
    {
        return clientGuestServiceRemote.getLastSoldItems(3).getDataObject();
    }
    
    public String postRequestReactivate()
    {
        ResponseService responseService = clientGuestServiceRemote.postRequestReactivate(getReactivateUsername(), getReactivatePassword(), getReactivateReason());
        
        FacesContext facesContext = FacesContext.getCurrentInstance();
        if (responseService.isSuccess()) {
            AlertMessagesUtil.displayResponseMessage(responseService, "Pedido registado com sucesso");
            return "guest_menu";
        }
        else
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "Não foi possivel registar o seu pedido");
            return null;
        }
    }
    
    public void cleanData()
    {
        setReactivatePassword(null);
        setReactivateReason(null);
        setReactivateUsername(null);
    }
    

    public String getReactivateUsername() {
        return reactivateUsername;
    }

    public void setReactivateUsername(String reactivateUsername) {
        this.reactivateUsername = reactivateUsername;
    }

    public String getReactivatePassword() {
        return reactivatePassword;
    }

    public void setReactivatePassword(String reactivatePassword) {
        this.reactivatePassword = reactivatePassword;
    }

    public String getReactivateReason() {
        return reactivateReason;
    }

    public void setReactivateReason(String reactivateReason) {
        this.reactivateReason = reactivateReason;
    }
    
    
    
}
