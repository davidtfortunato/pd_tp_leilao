/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.utils;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import pt.pd.leilao.server.model.ResponseService;

/**
 *
 * @author dfortunato
 */
public class AlertMessagesUtil {
    
    public static void displayResponseMessage(String message)
    {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, ""));
    }
    
    public static void displayResponseMessage(ResponseService responseService, String message)
    {
        if (responseService.isSuccess()) 
        {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, ""));
        }
        else
        {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, "ErrorCode: " + responseService.getResponseMessage()));
        }
    }
    
}
