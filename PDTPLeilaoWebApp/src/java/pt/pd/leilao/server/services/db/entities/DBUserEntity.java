/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.db.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.UserActivationOrderEntity;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.model.UserMessageEntity;
import pt.pd.leilao.server.model.enums.UserStateType;
import pt.pd.leilao.server.model.enums.UserType;

/**
 *
 * @author dfortunato
 */
@Entity
@Table(name = "pd_user")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DBUserEntity.findAll", query = "SELECT d FROM DBUserEntity d ORDER BY d.id ASC")
    , @NamedQuery(name = "DBUserEntity.findById", query = "SELECT d FROM DBUserEntity d WHERE d.id = :id")
    , @NamedQuery(name = "DBUserEntity.findByUsername", query = "SELECT d FROM DBUserEntity d WHERE d.username = :username")
    , @NamedQuery(name = "DBUserEntity.findByPassword", query = "SELECT d FROM DBUserEntity d WHERE d.password = :password")
    , @NamedQuery(name = "DBUserEntity.findByFullname", query = "SELECT d FROM DBUserEntity d WHERE d.fullname = :fullname")
    , @NamedQuery(name = "DBUserEntity.findByAddress", query = "SELECT d FROM DBUserEntity d WHERE d.address = :address")
    , @NamedQuery(name = "DBUserEntity.findByBalance", query = "SELECT d FROM DBUserEntity d WHERE d.balance = :balance")
    , @NamedQuery(name = "DBUserEntity.findByUserStateType", query = "SELECT d FROM DBUserEntity d WHERE d.userStateType = :userStateType")
    , @NamedQuery(name = "DBUserEntity.findByUserType", query = "SELECT d FROM DBUserEntity d WHERE d.userType = :userType")
    , @NamedQuery(name = "DBUserEntity.findBySuspensionReason", query = "SELECT d FROM DBUserEntity d WHERE d.suspensionReason = :suspensionReason")})
public class DBUserEntity implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "balance")
    private BigDecimal balance;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUserNotified")
    private Collection<DBUserNotificationEntity> dBUserNotificationEntityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUserReceiver")
    private Collection<DBTransactionEntity> dBTransactionEntityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUserSender")
    private Collection<DBTransactionEntity> dBTransactionEntityCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUserSentReport")
    private Collection<DBReportUser> dBReportUserCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUserTarget")
    private Collection<DBReportUser> dBReportUserCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<DBItemBid> dBItemBidCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idUserSentReport")
    private Collection<DBReportItem> dBReportItemCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private Collection<DBFollowItemEntity> dBFollowItemEntityCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "password")
    private String password;
    @Size(max = 100)
    @Column(name = "fullname")
    private String fullname;
    @Size(max = 256)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "user_state_type")
    private String userStateType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "user_type")
    private String userType;
    @Size(max = 256)
    @Column(name = "suspension_reason")
    private String suspensionReason;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userTargetId")
    private Collection<DBUserActivationOrderEntity> dBUserActivationOrderEntityCollection;
    @OneToMany(mappedBy = "userBuyerId")
    private Collection<DBItemEntity> dBItemEntityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userSellerId")
    private Collection<DBItemEntity> dBItemEntityCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userReceiverId")
    private Collection<DBUserMessageEntity> dBUserMessageEntityCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userSenderId")
    private Collection<DBUserMessageEntity> dBUserMessageEntityCollection1;

    public DBUserEntity() {
    }

    public DBUserEntity(Integer id) {
        this.id = id;
    }

    public DBUserEntity(Integer id, String username, String password, BigDecimal balance, String userStateType, String userType) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.balance = balance;
        this.userStateType = userStateType;
        this.userType = userType;
    }
    
    public DBUserEntity(UserEntity userEntity)
    {
        this.id = userEntity.getId();
        this.username = userEntity.getUserName();
        this.address = userEntity.getMorada();
        this.balance = new BigDecimal(String.valueOf(userEntity.getSaldo()));
        this.fullname = userEntity.getNome();
        this.password = userEntity.getPassword();
        this.suspensionReason = userEntity.getSuspensionReason();
        this.userType = userEntity.getUserType().getTypeValue();
        this.userStateType = userEntity.getUserStateType().getTypeValue();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public String getUserStateType() {
        return userStateType;
    }

    public void setUserStateType(String userStateType) {
        this.userStateType = userStateType;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getSuspensionReason() {
        return suspensionReason;
    }

    public void setSuspensionReason(String suspensionReason) {
        this.suspensionReason = suspensionReason;
    }

    @XmlTransient
    public Collection<DBUserActivationOrderEntity> getDBUserActivationOrderEntityCollection() {
        return dBUserActivationOrderEntityCollection;
    }

    public void setDBUserActivationOrderEntityCollection(Collection<DBUserActivationOrderEntity> dBUserActivationOrderEntityCollection) {
        this.dBUserActivationOrderEntityCollection = dBUserActivationOrderEntityCollection;
    }

    @XmlTransient
    public Collection<DBItemEntity> getDBItemEntityCollection() {
        return dBItemEntityCollection;
    }

    public void setDBItemEntityCollection(Collection<DBItemEntity> dBItemEntityCollection) {
        this.dBItemEntityCollection = dBItemEntityCollection;
    }

    @XmlTransient
    public Collection<DBItemEntity> getDBItemEntityCollection1() {
        return dBItemEntityCollection1;
    }

    public void setDBItemEntityCollection1(Collection<DBItemEntity> dBItemEntityCollection1) {
        this.dBItemEntityCollection1 = dBItemEntityCollection1;
    }

    @XmlTransient
    public Collection<DBUserMessageEntity> getDBUserMessageEntityCollection() {
        return dBUserMessageEntityCollection;
    }

    public void setDBUserMessageEntityCollection(Collection<DBUserMessageEntity> dBUserMessageEntityCollection) {
        this.dBUserMessageEntityCollection = dBUserMessageEntityCollection;
    }

    @XmlTransient
    public Collection<DBUserMessageEntity> getDBUserMessageEntityCollection1() {
        return dBUserMessageEntityCollection1;
    }

    public void setDBUserMessageEntityCollection1(Collection<DBUserMessageEntity> dBUserMessageEntityCollection1) {
        this.dBUserMessageEntityCollection1 = dBUserMessageEntityCollection1;
    }

    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DBUserEntity)) {
            return false;
        }
        DBUserEntity other = (DBUserEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.pd.leilao.server.services.db.entities.DBUserEntity[ id=" + id + " ]";
    }
    
    public UserEntity convertToModelEntity()
    {
        UserEntity entity = new UserEntity();
        entity.setId(getId());
        entity.setMorada(getAddress());
        entity.setNome(getFullname());
        entity.setPassword(getPassword());
        entity.setSaldo(getBalance().floatValue());
        entity.setUserName(getUsername());
        entity.setUserStateType(UserStateType.getType(getUserStateType()));
        entity.setUserType(UserType.getType(getUserType()));
        entity.setSuspensionReason(getSuspensionReason());
        
        
        return entity;
    }

    @XmlTransient
    public Collection<DBFollowItemEntity> getDBFollowItemEntityCollection() {
        return dBFollowItemEntityCollection;
    }

    public void setDBFollowItemEntityCollection(Collection<DBFollowItemEntity> dBFollowItemEntityCollection) {
        this.dBFollowItemEntityCollection = dBFollowItemEntityCollection;
    }


    @XmlTransient
    public Collection<DBReportUser> getDBReportUserCollection() {
        return dBReportUserCollection;
    }

    public void setDBReportUserCollection(Collection<DBReportUser> dBReportUserCollection) {
        this.dBReportUserCollection = dBReportUserCollection;
    }

    @XmlTransient
    public Collection<DBReportUser> getDBReportUserCollection1() {
        return dBReportUserCollection1;
    }

    public void setDBReportUserCollection1(Collection<DBReportUser> dBReportUserCollection1) {
        this.dBReportUserCollection1 = dBReportUserCollection1;
    }

    @XmlTransient
    public Collection<DBItemBid> getDBItemBidCollection() {
        return dBItemBidCollection;
    }

    public void setDBItemBidCollection(Collection<DBItemBid> dBItemBidCollection) {
        this.dBItemBidCollection = dBItemBidCollection;
    }

    @XmlTransient
    public Collection<DBReportItem> getDBReportItemCollection() {
        return dBReportItemCollection;
    }

    public void setDBReportItemCollection(Collection<DBReportItem> dBReportItemCollection) {
        this.dBReportItemCollection = dBReportItemCollection;
    }


    @XmlTransient
    public Collection<DBTransactionEntity> getDBTransactionEntityCollection() {
        return dBTransactionEntityCollection;
    }

    public void setDBTransactionEntityCollection(Collection<DBTransactionEntity> dBTransactionEntityCollection) {
        this.dBTransactionEntityCollection = dBTransactionEntityCollection;
    }

    @XmlTransient
    public Collection<DBTransactionEntity> getDBTransactionEntityCollection1() {
        return dBTransactionEntityCollection1;
    }

    public void setDBTransactionEntityCollection1(Collection<DBTransactionEntity> dBTransactionEntityCollection1) {
        this.dBTransactionEntityCollection1 = dBTransactionEntityCollection1;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @XmlTransient
    public Collection<DBUserNotificationEntity> getDBUserNotificationEntityCollection() {
        return dBUserNotificationEntityCollection;
    }

    public void setDBUserNotificationEntityCollection(Collection<DBUserNotificationEntity> dBUserNotificationEntityCollection) {
        this.dBUserNotificationEntityCollection = dBUserNotificationEntityCollection;
    }
    
}
