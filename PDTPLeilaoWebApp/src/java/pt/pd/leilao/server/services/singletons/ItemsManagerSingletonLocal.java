/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.util.List;
import javax.ejb.Local;
import pt.pd.leilao.server.model.ItemBidEntity;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.UserEntity;

/**
 *
 * @author dfortunato
 */
@Local
public interface ItemsManagerSingletonLocal {
    
    public void putItem(ItemEntity itemEntity);
    
    public List<ItemEntity> getListItems();
    
    public List<ItemEntity> getUserSellerItems(int userId);
    
    public List<ItemEntity> getUserBoughtItems(int userId);
    
    public ItemEntity findItemById(int itemId);
    
    public List<ItemEntity> searchItemsByText(String textToSearch);
    
    public ResponseService<ItemBidEntity> putItemBid(int itemId, int userId, float valueBid);
    
    public List<ItemBidEntity> getListItemBids(int itemId);
    
    public ItemBidEntity getHighestItemBid(int itemId);
    
    public void validateAllPendentBidItems();
    
    public void notifItemSold(ItemEntity itemEntity);
    
    public void notifItemNewBid(ItemBidEntity itemBidEntity);
 
    public void notifItemExpired(ItemEntity itemEntity);
    
}
