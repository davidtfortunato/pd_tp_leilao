/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.db.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ReportUserEntity;
import pt.pd.leilao.server.model.enums.ItemStateType;
import pt.pd.leilao.server.model.enums.ReportStateType;

/**
 *
 * @author dfortunato
 */
@Entity
@Table(name = "pd_report_user")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DBReportUser.findAll", query = "SELECT d FROM DBReportUser d")
    , @NamedQuery(name = "DBReportUser.findById", query = "SELECT d FROM DBReportUser d WHERE d.id = :id")
    , @NamedQuery(name = "DBReportUser.findByMessage", query = "SELECT d FROM DBReportUser d WHERE d.message = :message")
    , @NamedQuery(name = "DBReportUser.findByCurrentStateType", query = "SELECT d FROM DBReportUser d WHERE d.currentStateType = :currentStateType")
    , @NamedQuery(name = "DBReportUser.findByUserId", query = "SELECT d FROM DBReportUser d WHERE d.idUserTarget.id = :userId")})
public class DBReportUser implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 256)
    @Column(name = "message")
    private String message;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "current_state_type")
    private String currentStateType;
    @JoinColumn(name = "id_user_sent_report", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private DBUserEntity idUserSentReport;
    @JoinColumn(name = "id_user_target", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private DBUserEntity idUserTarget;

    public DBReportUser() {
    }

    public DBReportUser(Integer id) {
        this.id = id;
    }

    public DBReportUser(Integer id, String message, String currentStateType) {
        this.id = id;
        this.message = message;
        this.currentStateType = currentStateType;
    }
    
    public DBReportUser(ReportUserEntity reportUserEntity)
    {
        this.id = reportUserEntity.getId();
        this.message = reportUserEntity.getMessage();
        if (reportUserEntity.getIdUserTarget() != null) {
            this.idUserTarget = new DBUserEntity(reportUserEntity.getIdUserTarget());
        }
        if (reportUserEntity.getIdUserSentReport()!= null) {
            this.idUserSentReport = new DBUserEntity(reportUserEntity.getIdUserSentReport());
        }
        this.currentStateType = reportUserEntity.getCurrentStateType().getTypeValue();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCurrentStateType() {
        return currentStateType;
    }

    public void setCurrentStateType(String currentStateType) {
        this.currentStateType = currentStateType;
    }

    public DBUserEntity getIdUserSentReport() {
        return idUserSentReport;
    }

    public void setIdUserSentReport(DBUserEntity idUserSentReport) {
        this.idUserSentReport = idUserSentReport;
    }

    public DBUserEntity getIdUserTarget() {
        return idUserTarget;
    }

    public void setIdUserTarget(DBUserEntity idUserTarget) {
        this.idUserTarget = idUserTarget;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DBReportUser)) {
            return false;
        }
        DBReportUser other = (DBReportUser) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.pd.leilao.server.services.db.entities.DBReportUser[ id=" + id + " ]";
    }
    
    public ReportUserEntity convertToModelEntity()
    {
        ReportUserEntity reportUserEntity = new ReportUserEntity();
        reportUserEntity.setCurrentStateType(ReportStateType.getType(getCurrentStateType()));
        reportUserEntity.setId(getId());
        reportUserEntity.setIdUserSentReport(getIdUserSentReport().convertToModelEntity());
        reportUserEntity.setIdUserTarget(getIdUserTarget().convertToModelEntity());
        reportUserEntity.setMessage(getMessage());
        
        return reportUserEntity;
    }
    
}
