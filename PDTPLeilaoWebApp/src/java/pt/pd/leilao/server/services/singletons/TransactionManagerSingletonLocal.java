/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.util.List;
import javax.ejb.Local;
import pt.pd.leilao.server.model.ItemBidEntity;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.TransactionEntity;

/**
 *
 * @author dfortunato
 */
@Local
public interface TransactionManagerSingletonLocal {
    
    public List<TransactionEntity> getAllTransactions();
    
    public List<TransactionEntity> getUserSentTransactions(int userId);
    
    public List<TransactionEntity> getUserReceiveTransactions(int userId);
    
    public TransactionEntity findItemTransaction(int itemId);
    
    public ResponseService<TransactionEntity> processTransaction(ItemBidEntity itemBidEntity);
    
}
