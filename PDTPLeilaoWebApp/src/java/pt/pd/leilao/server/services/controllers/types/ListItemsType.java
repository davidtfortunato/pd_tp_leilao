/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.controllers.types;

/**
 *
 * @author dfortunato
 */
public enum ListItemsType {
    LIST_USER_ITEMS_SELLING("items_selling"), 
    LIST_USER_ITEMS_BOUGHT("items_bought"),
    LIST_USER_ITEMS_SOLD("items_sold"),
    LIST_USER_ITEMS_FOLLOWING("items_following"),
    UNKNOWN("unknown");
    
    private String typeValue;
    
    ListItemsType(String typeValue)
    {
        this.typeValue = typeValue;
    }

    public String getTypeValue() {
        return typeValue;
    }
    
    public String getTitle()
    {
        switch(this)
        {
            case LIST_USER_ITEMS_BOUGHT:
                return "Lista de items comprados";
            case LIST_USER_ITEMS_FOLLOWING:
                return "Lista de items a seguir";
            case LIST_USER_ITEMS_SELLING:
                return "Lista de items à venda";
            case LIST_USER_ITEMS_SOLD:
                return "Lista de items vendidos";
            default:
                return "";
        }
    }
    
    public static ListItemsType getType(String typeValue)
    {
        if (typeValue != null) 
        {
            if (typeValue.equals(LIST_USER_ITEMS_SELLING.getTypeValue())) 
            {
                return LIST_USER_ITEMS_SELLING;
            } else if(typeValue.equals(LIST_USER_ITEMS_BOUGHT.getTypeValue()))
            {
                return LIST_USER_ITEMS_BOUGHT;
            } else if(typeValue.equals(LIST_USER_ITEMS_FOLLOWING.getTypeValue()))
            {
                return LIST_USER_ITEMS_FOLLOWING;
            } else if (typeValue.equals(LIST_USER_ITEMS_SOLD.getTypeValue())) 
            {
                return LIST_USER_ITEMS_SOLD;
            } 
        }
        
        return UNKNOWN;
    }
    
}
