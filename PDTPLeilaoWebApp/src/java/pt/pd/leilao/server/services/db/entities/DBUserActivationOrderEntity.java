/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.db.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import pt.pd.leilao.server.model.UserActivationOrderEntity;
import pt.pd.leilao.server.model.UserMessageEntity;
import pt.pd.leilao.server.model.enums.AdminOrderResponseType;
import pt.pd.leilao.server.model.enums.AdminOrderType;

/**
 *
 * @author dfortunato
 */
@Entity
@Table(name = "pd_user_activation_order")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DBUserActivationOrderEntity.findAll", query = "SELECT d FROM DBUserActivationOrderEntity d ORDER BY d.id ASC")
    , @NamedQuery(name = "DBUserActivationOrderEntity.findById", query = "SELECT d FROM DBUserActivationOrderEntity d WHERE d.id = :id")
    , @NamedQuery(name = "DBUserActivationOrderEntity.findByAdminOrderType", query = "SELECT d FROM DBUserActivationOrderEntity d WHERE d.adminOrderType = :adminOrderType")
    , @NamedQuery(name = "DBUserActivationOrderEntity.findByAdminOrderResponseType", query = "SELECT d FROM DBUserActivationOrderEntity d WHERE d.adminOrderResponseType = :adminOrderResponseType")
    , @NamedQuery(name = "DBUserActivationOrderEntity.findByDescription", query = "SELECT d FROM DBUserActivationOrderEntity d WHERE d.description = :description")
    , @NamedQuery(name = "DBUserActivationOrderEntity.findByUserPending", query = "SELECT d FROM DBUserActivationOrderEntity d WHERE d.userTargetId.username = :username AND d.adminOrderResponseType = 'pending'")})
public class DBUserActivationOrderEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "admin_order_type")
    private String adminOrderType;
    @Size(max = 30)
    @Column(name = "admin_order_response_type")
    private String adminOrderResponseType;
    @Size(max = 256)
    @Column(name = "description")
    private String description;
    @JoinColumn(name = "user_target_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private DBUserEntity userTargetId;

    public DBUserActivationOrderEntity() {
    }

    public DBUserActivationOrderEntity(Integer id) {
        this.id = id;
    }

    public DBUserActivationOrderEntity(Integer id, String adminOrderType) {
        this.id = id;
        this.adminOrderType = adminOrderType;
    }
    
    public DBUserActivationOrderEntity(UserActivationOrderEntity userActivationOrderEntity)
    {
        this.id = userActivationOrderEntity.getId();
        this.adminOrderResponseType = userActivationOrderEntity.getAdminOrderResponseType().getTypeValue();
        this.adminOrderType = userActivationOrderEntity.getAdminOrderType().getTypeValue();
        this.description = userActivationOrderEntity.getDescription();
        this.userTargetId = new DBUserEntity(userActivationOrderEntity.getTargetUser());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAdminOrderType() {
        return adminOrderType;
    }

    public void setAdminOrderType(String adminOrderType) {
        this.adminOrderType = adminOrderType;
    }

    public String getAdminOrderResponseType() {
        return adminOrderResponseType;
    }

    public void setAdminOrderResponseType(String adminOrderResponseType) {
        this.adminOrderResponseType = adminOrderResponseType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DBUserEntity getUserTargetId() {
        return userTargetId;
    }

    public void setUserTargetId(DBUserEntity userTargetId) {
        this.userTargetId = userTargetId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DBUserActivationOrderEntity)) {
            return false;
        }
        DBUserActivationOrderEntity other = (DBUserActivationOrderEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pt.pd.leilao.server.services.db.entities.DBUserActivationOrderEntity[ id=" + id + " ]";
    }
    
    public UserActivationOrderEntity convertToModelEntity()
    {
        UserActivationOrderEntity entity = new UserActivationOrderEntity();
        entity.setAdminOrderResponseType(AdminOrderResponseType.getType(getAdminOrderResponseType()));
        entity.setAdminOrderType(AdminOrderType.getType(getAdminOrderType()));
        entity.setDescription(getDescription());
        entity.setId(getId());
        entity.setTargetUser(getUserTargetId().convertToModelEntity());
        
        return entity;
    }
    
}
