/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import pt.pd.leilao.server.model.NewsletterMessageEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.model.UserMessageEntity;
import pt.pd.leilao.server.services.db.DAOManagerLocal;
import pt.pd.leilao.server.services.db.entities.DBUserMessageEntity;
import pt.pd.leilao.server.services.utils.FileUtils;

/**
 *
 * @author dfortunato
 */
@Singleton
public class MessagesManagerSingleton implements MessagesManagerSingletonLocal {
    private static final String TAG = MessagesManagerSingleton.class.getSimpleName();
    
    @EJB
    UserManagerSingletonLocal userManagerSingletonLocal;
    
    @EJB
    DAOManagerLocal dAOManagerLocal;
    
    @Override
    public ResponseService<UserMessageEntity> sendNewMessage(String usernameSender, String usernameReceiver, String message) {
        UserEntity userSender = userManagerSingletonLocal.findUserByUsername(usernameSender);
        UserEntity userReceiver = userManagerSingletonLocal.findUserByUsername(usernameReceiver);
        if (userSender == null || userReceiver == null) {
            return new ResponseService<>(ResponseService.RESPONSE_CODE_NOT_FOUND);
        }
        
        UserMessageEntity userMessageEntity = UserMessageEntity.generateNewMessage(userSender, userReceiver, message);
        if (userMessageEntity != null) {
            dAOManagerLocal.getEntityManager().persist(new DBUserMessageEntity(userMessageEntity));
            return new ResponseService<>(userMessageEntity);
        }
        else
        {
            return new ResponseService<>(ResponseService.RESPONSE_CODE_INVALID_DATA);
        }
        
    }

    @Override
    public ResponseService<List<UserMessageEntity>> getAllMessages() {
        List<DBUserMessageEntity> listDBEntities = dAOManagerLocal.getEntityManager().createNamedQuery("DBUserMessageEntity.findAll").getResultList();
        List<UserMessageEntity> listEntities = new ArrayList<>();
        for (DBUserMessageEntity listDBEntity : listDBEntities) {
            listEntities.add(listDBEntity.convertToModelEntity());
        }
        
        return new ResponseService<>(listEntities);
    }

    @Override
    public ResponseService<List<UserMessageEntity>> getMessagesSentBy(String username) {
        // Get User Sent Messages
        List<DBUserMessageEntity> listDBEntities = dAOManagerLocal.getEntityManager().createNamedQuery("DBUserMessageEntity.findUserSentMessages").setParameter("username", username).getResultList();
        List<UserMessageEntity> listEntities = new ArrayList<>();
        
        for (DBUserMessageEntity listDBEntity : listDBEntities) {
            listEntities.add(listDBEntity.convertToModelEntity());
        }
        return new ResponseService<>(listEntities);
    }

    @Override
    public ResponseService<List<UserMessageEntity>> getMessagesReceivedBy(String username) {
        // Get User Sent Messages
        List<DBUserMessageEntity> listDBEntities = dAOManagerLocal.getEntityManager().createNamedQuery("DBUserMessageEntity.findUserReceivedMessages").setParameter("username", username).getResultList();
        List<UserMessageEntity> listEntities = new ArrayList<>();
        
        for (DBUserMessageEntity listDBEntity : listDBEntities) {
            listEntities.add(listDBEntity.convertToModelEntity());
        }
        return new ResponseService<>(listEntities);
    }
}
