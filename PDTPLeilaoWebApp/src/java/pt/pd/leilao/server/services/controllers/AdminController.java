/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.controllers;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import pt.pd.leilao.server.model.CategoryEntity;
import pt.pd.leilao.server.model.ReportItemEntity;
import pt.pd.leilao.server.model.ReportUserEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.UserActivationOrderEntity;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.model.enums.AdminOrderResponseType;
import pt.pd.leilao.server.model.enums.ItemStateType;
import pt.pd.leilao.server.model.enums.ReportStateType;
import pt.pd.leilao.server.model.enums.UserStateType;
import pt.pd.leilao.server.model.enums.UserType;
import pt.pd.leilao.server.services.ClientAdminUserServiceRemote;
import pt.pd.leilao.server.services.ClientItemsService;
import pt.pd.leilao.server.services.ClientItemsServiceRemote;
import pt.pd.leilao.server.services.utils.AlertMessagesUtil;

/**
 *
 * @author dfortunato
 */
@Named(value = "adminController")
@SessionScoped
public class AdminController implements Serializable {

    @EJB
    ClientAdminUserServiceRemote clientAdminUserServiceRemote;
    
    /**
     * Creates a new instance of AdminController
     */
    public AdminController() {
    }
    
    public List<UserActivationOrderEntity> getListActivationUserOrders()
    {
        List<UserActivationOrderEntity> listOrders = new ArrayList<>();
        ResponseService<List<UserActivationOrderEntity>> response = clientAdminUserServiceRemote.getListUserActivationOrderEntity();
        for (UserActivationOrderEntity order : response.getDataObject()) 
        {
            if (order.getAdminOrderResponseType() == AdminOrderResponseType.PENDING) 
            {
                listOrders.add(order);
            }
        }
        
        return listOrders;
    }
    
    public String replyUserOrder()
    {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext extContext = context.getExternalContext();
        Map<String, String> params = extContext.getRequestParameterMap();
        String orderId = params.get("order_reply_id");
        String replyType = params.get("order_reply_type");
        ResponseService response = null;
        if (replyType != null && orderId != null) 
        {
            switch(replyType)
            {
                case "accept":
                    response = clientAdminUserServiceRemote.replyUserActivationOrder(Integer.valueOf(orderId), AdminOrderResponseType.APPROVED);
                    break;
                case "reject":
                    response = clientAdminUserServiceRemote.replyUserActivationOrder(Integer.valueOf(orderId), AdminOrderResponseType.DECLINED);
                    break;
            }

            if (response != null && response.isSuccess()) 
            {
                AlertMessagesUtil.displayResponseMessage(response, "O pedido foi respondido com sucesso");
            }
            else {
                AlertMessagesUtil.displayResponseMessage(response, "Não foi possivel responder ao pedido");
            } 
        }
        return "admin_manage_user_requests";
    }
    
    public void cancelItem()
    {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext extContext = context.getExternalContext();
        Map<String, String> params = extContext.getRequestParameterMap();
        String itemId = params.get("item_id");
        ResponseService responseService = clientAdminUserServiceRemote.cancelItem(Integer.valueOf(itemId));
        if (responseService.isSuccess()) 
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "O item foi cancelado com sucesso");
        }
        else
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "Não foi possivel cancelar o item seleccionado");
        }
    }
    
    /************* Categories Management *******************/
    
    public List<CategoryEntity> getListCategories()
    {
        return clientAdminUserServiceRemote.getListCategories().getDataObject();
    }
    
    public void addCategory()
    {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext extContext = context.getExternalContext();
        Map<String, String> params = extContext.getRequestParameterMap();
        String categoryName = params.get("in_category");
        ResponseService responseService = clientAdminUserServiceRemote.addCategory(categoryName);
        if (responseService.isSuccess()) 
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "A categoria foi adicionada com sucesso");
        }
        else
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "Não foi possivel adicionar a nova categoria");
        }
    }
    
    public void refreshCategoryName()
    {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext extContext = context.getExternalContext();
        Map<String, String> params = extContext.getRequestParameterMap();
        String categoryId = params.get("categoryId");
        String tableRow = params.get("tableRow");
        String categoryName = params.get("form_categories:table_categories:" + tableRow + ":input_category");
        
        ResponseService responseService = clientAdminUserServiceRemote.changeCategoryName(Integer.valueOf(categoryId), categoryName);
        
        if (responseService.isSuccess()) 
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "Actualizado com sucesso");
        }
        else
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "Falhou o novo nome");
        }
    }
    
    /************* Users Management *******************/
    
    public List<UserEntity> getListUsers()
    {
        ResponseService<List<UserEntity>> response = clientAdminUserServiceRemote.getListUsers();
        Iterator<UserEntity> it = response.getDataObject().iterator();
        while (it.hasNext()) 
        {
            UserEntity userEntity = it.next();
            if (userEntity.getUserType() != UserType.REGISTERED 
                    || userEntity.getUserStateType() == UserStateType.PENDING_APPROVE) {
                it.remove();
            }
            
        }
        return response.getDataObject();
    }
    
    public void activateUser()
    {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext extContext = context.getExternalContext();
        Map<String, String> params = extContext.getRequestParameterMap();
        String username = params.get("username");
        ResponseService responseService = clientAdminUserServiceRemote.reactivateUser(username);
        if (responseService.isSuccess()) 
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "O utilizador foi activado com sucesso.");
        }
        else
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "Não foi possivel activar o utilizador");
        }
    }
    
    public void suspendUser()
    {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext extContext = context.getExternalContext();
        Map<String, String> params = extContext.getRequestParameterMap();
        String username = params.get("username");
        String reason = params.get("in_suspension_reason");
        ResponseService responseService = clientAdminUserServiceRemote.suspendUser(username, reason);
        if (responseService.isSuccess()) 
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "O utilizador foi suspenso com sucesso.");
        }
        else
        {
            AlertMessagesUtil.displayResponseMessage(responseService, "Não foi possivel suspender o utilizador");
        }
    }
    
    
    /************** MANAGE REPORTS **************/
    
    public List<ReportItemEntity> getListItemReports()
    {
        List<ReportItemEntity> listReports = clientAdminUserServiceRemote.getListItemsReport();
        
        Iterator<ReportItemEntity> it = listReports.iterator();
        while(it.hasNext())
        {
            if (it.next().getCurrentStateType() != ReportStateType.PENDING) 
            {
               it.remove();
            }
        }
        return listReports;
    }
    
    public List<ReportUserEntity> getListUserReports()
    {
        List<ReportUserEntity> listReports = clientAdminUserServiceRemote.getListUsersReport();
        
        Iterator<ReportUserEntity> it = listReports.iterator();
        while(it.hasNext())
        {
            if (it.next().getCurrentStateType() != ReportStateType.PENDING) 
            {
               it.remove();
            }
        }
        return listReports;
    }
    
    public void replyReportUser()
    {
        Map<String, String> mapParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String reportId = mapParams.get("reportId");
        String newReportState = mapParams.get("newReportState");
        
        clientAdminUserServiceRemote.replyUserReport(Integer.valueOf(reportId), ReportStateType.getType(newReportState));
    }
    
    public void replyReportItem()
    {
        Map<String, String> mapParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String reportId = mapParams.get("reportId");
        String newReportState = mapParams.get("newReportState");
        
        clientAdminUserServiceRemote.replyItemReport(Integer.valueOf(reportId), ReportStateType.getType(newReportState));
    }
    
}
