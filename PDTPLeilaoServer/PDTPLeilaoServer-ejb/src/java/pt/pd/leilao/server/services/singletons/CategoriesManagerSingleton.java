/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import pt.pd.leilao.server.model.CategoryEntity;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.services.utils.FileUtils;

/**
 *
 * @author dfortunato
 */
@Singleton
public class CategoriesManagerSingleton implements CategoriesManagerSingletonLocal {
    private static final String TAG = CategoriesManagerSingleton.class.getName();
    
    private static final String FILENAME_CATEGORIES = "/tmp/categories";
    
    // Data
    private List<CategoryEntity> listCategories;

    public CategoriesManagerSingleton() {
        listCategories = new ArrayList<>();
    }

    @Override
    public List<CategoryEntity> getListCategories() {
        return listCategories;
    }

    @Override
    public boolean addCategory(String name) {
        // Check if already exists
        if (findCategoryByName(name) != null) {
            return false;
        } else {
            CategoryEntity categoryEntity = new CategoryEntity();
            categoryEntity.setName(name);
            categoryEntity.setId(listCategories.size());
            listCategories.add(categoryEntity);
            return true;
        }
    }

    @Override
    public ResponseService updateCategory(CategoryEntity categoryEntity) {
        CategoryEntity oldCat = findCategoryById(categoryEntity.getId());
        CategoryEntity searchCatName = findCategoryByName(categoryEntity.getName());
        ResponseService responseService = new ResponseService(false);
        if (oldCat == null) {
            responseService.setResponseCode(ResponseService.RESPONSE_CODE_NOT_FOUND);
        }
        else if(searchCatName != null)
        {
            responseService.setResponseCode(ResponseService.RESPONSE_CODE_ERROR_ALREADY_EXISTS);
        } else {
            listCategories.remove(oldCat);
            listCategories.add(categoryEntity);  
            responseService.setResponseCode(ResponseService.RESPONSE_CODE_SUCCESS);
        }
        return responseService;
    }

    @Override
    public CategoryEntity findCategoryByName(String categoryName) {
        for (CategoryEntity categoryEntity : listCategories) {
            if (categoryEntity.getName().equals(categoryName)) {
                return categoryEntity;
            }
        }
        return null;
    }

    public CategoryEntity findCategoryById(int categoryId) {
        for (CategoryEntity categoryEntity : listCategories) {
            if (categoryEntity.getId() == categoryId) {
                return categoryEntity;
            }
        }
        return null;
    }
    
    @PostConstruct
    public void loadState() {
        Logger.getLogger(TAG).log(Level.INFO, "init LoadState");
        
        // Load Categories
        listCategories =  (List<CategoryEntity>) FileUtils.loadData(FILENAME_CATEGORIES);

        // Check if should generate Collections
        if (listCategories == null) {
            listCategories = new ArrayList<>();
        }
        
        if (listCategories.isEmpty()) {
            // Add Geral Category
            addCategory("Geral");
        }
    }

    @PreDestroy
    public synchronized void saveState() {
        Logger.getLogger(TAG).log(Level.INFO, "init SaveState");
        
        // Save Categories
        FileUtils.saveData(FILENAME_CATEGORIES, listCategories);
        
    }
    
}
