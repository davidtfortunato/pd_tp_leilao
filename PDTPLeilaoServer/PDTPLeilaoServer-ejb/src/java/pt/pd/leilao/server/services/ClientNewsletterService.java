/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import pt.pd.leilao.server.model.NewsletterMessageEntity;
import pt.pd.leilao.server.services.singletons.NewsletterManagerSingletonLocal;

/**
 *
 * @author dfortunato
 */
@Stateless
public class ClientNewsletterService implements ClientNewsletterServiceRemote {

    @EJB
    NewsletterManagerSingletonLocal newsletterManagerSingletonLocal;
    
    @Override
    public List<NewsletterMessageEntity> getListNewsletters() {
        return newsletterManagerSingletonLocal.getListNewsletter();
    }
    
}
