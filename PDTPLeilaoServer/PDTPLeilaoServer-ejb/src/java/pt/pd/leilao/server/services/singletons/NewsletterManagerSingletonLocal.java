/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.util.List;
import javax.ejb.Local;
import pt.pd.leilao.server.model.NewsletterMessageEntity;

/**
 *
 * @author dfortunato
 */
@Local
public interface NewsletterManagerSingletonLocal {
    
    public void addNewsletter(NewsletterMessageEntity newsletterMessageEntity);
    
    public List<NewsletterMessageEntity> getListNewsletter();
    
}
