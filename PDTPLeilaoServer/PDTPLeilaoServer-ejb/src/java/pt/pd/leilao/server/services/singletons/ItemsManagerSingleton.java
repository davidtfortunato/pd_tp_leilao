/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.enums.ItemStateType;
import pt.pd.leilao.server.model.enums.NewsletterMessageType;
import pt.pd.leilao.server.services.utils.FileUtils;
import pt.pd.leilao.server.services.utils.NewsletterUtil;

/**
 *
 * @author dfortunato
 */
@Singleton
public class ItemsManagerSingleton implements ItemsManagerSingletonLocal {
    private static final String TAG = ItemsManagerSingleton.class.getName();
    
    private static final String FILENAME_ITEMS = "/tmp/items";
    
    // Data
    private Map<Integer, ItemEntity> mapItems; // ItemId, ItemEntity
    
    @EJB
    NewsletterManagerSingletonLocal newsletterManagerSingletonLocal;
    
    public ItemsManagerSingleton() {
        mapItems = new HashMap<Integer, ItemEntity>();
    }

    @Override
    public void putItem(ItemEntity itemEntity) {
        // Generate ID
        if (itemEntity.getId() == -1) {
            itemEntity.setId(mapItems.size());
            newsletterManagerSingletonLocal.addNewsletter(NewsletterUtil.generateItemNewsletterEntity(NewsletterMessageType.ITEM_ADDED_NEW_ITEM, itemEntity));
        }
        
        // Check if can be updated
        ItemEntity lastItemEntity = findItemById(itemEntity.getId());
        if (lastItemEntity != null 
                && lastItemEntity.getUserBuyer() != null
                && lastItemEntity.getItemStateType() != ItemStateType.ACTIVE_SELL) {
            Logger.getLogger(TAG).log(Level.WARNING, "Item " + itemEntity.getId() + " cannot be edited anymore");
        }
        else
        {
           // Update
            itemEntity.setLastUpdate(Calendar.getInstance().getTimeInMillis());
            mapItems.put(itemEntity.getId(), itemEntity); 
        }
        
        
    }

    @Override
    public List<ItemEntity> getListItems() {
        List<ItemEntity> listItems = new ArrayList<>(mapItems.values());
        
        Iterator<ItemEntity> itListItems = listItems.iterator();
        while (itListItems.hasNext()) {
            if (itListItems.next().getItemStateType() == ItemStateType.CANCELLED) {
                // Filter cancelled items
                itListItems.remove();
            }
        }
        
        return listItems;
    }

    @Override
    public List<ItemEntity> getUserSellerItems(String userName) {
        List<ItemEntity> listItems = getListItems();
        List<ItemEntity> listUserItems = new ArrayList<>();
        for (ItemEntity itemEntity : listItems) {
            if (itemEntity.getUserSeller().getUserName().equalsIgnoreCase(userName)) {
                listUserItems.add(itemEntity);
            }
        }
        return listUserItems;
    }

    @Override
    public List<ItemEntity> getUserBoughtItems(String userName) {
        List<ItemEntity> listItems = getListItems();
        List<ItemEntity> listUserItems = new ArrayList<>();
        for (ItemEntity itemEntity : listItems) {
            if (itemEntity.getUserBuyer() != null && itemEntity.getUserBuyer().getUserName().equalsIgnoreCase(userName)) {
                listUserItems.add(itemEntity);
            }
        }
        return listUserItems;
    }

    @Override
    public ItemEntity findItemById(int itemId) {
        return mapItems.get(itemId);
    }

    @PostConstruct
    public void loadState() {
        Logger.getLogger(TAG).log(Level.INFO, "init LoadState");
        
        // Load Items
        mapItems = (Map<Integer, ItemEntity>) FileUtils.loadData(FILENAME_ITEMS);

        // Check if should generate Collections
        if (mapItems == null) {
            mapItems = new HashMap<>();
        }
    }

    @PreDestroy
    public synchronized void saveState() {
        Logger.getLogger(TAG).log(Level.INFO, "init SaveState");
        
        // Save Items
        FileUtils.saveData(FILENAME_ITEMS, mapItems);
        
    }
}
