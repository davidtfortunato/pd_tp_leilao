/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.util.List;
import javax.ejb.Local;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.UserEntity;

/**
 *
 * @author dfortunato
 */
@Local
public interface ItemsManagerSingletonLocal {
    
    public void putItem(ItemEntity itemEntity);
    
    public List<ItemEntity> getListItems();
    
    public List<ItemEntity> getUserSellerItems(String userName);
    
    public List<ItemEntity> getUserBoughtItems(String userName);
    
    public ItemEntity findItemById(int itemId);
    
}
