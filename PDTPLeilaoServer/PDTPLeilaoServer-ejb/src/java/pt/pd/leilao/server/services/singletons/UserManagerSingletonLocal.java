/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.util.List;
import javax.ejb.Local;
import pt.pd.leilao.server.model.UserActivationOrderEntity;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.model.enums.AdminOrderResponseType;
import pt.pd.leilao.server.model.enums.responses.ResponseUserRegisterType;
import pt.pd.leilao.server.model.responses.ResponseUserLoginData;
import pt.pd.leilao.server.services.ClientUserServiceRemote;

/**
 *
 * @author dfortunato
 */
@Local
public interface UserManagerSingletonLocal {
    
    public ResponseUserRegisterType registerUser(UserEntity userData);
    
    public boolean addAdminOrder(UserActivationOrderEntity adminOrder);
    
    public ResponseUserLoginData loginUser(ClientUserServiceRemote clientUserServiceRemote, String username, String password);
    
    public boolean logoutUser(String username);
    
    public List<UserEntity> getListUsers();
    
    public List<UserActivationOrderEntity> getListAdminUserActivationOrders();
    
    public boolean replyAdminUserActivationOrder(int idAdminOrder, AdminOrderResponseType responseType);
    
    public UserEntity findUserByUsername(String username);
    
    public void putUserEntity(UserEntity userData);
}
