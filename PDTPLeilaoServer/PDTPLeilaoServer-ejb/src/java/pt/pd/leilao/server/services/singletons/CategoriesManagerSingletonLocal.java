/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.util.List;
import javax.ejb.Local;
import pt.pd.leilao.server.model.CategoryEntity;
import pt.pd.leilao.server.model.ResponseService;

/**
 *
 * @author dfortunato
 */
@Local
public interface CategoriesManagerSingletonLocal {
    
    public List<CategoryEntity> getListCategories();
    
    public boolean addCategory(String name);
    
    public ResponseService updateCategory(CategoryEntity categoryEntity);
    
    public CategoryEntity findCategoryByName(String categoryName);
    
}
