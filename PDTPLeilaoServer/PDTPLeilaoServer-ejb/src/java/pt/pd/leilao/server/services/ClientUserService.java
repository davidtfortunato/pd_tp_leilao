/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services;

import java.util.Iterator;
import java.util.List;
import pt.pd.leilao.server.services.ClientUserServiceRemote;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import org.jboss.logging.Logger;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.UserActivationOrderEntity;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.model.UserMessageEntity;
import pt.pd.leilao.server.model.enums.AdminOrderType;
import pt.pd.leilao.server.model.enums.responses.ResponseUserLoginType;
import pt.pd.leilao.server.model.enums.responses.ResponseUserRegisterType;
import pt.pd.leilao.server.model.responses.ResponseUserLoginData;
import pt.pd.leilao.server.services.singletons.FollowItemManagerSingletonLocal;
import pt.pd.leilao.server.services.singletons.ItemsManagerSingletonLocal;
import pt.pd.leilao.server.services.singletons.MessagesManagerSingletonLocal;
import pt.pd.leilao.server.services.singletons.UserManagerSingleton;
import pt.pd.leilao.server.services.singletons.UserManagerSingletonLocal;

/**
 *
 * @author dfortunato
 */
@Stateful
public class ClientUserService implements ClientUserServiceRemote {
    private static final String TAG = ClientUserService.class.getSimpleName();
    
    @EJB
    UserManagerSingletonLocal userManagerSingletonLocal;
    
    @EJB
    ItemsManagerSingletonLocal itemsManagerSingletonLocal;
    
    @EJB
    FollowItemManagerSingletonLocal followItemManagerSingletonLocal;
    
    @EJB
    MessagesManagerSingletonLocal messagesManagerSingletonLocal;
    
    // Current Data
    private UserEntity userData;

    public ClientUserService() 
    {
        userData = new UserEntity();
        Logger.getLogger(TAG).debug("Constructor new ClientUserService");
    }
    
    @Override
    public ResponseUserLoginData loginUser(String username, String password) {
        if (userData != null && userData.isUserLoggedIn()) {
            // First Logout
            logout();
        }
        
        ResponseUserLoginData responseUserLoginData = userManagerSingletonLocal.loginUser(this, username, password);
        if (responseUserLoginData.getResponseUserLoginType() == ResponseUserLoginType.LOGIN_SUCCESS) {
            userData = responseUserLoginData.getUserData();
        }
        return responseUserLoginData;
    }

    @Override
    public ResponseUserRegisterType registerUser(UserEntity user) {
        if (user.isUserDataValid()) {
            // Register User
            return userManagerSingletonLocal.registerUser(user);
        }
        else
        {
            return ResponseUserRegisterType.USERDATA_NOT_VALID;
        }
    }

    @Override
    public boolean logout() {
        boolean result = false;
        if (userData != null && userData.isUserLoggedIn()) {
            result = userManagerSingletonLocal.logoutUser(userData.getUserName());
        }
        
        // reset user
        userData = new UserEntity();
        
        return true;
    }

    
    
    @Override
    public UserEntity getCurrentUser() {
        return userData;
    }

    @Override
    public ResponseService changePassword(String oldPassword, String newPassword) {
        
        if (getCurrentUser() != null && getCurrentUser().isUserLoggedIn() && oldPassword != null && newPassword != null) {
            if (getCurrentUser().getPassword().equals(oldPassword) && !newPassword.isEmpty()) {
                getCurrentUser().setPassword(newPassword);
                userManagerSingletonLocal.putUserEntity(getCurrentUser());
                return new ResponseService(true);
            } else {
                return new ResponseService(ResponseService.RESPONSE_CODE_INVALID_DATA);
            }
        }
        else
        {
            return new ResponseService(ResponseService.RESPONSE_CODE_ERROR_NOT_LOGGEDIN);
        }
    }
    
    @PreDestroy
    public void onDestroy()
    {
        if (userData != null && userData.isUserLoggedIn()) {
            logout();
        }
    }
    
    
    /************** Client User Service from Menu *****************/
    

    @Override
    public ResponseService updateUserData(UserEntity newUserEntity) {
        if (getCurrentUser().isUserLoggedIn()) {
            if (newUserEntity != null) {
                if (newUserEntity.getNome() != null && !newUserEntity.getNome().isEmpty()) {
                    getCurrentUser().setNome(newUserEntity.getNome());
                }
                if (newUserEntity.getMorada() != null && !newUserEntity.getMorada().isEmpty()) {
                    getCurrentUser().setMorada(newUserEntity.getMorada());
                }
                userManagerSingletonLocal.putUserEntity(getCurrentUser());
            }
            return new ResponseService(true);
        }
        else
        {
            return new ResponseService(ResponseService.RESPONSE_CODE_ERROR_NOT_LOGGEDIN);
        }
        
    }

    @Override
    public ResponseService suspendUser(String motivo) {
        if (getCurrentUser().isUserLoggedIn()) {
            UserActivationOrderEntity newOrder = new UserActivationOrderEntity();
            newOrder.setAdminOrderType(AdminOrderType.USER_SUSPEND);
            newOrder.setDescription(motivo);
            newOrder.setTargetUser(getCurrentUser());
            return new ResponseService(userManagerSingletonLocal.addAdminOrder(newOrder));
        }
        else
        {
            return new ResponseService(ResponseService.RESPONSE_CODE_ERROR_NOT_LOGGEDIN);
        }
        
    }

    @Override
    public ResponseService transferMoney(float moneyTransfered) {
        if (getCurrentUser().isUserLoggedIn()) {
            getCurrentUser().setSaldo(getCurrentUser().getSaldo() + moneyTransfered);
            userManagerSingletonLocal.putUserEntity(getCurrentUser());
            return new ResponseService(true);
        }
        else
        {
            return new ResponseService(ResponseService.RESPONSE_CODE_ERROR_NOT_LOGGEDIN);
        }
    }

    @Override
    public ResponseService<List<ItemEntity>> getMyListItems() {
        if (getCurrentUser().isUserLoggedIn()) {
            return new ResponseService<>(itemsManagerSingletonLocal.getUserSellerItems(getCurrentUser().getId()));
        }
        else
        {
            return new ResponseService<>(ResponseService.RESPONSE_CODE_ERROR_NOT_LOGGEDIN);
        }
    }

    @Override
    public ResponseService addItem(ItemEntity itemEntity) {
        if (getCurrentUser().isUserLoggedIn()) {
            itemEntity.setUserSeller(getCurrentUser());
            if (itemEntity.isInitialDataValidToSell()) {
                itemsManagerSingletonLocal.putItem(itemEntity);
                return new ResponseService(true);
            }
            else
            {
                return new ResponseService(ResponseService.RESPONSE_CODE_INVALID_DATA);
            }
            
        }
        else
        {
            return new ResponseService<>(ResponseService.RESPONSE_CODE_ERROR_NOT_LOGGEDIN);
        }
    }

    @Override
    public ResponseService<List<ItemEntity>> getListItemsBought() {
        if (getCurrentUser().isUserLoggedIn()) {
            return new ResponseService<>(itemsManagerSingletonLocal.getUserBoughtItems(getCurrentUser().getUserName()));
        }
        else
        {
            return new ResponseService<>(ResponseService.RESPONSE_CODE_ERROR_NOT_LOGGEDIN);
        }
    }

    @Override
    public ResponseService<List<ItemEntity>> getListItemsSold() {
        if (getCurrentUser().isUserLoggedIn()) {
            List<ItemEntity> listItems = itemsManagerSingletonLocal.getUserSellerItems(getCurrentUser().getId());
            Iterator<ItemEntity> itListItems = listItems.iterator();
            while (itListItems.hasNext()) {
                if (itListItems.next().getUserBuyer() == null) {
                    // Filter not sold items
                    itListItems.remove();
                }
            }
            return new ResponseService<>(listItems);
        }
        else
        {
            return new ResponseService<>(ResponseService.RESPONSE_CODE_ERROR_NOT_LOGGEDIN);
        }
    }

    @Override
    public boolean isFollowingItem(int itemId) {
        if (getCurrentUser().isUserLoggedIn()) {
            return followItemManagerSingletonLocal.isItemFollowedByUser(itemId, getCurrentUser().getUserName());
        }
        else
        {
            return false;
        }
    }

    @Override
    public ResponseService toggleFollowItem(int itemId) {
        if (getCurrentUser().isUserLoggedIn()) {
            return followItemManagerSingletonLocal.toggleItemFollowedByUser(itemId, getCurrentUser().getUserName());
        }
        else
        {
            return new ResponseService<>(ResponseService.RESPONSE_CODE_ERROR_NOT_LOGGEDIN);
        }
    }

    @Override
    public ResponseService<List<ItemEntity>> getListItemsFollowed() {
        if (getCurrentUser().isUserLoggedIn()) {
            return followItemManagerSingletonLocal.getListItemsFollowing(getCurrentUser().getUserName());
        }
        else
        {
            return new ResponseService<>(ResponseService.RESPONSE_CODE_ERROR_NOT_LOGGEDIN);
        }
    }

    @Override
    public ResponseService<UserMessageEntity> sendUserMessage(String usernameReceiver, String message) {
        if (getCurrentUser().isUserLoggedIn()) {
            return messagesManagerSingletonLocal.sendNewMessage(getCurrentUser().getUserName(), usernameReceiver, message);
        }
        else
        {
            return new ResponseService<>(ResponseService.RESPONSE_CODE_ERROR_NOT_LOGGEDIN);
        }
    }

    @Override
    public ResponseService<List<UserMessageEntity>> getReceivedMessages() {
        if (getCurrentUser().isUserLoggedIn()) {
            return messagesManagerSingletonLocal.getMessagesReceivedBy(getCurrentUser().getUserName());
        }
        else
        {
            return new ResponseService<>(ResponseService.RESPONSE_CODE_ERROR_NOT_LOGGEDIN);
        }
    }

    @Override
    public ResponseService<List<UserMessageEntity>> getSentMessages() {
        if (getCurrentUser().isUserLoggedIn()) {
            return messagesManagerSingletonLocal.getMessagesSentBy(getCurrentUser().getUserName());
        }
        else
        {
            return new ResponseService<>(ResponseService.RESPONSE_CODE_ERROR_NOT_LOGGEDIN);
        }
    }
    
}
