/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.util.List;
import javax.ejb.Local;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.UserMessageEntity;

/**
 *
 * @author dfortunato
 */
@Local
public interface MessagesManagerSingletonLocal {
    
    public ResponseService<UserMessageEntity> sendNewMessage(String usernameSender, String usernameReceiver, String message);
    
    public ResponseService<List<UserMessageEntity>> getAllMessages();
    
    public ResponseService<List<UserMessageEntity>> getMessagesSentBy(String username);
    
    public ResponseService<List<UserMessageEntity>> getMessagesReceivedBy(String username);
}
