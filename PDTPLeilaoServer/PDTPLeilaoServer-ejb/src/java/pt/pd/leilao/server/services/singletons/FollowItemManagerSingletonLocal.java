/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.util.List;
import javax.ejb.Local;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ResponseService;

/**
 *
 * @author dfortunato
 */
@Local
public interface FollowItemManagerSingletonLocal {
    
    public boolean isItemFollowedByUser(int itemId, String username);
    
    public ResponseService toggleItemFollowedByUser(int itemId, String username);
    
    public ResponseService<List<String>> getListUsersFollowingItem(int itemId);
    
    public ResponseService<List<ItemEntity>> getListItemsFollowing(String username);
}
