/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import pt.pd.leilao.server.model.NewsletterMessageEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.model.UserMessageEntity;
import pt.pd.leilao.server.services.utils.FileUtils;

/**
 *
 * @author dfortunato
 */
@Singleton
public class MessagesManagerSingleton implements MessagesManagerSingletonLocal {
    private static final String TAG = MessagesManagerSingleton.class.getSimpleName();
    
    private static final String FILENAME_MESSAGES = "/tmp/usermessages";
    
    private List<UserMessageEntity> listMessages;
    
    @EJB
    UserManagerSingletonLocal userManagerSingletonLocal;
    
    @Override
    public ResponseService<UserMessageEntity> sendNewMessage(String usernameSender, String usernameReceiver, String message) {
        UserEntity userSender = userManagerSingletonLocal.findUserByUsername(usernameSender);
        UserEntity userReceiver = userManagerSingletonLocal.findUserByUsername(usernameReceiver);
        if (userSender == null || userReceiver == null) {
            return new ResponseService<>(ResponseService.RESPONSE_CODE_NOT_FOUND);
        }
        
        UserMessageEntity userMessageEntity = UserMessageEntity.generateNewMessage(userSender, userReceiver, message);
        if (userMessageEntity != null) {
            userMessageEntity.setId(listMessages.size());
            listMessages.add(userMessageEntity);
            return new ResponseService<>(userMessageEntity);
        }
        else
        {
            return new ResponseService<>(ResponseService.RESPONSE_CODE_INVALID_DATA);
        }
        
    }

    @Override
    public ResponseService<List<UserMessageEntity>> getAllMessages() {
        return new ResponseService<>(listMessages);
    }

    @Override
    public ResponseService<List<UserMessageEntity>> getMessagesSentBy(String username) {
        ResponseService<List<UserMessageEntity>> response = getAllMessages();
        if (response.isSuccess()) {
            List<UserMessageEntity> listSentMessages = new ArrayList<>();
            for(UserMessageEntity userMessageEntity : response.getDataObject())
            {
                if (userMessageEntity.getUserSender().getUserName().equals(username)) {
                    listSentMessages.add(userMessageEntity);
                }
            }
            
            response = new ResponseService<>(listSentMessages);
        }
        
        return response;
    }

    @Override
    public ResponseService<List<UserMessageEntity>> getMessagesReceivedBy(String username) {
        ResponseService<List<UserMessageEntity>> response = getAllMessages();
        if (response.isSuccess()) {
            List<UserMessageEntity> listSentMessages = new ArrayList<>();
            for(UserMessageEntity userMessageEntity : response.getDataObject())
            {
                if (userMessageEntity.getUserReceive().getUserName().equals(username)) {
                    listSentMessages.add(userMessageEntity);
                }
            }
            
            response = new ResponseService<>(listSentMessages);
        }
        
        return response;
    }

    @PostConstruct
    public void loadState() {
        Logger.getLogger(TAG).log(Level.INFO, "init LoadState");
        
        // Load Newsletter
        listMessages = (List<UserMessageEntity>) FileUtils.loadData(FILENAME_MESSAGES);

        // Check if should generate Collections
        if (listMessages == null) {
            listMessages = new ArrayList<>();
        }
    }

    @PreDestroy
    public synchronized void saveState() {
        Logger.getLogger(TAG).log(Level.INFO, "init SaveState");
        
        // Save Messages
        FileUtils.saveData(FILENAME_MESSAGES, listMessages);
        
    }
    
}
