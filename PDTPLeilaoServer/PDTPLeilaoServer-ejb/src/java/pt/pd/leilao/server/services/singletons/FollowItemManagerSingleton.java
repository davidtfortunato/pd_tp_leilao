/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.NewsletterMessageEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.services.utils.FileUtils;

/**
 *
 * @author dfortunato
 */
@Singleton
public class FollowItemManagerSingleton implements FollowItemManagerSingletonLocal {
    private static final String TAG = NewsletterManagerSingleton.class.getName();
    
    private static final String FILENAME_MAP_ITEMS_FOLLOWED = "/tmp/itemsfollowed";
    private static final String FILENAME_MAP_USERS_FOLLOWING_ITEMS = "/tmp/usersfollowingitems";
    
    private Map<Integer, Set<String>> mapItemsFollowedByUsers;
    private Map<String, List<ItemEntity>> mapUsersFollowingItems;

    @EJB
    ItemsManagerSingletonLocal itemsManagerSingletonLocal;
    
    public FollowItemManagerSingleton() 
    {
        mapItemsFollowedByUsers = new HashMap<Integer, Set<String>>();
        mapUsersFollowingItems = new HashMap<String, List<ItemEntity>>();
    }
    
    @PostConstruct
    public void loadState() {
        Logger.getLogger(TAG).log(Level.INFO, "init LoadState");
        
        // Load Newsletter
        mapItemsFollowedByUsers = (Map<Integer, Set<String>>) FileUtils.loadData(FILENAME_MAP_ITEMS_FOLLOWED);
        mapUsersFollowingItems = (Map<String, List<ItemEntity>>) FileUtils.loadData(FILENAME_MAP_USERS_FOLLOWING_ITEMS);

        // Check if should generate Collections
        if (mapItemsFollowedByUsers == null) {
            mapItemsFollowedByUsers = new HashMap<>();
        }
        if (mapUsersFollowingItems == null) {
            mapUsersFollowingItems = new HashMap<>();
        }
    }

    @PreDestroy
    public synchronized void saveState() {
        Logger.getLogger(TAG).log(Level.INFO, "init SaveState");
        
        // Save Newsletters
        FileUtils.saveData(FILENAME_MAP_ITEMS_FOLLOWED, mapItemsFollowedByUsers);
        FileUtils.saveData(FILENAME_MAP_USERS_FOLLOWING_ITEMS, mapUsersFollowingItems);
        
    }

    @Override
    public boolean isItemFollowedByUser(int itemId, String username) {
        Set<String> setFollowers = getItemFollowers(itemId);
        if (setFollowers != null) {
            return setFollowers.contains(username);
        }
        return false;
    }

    @Override
    public ResponseService toggleItemFollowedByUser(int itemId, String username) {
        Set<String> setFollowers = getItemFollowers(itemId);
        ResponseService responseService = new ResponseService();
        if (setFollowers != null) {
            if (setFollowers.contains(username)) {
                responseService.setSuccess(setFollowers.remove(username));
                toggleUserFollowingItem(username, itemId, false);
            }
            else
            {
                responseService.setSuccess(setFollowers.add(username));
                toggleUserFollowingItem(username, itemId, true);
            }
        }
        else
        {
            responseService.setResponseCode(ResponseService.RESPONSE_CODE_NOT_FOUND);
        }
        
        return responseService;
    }

    private void toggleUserFollowingItem(String username, int itemId, boolean isFollowing)
    {
        List<ItemEntity> itemsFollowed = mapUsersFollowingItems.get(username);
        if (itemsFollowed == null) {
            itemsFollowed = new ArrayList<ItemEntity>();
            mapUsersFollowingItems.put(username, itemsFollowed);
        }
        
        if (isFollowing) {
            // Add Item
            ItemEntity itemEntity = itemsManagerSingletonLocal.findItemById(itemId);
            if (itemEntity != null) {
                itemsFollowed.add(itemEntity);
            }
        }
        else
        {
            Iterator<ItemEntity> itList = itemsFollowed.iterator();
            while (itList.hasNext()) {
                ItemEntity item = itList.next();
                if (item.getId() == itemId) {
                    itList.remove();
                }
            }
        }
    }
    
    @Override
    public ResponseService<List<String>> getListUsersFollowingItem(int itemId) {
        Set<String> setFollowers = getItemFollowers(itemId);
        
        if (setFollowers != null) {
            return new ResponseService<>(new ArrayList<>(setFollowers));
        }
        else
        {
            return new ResponseService<>(ResponseService.RESPONSE_CODE_NOT_FOUND);
        }
    }

    @Override
    public ResponseService<List<ItemEntity>> getListItemsFollowing(String username) {
        return new ResponseService<>(mapUsersFollowingItems.get(username));
    }
    
    private Set<String> getItemFollowers(int itemId)
    {
        Set<String> setFollowers = mapItemsFollowedByUsers.get(itemId);
        if (setFollowers == null && itemsManagerSingletonLocal.findItemById(itemId) != null) {
            setFollowers = new HashSet<>();
            mapItemsFollowedByUsers.put(itemId, setFollowers);
        }
        return setFollowers;
    }
}
