/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.singletons;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import pt.pd.leilao.server.model.UserActivationOrderEntity;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.model.enums.AdminOrderResponseType;
import pt.pd.leilao.server.model.enums.AdminOrderType;
import pt.pd.leilao.server.model.enums.NewsletterMessageType;
import pt.pd.leilao.server.model.enums.UserStateType;
import pt.pd.leilao.server.model.enums.UserType;
import pt.pd.leilao.server.model.enums.responses.ResponseUserLoginType;
import pt.pd.leilao.server.model.enums.responses.ResponseUserRegisterType;
import pt.pd.leilao.server.model.responses.ResponseUserLoginData;
import pt.pd.leilao.server.services.ClientUserServiceRemote;
import pt.pd.leilao.server.services.utils.FileUtils;
import pt.pd.leilao.server.services.utils.NewsletterUtil;

/**
 *
 * @author dfortunato
 */
@Singleton
public class UserManagerSingleton implements UserManagerSingletonLocal {

    private static final String TAG = UserManagerSingleton.class.getSimpleName();

    private static final String FILENAME_LIST_USERS = "/tmp/users";
    private static final String FILENAME_LIST_ADMINORDERS = "/tmp/adminorders";

    public static final String DEFAULT_ADMIN_USERNAME = "admin";
    private static final String DEFAULT_ADMIN_PASSWORD = "admin";

    private Map<String, UserEntity> mapUsers; // Map of Users
    private List<UserActivationOrderEntity> listAdminOrders;

    // Maps
    private Map<String, ClientUserServiceRemote> mapUsersLoggedin; // Map with the current users loggedin. Key = UserName, ClientUserService instance

    @EJB
    NewsletterManagerSingletonLocal newsletterManagerSingletonLocal;
    
    public UserManagerSingleton() {
        mapUsersLoggedin = new HashMap<>();
    }

    @Override
    public ResponseUserRegisterType registerUser(UserEntity userData) {
        if (checkUserNameExists(userData.getUserName())) {
            return ResponseUserRegisterType.USERNAME_ALREADY_EXISTS;
        }

        // Generate an UserActivationOrderEntity and register user on the list
        UserActivationOrderEntity adminOrder = new UserActivationOrderEntity();
        adminOrder.setDescription("Register a user");
        adminOrder.setTargetUser(userData);
        adminOrder.setAdminOrderType(AdminOrderType.REGISTER);
        if (addAdminOrder(adminOrder)) {
            // Config user data to be added on the list
            userData.setId(mapUsers.size());
            userData.setSaldo(0);
            userData.setUserStateType(UserStateType.PENDING_APPROVE);
            userData.setUserType(UserType.GUEST);
            putUserEntity(userData);

            return ResponseUserRegisterType.USER_REGISTER_PENDING_APPROVAL;
        } else {
            return ResponseUserRegisterType.USERNAME_ALREADY_EXISTS;
        }
    }

    @Override
    public boolean addAdminOrder(UserActivationOrderEntity adminOrder) {
                
        for (UserActivationOrderEntity order : listAdminOrders) {
            if (order.getTargetUser().getUserName().equals(adminOrder.getTargetUser().getUserName()) && order.getAdminOrderResponseType() == AdminOrderResponseType.PENDING) {
                return false;
            }
        }
        // Setup Order ID
        adminOrder.setId(listAdminOrders.size());
        listAdminOrders.add(adminOrder);
        return true;
    }

    @Override
    public ResponseUserLoginData loginUser(ClientUserServiceRemote clientUserServiceRemote, String username, String password) {
        ResponseUserLoginData responseUserLoginData = new ResponseUserLoginData();
        responseUserLoginData.setUserData(findUserByUsername(username));

        if (responseUserLoginData.getUserData() == null) {
            responseUserLoginData.setResponseUserLoginType(ResponseUserLoginType.FAILED_USERNAME_NOT_FOUND);
        } else {
            if (!responseUserLoginData.getUserData().validateCredentials(password)) {
                responseUserLoginData.setResponseUserLoginType(ResponseUserLoginType.FAILED_PASSWORD_WRONG);
            } else if (responseUserLoginData.getUserData().getUserStateType() != UserStateType.ACTIVE) {
                responseUserLoginData.setResponseUserLoginType(ResponseUserLoginType.FAILED_NOT_ACTIVE);
            } else {
                // Check if should force logout before new login
                if (mapUsersLoggedin.containsKey(username)) {
                    forceLogoutUser(username);
                }

                // Add Client Loggedin on Map
                addClientLoggedinOnMap(clientUserServiceRemote, username);

                responseUserLoginData.setResponseUserLoginType(ResponseUserLoginType.LOGIN_SUCCESS);

                Logger.getLogger(TAG).log(Level.INFO, "User logged in: " + username);
            }

        }

        return responseUserLoginData;
    }

    @Override
    public boolean replyAdminUserActivationOrder(int idAdminOrder, AdminOrderResponseType responseType) {
        UserActivationOrderEntity adminOrder = null;

        for (UserActivationOrderEntity ao : listAdminOrders) {
            if (ao.getId() == idAdminOrder) {
                adminOrder = ao;
                break;
            }
        }

        if (adminOrder != null && adminOrder.getAdminOrderResponseType() == AdminOrderResponseType.PENDING) {

            UserEntity userEntity = findUserByUsername(adminOrder.getTargetUser().getUserName());
            if (userEntity != null) {
                switch (responseType) {
                    case APPROVED:
                        switch (adminOrder.getAdminOrderType()) {
                            case REACTIVATION:
                                newsletterManagerSingletonLocal.addNewsletter(NewsletterUtil.generateUserNewsletterEntity(NewsletterMessageType.USER_REACTIVATED, userEntity));
                                userEntity.setUserStateType(UserStateType.ACTIVE);
                                break;
                            case REGISTER:
                                newsletterManagerSingletonLocal.addNewsletter(NewsletterUtil.generateUserNewsletterEntity(NewsletterMessageType.USER_APPROVED, userEntity));
                                userEntity.setUserStateType(UserStateType.ACTIVE);
                                break;
                            case USER_SUSPEND:
                                userEntity.setSuspensionReason(adminOrder.getDescription());
                                userEntity.setUserStateType(UserStateType.SUSPENDED_USER);
                                newsletterManagerSingletonLocal.addNewsletter(NewsletterUtil.generateUserNewsletterEntity(NewsletterMessageType.USER_SUSPENDED_BY_USER, userEntity));
                                forceLogoutUser(userEntity.getUserName());
                                break;
                        }
                        userEntity.setUserType(UserType.REGISTERED);
                        putUserEntity(userEntity);
                        break;
                    case DECLINED:
                        switch (adminOrder.getAdminOrderType()) {
                            case REACTIVATION:
                            case REGISTER:
                                userEntity.setUserStateType(UserStateType.SUSPENDED_ADMIN);
                                userEntity.setUserType(UserType.REGISTERED);
                                userEntity.setSuspensionReason("Registo invalido");
                                putUserEntity(userEntity);
                                newsletterManagerSingletonLocal.addNewsletter(NewsletterUtil.generateUserNewsletterEntity(NewsletterMessageType.USER_NOT_APPROVED, userEntity));
                                break;
                        }
                        break;
                }

                adminOrder.setAdminOrderResponseType(responseType);
                return true;
            }
        }

        return false;
    }

    /**
     * Force logout
     *
     * @param username
     */
    private void forceLogoutUser(String username) {
        ClientUserServiceRemote currentServiceLoggedIn = mapUsersLoggedin.get(username);
        if (currentServiceLoggedIn != null) {
            // Last Service should logout
            currentServiceLoggedIn.logout();
        }
    }

    /**
     * Logout a user
     *
     * @param username
     * @return
     */
    @Override
    public boolean logoutUser(String username) {
        ClientUserServiceRemote currentServiceLoggedIn = mapUsersLoggedin.get(username);
        if (currentServiceLoggedIn != null) {
            // Last Service should logout
            mapUsersLoggedin.remove(username);

            Logger.getLogger(TAG).log(Level.INFO, "User logout: " + username);

            // Save Current data
            saveState();

            return true;
        }
        return false;
    }

    @Override
    public List<UserEntity> getListUsers() {
        return new ArrayList<UserEntity>(mapUsers.values());
    }

    @Override
    public List<UserActivationOrderEntity> getListAdminUserActivationOrders() {
        return listAdminOrders;
    }

    /**
     * Add new logged in on map
     *
     * @param clientUserServiceRemote
     * @param username
     */
    private void addClientLoggedinOnMap(ClientUserServiceRemote clientUserServiceRemote, String username) {
        ClientUserServiceRemote currentServiceLoggedIn = mapUsersLoggedin.get(username);
        if (currentServiceLoggedIn != null) {
            // Last Service should logout
            currentServiceLoggedIn.logout();
            mapUsersLoggedin.remove(username);
        }

        // Put New Loggedin
        mapUsersLoggedin.put(username, clientUserServiceRemote);
    }

    /**
     * Check if a given UserName already exists on the list
     *
     * @param userName UserName to be validated
     * @return True if already exists, false otherwise
     */
    private boolean checkUserNameExists(String userName) {
        UserEntity user = findUserByUsername(userName);

        return user != null;
    }

    /**
     * Find a user by his Username
     *
     * @param username Username to find
     * @return UserName object if found, or null if doesn't exists
     */
    @Override
    public UserEntity findUserByUsername(String username) {
        UserEntity userData = mapUsers.get(username);
        return userData;
    }

    /**
     * Add a new user to the map
     *
     * @param userData
     */
    @Override
    public void putUserEntity(UserEntity userData) {
        if (userData != null && userData.isUserDataValid()) {

            mapUsers.put(userData.getUserName(), userData);
        }
    }

    @PostConstruct
    public void loadState() {
        Logger.getLogger(TAG).log(Level.INFO, "Init LoadState!");

        // Load Users
        mapUsers = (HashMap<String, UserEntity>) FileUtils.loadData(FILENAME_LIST_USERS);

        // Load Admin Orders
        listAdminOrders = (ArrayList<UserActivationOrderEntity>) FileUtils.loadData(FILENAME_LIST_ADMINORDERS);

        // Check if should generate Collections
        if (mapUsers == null) {
            mapUsers = new HashMap<>();
        }
        if (listAdminOrders == null) {
            listAdminOrders = new ArrayList<>();
        }

        // Check if already exists an Admin User
        UserEntity adminUser = findUserByUsername(DEFAULT_ADMIN_USERNAME);
        if (adminUser == null) {
            // Generate an Admin User
            adminUser = new UserEntity();
            adminUser.setId(mapUsers.size());
            adminUser.setUserName(DEFAULT_ADMIN_USERNAME);
            adminUser.setPassword(DEFAULT_ADMIN_PASSWORD);
            adminUser.setUserStateType(UserStateType.ACTIVE);
            adminUser.setUserType(UserType.ADMIN);
            adminUser.setSaldo(0);
            adminUser.setNome(DEFAULT_ADMIN_USERNAME);
            adminUser.setMorada("");
            putUserEntity(adminUser);
        }

    }

    @PreDestroy
    public synchronized void saveState() {
        Logger.getLogger(TAG).log(Level.INFO, "Init SaveState!");

        // Save Users
        FileUtils.saveData(FILENAME_LIST_USERS, mapUsers);
        // Save Admin orders
        FileUtils.saveData(FILENAME_LIST_ADMINORDERS, listAdminOrders);

    }

}
