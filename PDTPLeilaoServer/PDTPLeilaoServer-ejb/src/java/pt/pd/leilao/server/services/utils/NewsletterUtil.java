/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.services.utils;

import java.util.Calendar;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.NewsletterMessageEntity;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.model.enums.NewsletterMessageType;

/**
 *
 * @author dfortunato
 */
public class NewsletterUtil {

    public static NewsletterMessageEntity generateItemNewsletterEntity(NewsletterMessageType newsletterMessageType, ItemEntity itemEntity) {
        if (itemEntity != null) {
            NewsletterMessageEntity newsletterMessageEntity = new NewsletterMessageEntity();
            newsletterMessageEntity.setTimeCreated(Calendar.getInstance().getTimeInMillis());
            newsletterMessageEntity.setTargetId(itemEntity.getId());
            StringBuilder messageBuilder = new StringBuilder();

            switch (newsletterMessageType) {
                case ITEM_ADDED_NEW_ITEM:
                    messageBuilder.append("O item " + itemEntity.getDescription() + " foi adicionado pelo utilizador " + itemEntity.getUserSeller().getUserName());
                    break;
                case ITEM_CANCELLED_BY_ADMIN:
                    messageBuilder.append("O item " + itemEntity.getDescription() + " foi cancelado pelo admin");
                    break;
                case ITEM_SOLD:
                    messageBuilder.append("O item " + itemEntity.getDescription() + " foi vendido ao utilizador " + itemEntity.getUserBuyer() + " por " + Utils.getValorEuros(itemEntity.getLastPrice()));
                    break;

            }
            newsletterMessageEntity.setMessage(messageBuilder.toString());

            return newsletterMessageEntity;
        } else {
            return null;
        }
    }

    public static NewsletterMessageEntity generateUserNewsletterEntity(NewsletterMessageType newsletterMessageType, UserEntity userEntity) {
        if (userEntity != null) {
            NewsletterMessageEntity newsletterMessageEntity = new NewsletterMessageEntity();
            newsletterMessageEntity.setTimeCreated(Calendar.getInstance().getTimeInMillis());
            newsletterMessageEntity.setTargetId(userEntity.getId());
            StringBuilder messageBuilder = new StringBuilder();

            switch (newsletterMessageType) {
                case USER_APPROVED:
                    messageBuilder.append("O utilizador " + userEntity.getUserName() + " registou-se e foi aprovado pelo admin.");
                    break;
                case USER_NOT_APPROVED:
                    messageBuilder.append("O registo do utilizador " + userEntity.getUserName() + " nao foi aprovado pelo admin.");
                    if (!Utils.isStringOnlySpacesOrEmpty(userEntity.getSuspensionReason())) {
                        messageBuilder.append(" Motivo: " + userEntity.getSuspensionReason());
                    }
                    break;
                case USER_REACTIVATED:
                    messageBuilder.append("O utilizador " + userEntity.getUserName() + " foi reactivado");
                    break;
                case USER_SUSPENDED_BY_USER:
                    messageBuilder.append("O utilizador " + userEntity.getUserName() + " foi suspenso apos pedido do proprio utilizador.");
                    if (!Utils.isStringOnlySpacesOrEmpty(userEntity.getSuspensionReason())) {
                        messageBuilder.append(" Motivo: " + userEntity.getSuspensionReason());
                    }
                    break;
                case USER_SUSPENDED_BY_ADMIN:
                    messageBuilder.append("O Admin decidiu suspender o utilizador " + userEntity.getUserName());
                    if (!Utils.isStringOnlySpacesOrEmpty(userEntity.getSuspensionReason())) {
                        messageBuilder.append(". Motivo: " + userEntity.getSuspensionReason());
                    };
                    break;
            }
            newsletterMessageEntity.setMessage(messageBuilder.toString());

            return newsletterMessageEntity;
        }
        else
        {
            return null;
        }
        
    }

}
