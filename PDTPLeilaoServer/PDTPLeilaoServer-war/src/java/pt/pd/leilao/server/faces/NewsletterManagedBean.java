/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.server.faces;

import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import pt.pd.leilao.server.model.NewsletterMessageEntity;
import pt.pd.leilao.server.services.singletons.NewsletterManagerSingletonLocal;

/**
 *
 * @author dfortunato
 */
@ManagedBean(name = "newsletterManagedBean")
@RequestScoped
public class NewsletterManagedBean {

    @EJB
    public NewsletterManagerSingletonLocal newsletterManagerSingletonLocal;
    
    /**
     * Creates a new instance of NewsletterManagedBean
     */
    public NewsletterManagedBean() {
    }
    
    public String getHelloNewsletter()
    {
        return "Mensagem a partir de " + NewsletterManagedBean.class.getName();
    }
    
    public String getListNewsletter()
    {
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append("****************");
        
        for(NewsletterMessageEntity newsletterMessageEntity : newsletterManagerSingletonLocal.getListNewsletter())
        {
            strBuilder.append("\n").append(newsletterMessageEntity.generateListLineDescription());
        }
        strBuilder.append("****************");
        
        return strBuilder.toString();
    }
    
}
