/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui;

/**
 *
 * @author dfortunato
 */
public class UIHelpCommandsMenu extends AbsUIMenu{

    public UIHelpCommandsMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        
        StringBuilder menuBuilder = new StringBuilder();
        
        menuBuilder.append(getHeaderMessage("Comandos disponiveis"))
                .append("\n" + COMMAND_INPUT_HELP + ": Abre este ecra.")
                .append("\n" + COMMAND_INPUT_BACK_PAGE + ": Volta para a pagina anterior.")
                .append("\n" + COMMAND_INPUT_EXIT_APP +": Sai imediatamente da aplicacao")
                .append("\n" + COMMAND_INPUT_NEWSLETTER +": Apresenta Newsletter")
                .append("\n" + COMMAND_INPUT_LOGOUT +": Efectua de imediato o logout da conta actual e retorna ao ecra inicial");
        
        return menuBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        return super.optionSelect(stepMenu, input);
    }
    
    
}
