/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui;

import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.server.model.ResponseService;

/**
 *
 * @author dfortunato
 */
public class UIRequestReactivationUserMenu extends AbsUIMenu{

    // Input Data
    private String username;
    private String password;
    private String reasonDescription;
    
    public UIRequestReactivationUserMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder menuBuilder = new StringBuilder();
        
        switch(stepMenu)
        {
            case 0:
                menuBuilder.append(getHeaderMessage("Formulario de reactivacao de user"))
                        .append("\nInsira o username: " );
                break;
            case 1:
                menuBuilder.append("\nInsira a password: ");
                break;
            case 2:
                menuBuilder.append("\nIndique o motivo para a sua reactivacao: ");
                break;
        }
        
        return menuBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);
    
        if (uiMenu == null) {
            switch(stepMenu)
            {
                case 0:
                    username = input;
                    nextStep();
                    uiMenu = this;
                    break;
                case 1:
                    password = input;
                    nextStep();
                    uiMenu = this;
                    break;
                case 2:
                    reasonDescription = input;
                    
                    ResponseService responseService = LeilaoRemoteServices.getInstance().getClientGuestServiceRemote().postRequestReactivate(username, password, reasonDescription);
                    if (responseService.isSuccess()) {
                        displayMessage("\nO seu pedido foi registado com sucesso. Posteriormente ser analisado pelo administrador", true);
                    }
                    else
                    {
                        displayMessage("\nOcorreu um erro ao registar o seu pedido. ErroCode: " + responseService.getResponseMessage(), true);
                    }
                    uiMenu = getParentMenu();
                    resetMenu();
                    break;
                default:
                    uiMenu = new UIInvalidOptionMenu(this);
            }
        }
        
        return uiMenu;
    }

    @Override
    protected UIMenuInputType getStepMenuInputType(int stepMenu) {
        switch(stepMenu)
        {
            case 1:
                return UIMenuInputType.PASSWORD_INPUT;
        }
        return super.getStepMenuInputType(stepMenu);
    }

    @Override
    public void resetMenu() {
        super.resetMenu();
        username = null;
        password = null;
        reasonDescription = null;
    }
    
    
    
    
}
