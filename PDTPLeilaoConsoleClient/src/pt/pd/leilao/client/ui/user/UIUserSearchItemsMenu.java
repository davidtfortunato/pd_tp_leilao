/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.user;

import java.util.ArrayList;
import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.client.ui.UIInvalidOptionMenu;
import pt.pd.leilao.server.model.IEntityToString;

/**
 *
 * @author dfortunato
 */
public class UIUserSearchItemsMenu extends UIUserBaseMenu{

    public UIUserSearchItemsMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder menuBuilder = new StringBuilder();
        
        menuBuilder.append(getHeaderMessage("Pesquisar items"))
                .append(generateOptionLine("Listar items por categoria", 1))
                .append(generateOptionLine("Listar items por descricao", 2))
                .append(generateOptionLine("Listar items por preco", 3))
                .append(generateOptionLine("Listar items por preco compra ja", 4))
                .append(generateOptionLine("Listar items por prazo", 5))
                .append(generateOptionLine("Visualizar detalhes de item", 6))
                .append(generateOptionLine("Pagina anterior", 7))
                .append(getSeparatorLine())
                .append(getMessageSelectOption());
    
        return menuBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input); 
        
        if (uiMenu == null) {
            uiMenu = this;
            switch(input)
            {
                case "1":
                    displayMessage(generateListEntities(new ArrayList<IEntityToString>(LeilaoRemoteServices.getInstance().getClientItemsServiceRemote().getListItemsByCategory().getDataObject()), "Nao existem items disponiveis"), true);
                    break;
                case "2":
                    displayMessage(generateListEntities(new ArrayList<IEntityToString>(LeilaoRemoteServices.getInstance().getClientItemsServiceRemote().getListItemsByDescription().getDataObject()), "Nao existem items disponiveis"), true);
                    break;
                case "3":
                    displayMessage(generateListEntities(new ArrayList<IEntityToString>(LeilaoRemoteServices.getInstance().getClientItemsServiceRemote().getListItemsByPrice().getDataObject()), "Nao existem items disponiveis"), true);
                    break;
                case "4":
                    displayMessage(generateListEntities(new ArrayList<IEntityToString>(LeilaoRemoteServices.getInstance().getClientItemsServiceRemote().getListItemsByBuyNowPrice().getDataObject()), "Nao existem items disponiveis"), true);
                    break;
                case "5":
                    displayMessage(generateListEntities(new ArrayList<IEntityToString>(LeilaoRemoteServices.getInstance().getClientItemsServiceRemote().getListItemsByEndDate().getDataObject()), "Nao existem items disponiveis"), true);
                    break;
                case "6":
                    uiMenu = new UIUserOpenItemDetailsMenu(this);
                    break;
                case "7":
                    uiMenu = getParentMenu();
                    break;
                default:
                    uiMenu = new UIInvalidOptionMenu(this);
            }
        }
        
        return uiMenu;
    }
    
    
    
}
