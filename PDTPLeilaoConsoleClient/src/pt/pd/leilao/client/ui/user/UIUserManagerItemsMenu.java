/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.user;

import java.util.ArrayList;
import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.client.ui.UIInvalidOptionMenu;
import pt.pd.leilao.client.ui.UINewsletterViewMenu;
import pt.pd.leilao.server.model.IEntityToString;

/**
 *
 * @author dfortunato
 */
public class UIUserManagerItemsMenu extends UIUserBaseMenu{

    public UIUserManagerItemsMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder menuBuilder = new StringBuilder();
        
        // Generate Admin menu
        menuBuilder.append(getHeaderMessage("Gerir Items"))
                .append(generateOptionLine("Listar Items à venda", 1))
                .append(generateOptionLine("Adicionar Item", 2))
                .append(generateOptionLine("Listar Items a seguir", 3))
                .append(generateOptionLine("Listar Items Comprados", 4))
                .append(generateOptionLine("Listar Items Vendidos", 5))
                .append(generateOptionLine("Pagina anterior", 6))
                .append(getSeparatorLine())
                .append(getMessageSelectOption());
        
        return menuBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);
        
        if (uiMenu == null) {
            // Check if still logged in as admin
            uiMenu = this;
            switch(input)
            {
                case "1":
                    displayMessage(generateListEntities(new ArrayList<IEntityToString>(LeilaoRemoteServices.getInstance().getClientUserServiceRemote().getMyListItems().getDataObject()), "Não existem items disponiveis"), true);
                    break;
                case "2":
                    uiMenu = new UIUserAddItemMenu(this);
                    break;
                case "3":
                    displayMessage(generateListEntities(new ArrayList<IEntityToString>(LeilaoRemoteServices.getInstance().getClientUserServiceRemote().getListItemsFollowed().getDataObject()), "Não existem items a serem seguidos"), true);
                    break;
                case "4":
                    displayMessage(generateListEntities(new ArrayList<IEntityToString>(LeilaoRemoteServices.getInstance().getClientUserServiceRemote().getListItemsBought().getDataObject()), "Ainda não comprou nenhum item"), true);
                    break;
                case "5":
                    displayMessage(generateListEntities(new ArrayList<IEntityToString>(LeilaoRemoteServices.getInstance().getClientUserServiceRemote().getListItemsSold().getDataObject()), "Ainda não vendeu nenhum item"), true);
                    break;
                case "6":
                    uiMenu = getParentMenu();
                    break;
                default:
                    uiMenu = new UIInvalidOptionMenu(this);
            }
        }
        
        return uiMenu;
    }
    
    
    
}
