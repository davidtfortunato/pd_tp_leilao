/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.user;

import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.server.model.UserEntity;

/**
 *
 * @author dfortunato
 */
public class UIUserEditAccountMenu extends UIUserBaseMenu{

    public UIUserEditAccountMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder menuBuilder = new StringBuilder();
        
        
        
        switch(stepMenu)
        {
            case 0:
                menuBuilder.append(getHeaderMessage("Editar Conta"))
                        .append("\nInsira novo nome (em branco caso nao pretenda alterar): ");
                break;
            case 1:
                menuBuilder.append("\nInsira nova morada (em branco caso nao pretenda alterar): ");
                break;
            default:
                menuBuilder.append(getSeparatorLine())
                        .append("\n" + LeilaoRemoteServices.getInstance().getCurrentUser().generateEntityDetailsDescription())
                        .append(getSeparatorLine());
        }
                
        
        return menuBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);
        
        if (uiMenu == null) {
            switch(stepMenu)
            {
                case 0:
                    if (input != null && !input.isEmpty()) {
                        UserEntity newUserData = new UserEntity();
                        String oldName = LeilaoRemoteServices.getInstance().getCurrentUser().getNome();
                        String newName = input;
                        newUserData.setNome(newName);
                        if (LeilaoRemoteServices.getInstance().getClientUserServiceRemote().updateUserData(newUserData).isSuccess()) {
                            displayMessage("\nNome alterado de " + oldName + " para " + newName, true);
                        }
                    }
                    nextStep();
                    uiMenu = this;
                    break;
                    
                case 1:
                    if (input != null && !input.isEmpty()) {
                        UserEntity newUserData = new UserEntity();
                        String oldMorada = LeilaoRemoteServices.getInstance().getCurrentUser().getMorada();
                        String newMorada = input;
                        newUserData.setMorada(newMorada);
                        if (LeilaoRemoteServices.getInstance().getClientUserServiceRemote().updateUserData(newUserData).isSuccess()) {
                            displayMessage("\nMorada alterada de " + oldMorada + " para " + newMorada, true);
                        }
                    }
                    nextStep();
                    uiMenu = this;
                    break;
                default:
                    uiMenu = getParentMenu();
                    resetMenu();
            }
        }
        
        return uiMenu;
    }
    
    
    
}
