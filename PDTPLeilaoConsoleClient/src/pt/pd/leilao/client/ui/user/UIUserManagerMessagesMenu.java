/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.user;

import java.util.ArrayList;
import java.util.List;
import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.client.ui.UIInvalidOptionMenu;
import pt.pd.leilao.server.model.IEntityToString;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.UserMessageEntity;

/**
 *
 * @author dfortunato
 */
public class UIUserManagerMessagesMenu extends UIUserBaseMenu{

    public UIUserManagerMessagesMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder menuBuilder = new StringBuilder();
        
        menuBuilder.append(getHeaderMessage("Gerir mensagens"))
                .append(generateOptionLine("Enviar nova mensagem", 1))
                .append(generateOptionLine("Consultar mensagens recebidas", 2))
                .append(generateOptionLine("Consultar mensagens enviadas", 3))
                .append(generateOptionLine("Pagina anterior", 4))
                .append(getSeparatorLine())
                .append(getMessageSelectOption());
        
        return menuBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);
    
        if (uiMenu == null) {
            switch(input)
            {
                case "1":
                    uiMenu = new UIUserSendMessageMenu(this, null);
                    break;
                case "2":
                    ResponseService<List<UserMessageEntity>> response = LeilaoRemoteServices.getInstance().getClientUserServiceRemote().getReceivedMessages();
                    if (response.isSuccess()) {
                        displayMessage(generateListEntities(new ArrayList<IEntityToString>(response.getDataObject()), "Ainda nao recebeu nenhuma mensagem"), true);
                    }
                    else
                    {
                        displayMessage("\nOcorreu um erro ao tentar obter as mensagens. ErrorCode: " + response.getResponseMessage(), true);
                    }
                    uiMenu = this;
                    break;
                case "3":
                    response = LeilaoRemoteServices.getInstance().getClientUserServiceRemote().getSentMessages();
                    if (response.isSuccess()) {
                        displayMessage(generateListEntities(new ArrayList<IEntityToString>(response.getDataObject()), "Ainda nao enviou qualquer mensagem"), true);
                    }
                    else
                    {
                        displayMessage("\nOcorreu um erro ao tentar obter as mensagens. ErrorCode: " + response.getResponseMessage(), true);
                    }
                    uiMenu = this;
                    break;
                case "4":
                    uiMenu = getParentMenu();
                    break;
                default:
                    uiMenu = new UIInvalidOptionMenu(this);
            }
        }
        
        return uiMenu;
    }
    
    
    
}
