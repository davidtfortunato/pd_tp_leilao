/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.user;

import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.server.model.ResponseService;

/**
 *
 * @author dfortunato
 */
public class UIUserOrderSuspendAccountMenu extends UIUserBaseMenu{

    public UIUserOrderSuspendAccountMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder menuBuilder = new StringBuilder();
        
        menuBuilder.append(getHeaderMessage("Registar pedido de suspensao de conta"))
                .append("\nInsira o motivo da sua intencao de suspensao de conta: ");
        return menuBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);
    
        if (uiMenu == null) {
            if (input == null || input.isEmpty()) {
                // Should set some reason
                displayMessage("\nDeve inserir o motivo.", true);
                uiMenu = this;
            }
            else
            {
                ResponseService response = LeilaoRemoteServices.getInstance().getClientUserServiceRemote().suspendUser(input);
                if (response.isSuccess()) {
                    displayMessage("\nPedido de suspensao enviado com sucesso. O seu pedido sera posteriormente analisado pelo administrador", true);
                }
                else
                {
                    displayMessage("\nNao foi possivel enviar o seu pedido, possivelmente porque ja tem um pedido pendente. ErrorCode: " + response.getResponseMessage(), true);
                }
                uiMenu = getParentMenu();
            }
        }
        
        return uiMenu;
    }
    
    
    
}
