/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.user;

import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.server.model.ResponseService;

/**
 *
 * @author dfortunato
 */
public class UIUserEditPasswordMenu extends UIUserBaseMenu{

    private String oldPassword;
    private String newPassword;
    
    public UIUserEditPasswordMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder menuBuilder = new StringBuilder();
        
        switch(stepMenu)
        {
            case 0:
                menuBuilder.append(getHeaderMessage("Editar password"))
                   .append("\nInsira a password antiga: ");
                break;
            case 1:
                menuBuilder
                   .append("\nInsira a password nova: ");
                break;
            case 2:
                menuBuilder
                   .append("\nRepita a password nova: ");
        }
        
        return menuBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input); 
        
        if (uiMenu == null) {
            switch(stepMenu)
            {
                case 0:
                    oldPassword = input;
                    nextStep();
                    uiMenu = this;
                    break;
                case 1:
                    newPassword = input;
                    
                    nextStep();
                    uiMenu = this;
                    break;
                case 2:
                    if (input != null && input.equals(newPassword)) {
                        ResponseService responseService = LeilaoRemoteServices.getInstance().getClientUserServiceRemote().changePassword(oldPassword, newPassword);
                    
                        switch(responseService.getResponseCode())
                        {
                            case ResponseService.RESPONSE_CODE_SUCCESS:
                                displayMessage("\nA sua password foi alterada com sucesso.", true);
                                break;
                            case ResponseService.RESPONSE_CODE_ERROR_NOT_LOGGEDIN:
                                displayMessage("\nA sua sessao expirou. Nao foi possivel alterar a password", true);
                                break;
                            default:
                                displayMessage("\nNao foi possivel alterar a password. ErrorCode: " + responseService.getResponseMessage(), true);
                        }
                        
                    }
                    else
                    {
                        displayMessage("\nA password repetida nao corresponde à inserida anteriormente", true);
                    }
                    resetMenu();
                    uiMenu = getParentMenu();
                    
                    break;
            }
        }
        
        return uiMenu;
    }

    @Override
    protected UIMenuInputType getStepMenuInputType(int stepMenu) {
        return UIMenuInputType.PASSWORD_INPUT;
    }
    
    @Override
    public void resetMenu() {
        super.resetMenu(); 
        oldPassword = null;
        newPassword = null;
    }
    
    
    
}
