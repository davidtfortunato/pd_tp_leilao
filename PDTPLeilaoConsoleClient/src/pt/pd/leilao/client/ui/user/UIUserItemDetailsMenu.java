/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.user;

import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.client.ui.UIFeatureNotImplementedMenu;
import pt.pd.leilao.client.ui.UIInvalidOptionMenu;
import pt.pd.leilao.server.model.ItemEntity;

/**
 *
 * @author dfortunato
 */
public class UIUserItemDetailsMenu extends UIUserBaseMenu{

    // Data
    private ItemEntity itemEntityOpened;
    
    public UIUserItemDetailsMenu(AbsUIMenu parentMenu, ItemEntity itemEntity) {
        super(parentMenu);
        this.itemEntityOpened = itemEntity;
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder menuBuilder = new StringBuilder();
        
        boolean isFollowingItem = LeilaoRemoteServices.getInstance().getClientUserServiceRemote().isFollowingItem(itemEntityOpened.getId());
        
        menuBuilder.append(getHeaderMessage("Detalhes de item"))
                .append("\n" + itemEntityOpened.generateEntityDetailsDescription())
                .append(getSeparatorLine())
                .append(generateOptionLine("Licitar", 1))
                .append(generateOptionLine("Enviar mensagem ao vendedor", 2))
                .append(generateOptionLine(isFollowingItem ? "Parar de seguir item" : "Seguir item", 3))
                .append(generateOptionLine("Denunciar Item", 4))
                .append(generateOptionLine("Denunciar vendedor", 5))
                .append(generateOptionLine("Pagina anterior", 6))
                .append(getSeparatorLine())
                .append(getMessageSelectOption());
        
        return menuBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input); 
    
        if (uiMenu == null) {
            switch(input)
            {
                case "1":
                    uiMenu = new UIFeatureNotImplementedMenu(this);
                    break;
                case "2":
                    uiMenu = new UIUserSendMessageMenu(this, itemEntityOpened.getUserSeller().getUserName());
                    break;
                case "3":
                    LeilaoRemoteServices.getInstance().getClientUserServiceRemote().toggleFollowItem(itemEntityOpened.getId());
                    uiMenu = this;
                    break;
                case "4":
                    uiMenu = new UIFeatureNotImplementedMenu(this);
                    break;
                case "5":
                    uiMenu = new UIFeatureNotImplementedMenu(this);
                    break;
                case "6":
                    uiMenu = getParentMenu();
                    break;
                default:
                    uiMenu = new UIInvalidOptionMenu(this);
            }
        }
        
        return uiMenu;
    }
    
}
