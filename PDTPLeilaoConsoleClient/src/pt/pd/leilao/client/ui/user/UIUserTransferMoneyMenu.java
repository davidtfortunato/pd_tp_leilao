/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.user;

import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.server.model.ResponseService;

/**
 *
 * @author dfortunato
 */
public class UIUserTransferMoneyMenu extends UIUserBaseMenu{

    public UIUserTransferMoneyMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder menuBuilder = new StringBuilder();
        
        menuBuilder.append(getHeaderMessage("Efectuar carregamento"))
                .append("\nIndique o valor que pretende carregar a sua conta: ");
        
        return menuBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);
        
        if (uiMenu == null) {
            if (input != null && !input.isEmpty()) {
                
                try
                {
                    float transferedMoney = Float.parseFloat(input);
                    if (transferedMoney > 0) {
                        ResponseService responseService = LeilaoRemoteServices.getInstance().getClientUserServiceRemote().transferMoney(transferedMoney);
                        
                        if (responseService.isSuccess()) {
                            displayMessage("\nTransferencia efectuada com sucesso.\nSaldo actual: " + LeilaoRemoteServices.getInstance().getCurrentUser().getSaldoString(), true);
                        }
                        else
                        {
                            displayMessage("\nOcorreu um erro a transferir o dinheiro. ", true);
                        }
                    }
                    else
                    {
                        displayMessage("\nO valor tem de ser superior a zero", true);
                    }
                }
                catch(NumberFormatException ex)
                {
                    displayMessage("\nO valor inserido não é valido", true);
                }
                uiMenu = getParentMenu();
            }
        }
        
        return uiMenu;
    }
    
}
