/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.user;

import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.client.ui.UIInvalidOptionMenu;
import pt.pd.leilao.client.ui.UINewsletterViewMenu;

/**
 *
 * @author dfortunato
 */
public class UIUserRegisteredInitialMenu extends UIUserBaseMenu {

    public UIUserRegisteredInitialMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder menuBuilder = new StringBuilder();

        // Generate Admin menu
        menuBuilder.append(getHeaderMessage("Bem Vindo " + LeilaoRemoteServices.getInstance().getCurrentUser().getNome()))
                .append(generateOptionLine("Gerir Conta", 1))
                .append(generateOptionLine("Gerir Items", 2))
                .append(generateOptionLine("Gerir Mensagens", 3))
                .append(generateOptionLine("Pesquisar Items", 4))
                .append(generateOptionLine("Visualizar Newsletter", 5))
                .append(generateOptionLine("Logout", 6))
                .append(getSeparatorLine())
                .append(getMessageSelectOption());

        return menuBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);

        if (uiMenu == null) {
            // Check if still logged in as admin
            uiMenu = this;
            switch (input) {
                case "1":
                    uiMenu = new UIUserManagerAccountMenu(this);
                    break;
                case "2":
                    uiMenu = new UIUserManagerItemsMenu(this);
                    break;
                case "3":
                    uiMenu = new UIUserManagerMessagesMenu(this);
                    break;
                case "4":
                    uiMenu = new UIUserSearchItemsMenu(this);
                    break;
                case "5":
                    uiMenu = new UINewsletterViewMenu(this);
                    break;
                case "6":
                    LeilaoRemoteServices.getInstance().getClientUserServiceRemote().logout();
                    uiMenu = getParentMenu();
                    break;
                default:
                    uiMenu = new UIInvalidOptionMenu(this);
            }
        }

        return uiMenu;
    }

}
