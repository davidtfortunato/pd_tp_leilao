/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.user;

import java.util.ArrayList;
import java.util.Calendar;
import pt.pd.leilao.client.Utils;
import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.server.model.CategoryEntity;
import pt.pd.leilao.server.model.IEntityToString;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ResponseService;

/**
 *
 * @author dfortunato
 */
public class UIUserAddItemMenu extends UIUserBaseMenu{

    private ItemEntity generatedItemEntity;
    
    public UIUserAddItemMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder stringBuilder = new StringBuilder();
        switch(stepMenu)
        {
            case 0:
                stringBuilder.append(getHeaderMessage("Adicionar novo item para venda"))
                        .append("\nInsira uma descricao do item: ");
                break;
            case 1:
                // Preco inicial
                stringBuilder
                        .append("\nInsira um valor inicial (superior a zero): ");
                break;
            case 2:
                // Preco Compra Ja
                stringBuilder
                        .append("\nInsira um valor Compra Ja. Deixe em branco caso nao pretenda inserir um valor compra ja): ");
                break;
            case 3:
                // Categoria
                stringBuilder
                        .append(generateListEntities(new ArrayList<IEntityToString>(LeilaoRemoteServices.getInstance().getClientItemsServiceRemote().getListCategories()), "\nNao existem categorias disponiveis"))
                        .append("\nInsira o nome da categoria da lista: ");
                break;
            case 4:
                // End Date em minutos
                stringBuilder.append(getSeparatorLine())
                        .append("\nInsira (em minutos) quanto tempo pretende que o seu item\nse mentenha disponivel para venda: ");
                break;
            case 5:
                // End Date em minutos
                stringBuilder.append(getSeparatorLine())
                        .append("\nO Item gerado: " + "\n" + generatedItemEntity.generateEntityDetailsDescription())
                        .append(getSeparatorLine())
                        .append("\nPrende publicar este item? (S/N)");
                break;
        }
        return stringBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);
        
        if (uiMenu == null) {
            uiMenu = this;
            switch(stepMenu)
            {
                case 0:
                    if (input != null && !input.isEmpty()) {
                        generatedItemEntity = new ItemEntity();
                        generatedItemEntity.setDescription(input);
                        generatedItemEntity.setUserSeller(LeilaoRemoteServices.getInstance().getCurrentUser());
                        nextStep();
                    }
                    else
                    {
                        displayMessage("\nA descricao nao pode ser vazia. Por favor insira uma descricao", true);
                    }
                    break;
                case 1:
                    try
                    {
                       float initialValue = Float.parseFloat(input);
                        if (initialValue > 0) {
                            generatedItemEntity.setInitialPrice(initialValue);
                            nextStep();
                        }
                        else
                        {
                            displayMessage("\nO valor tem de ser superior a zero", true);
                        }
                    }
                    catch(NumberFormatException ex)
                    {
                        displayMessage("\nO valor inserido não é valido", true);
                    }
                    
                    break;
                case 2:
                    if (input.equalsIgnoreCase("0") || input == null || input.isEmpty()) {
                        nextStep();
                    }
                    else
                    {
                        try
                        {
                            float buyNowPrice = Float.parseFloat(input);
                            if (buyNowPrice > 0) {
                                generatedItemEntity.setBuyNowPrice(buyNowPrice);
                                nextStep();
                            }
                            else
                            {
                                displayMessage("\nO valor tem de ser superior a zero", true);
                            }
                        }
                        catch(NumberFormatException ex)
                        {
                            displayMessage("\nO valor inserido não é valido", true);
                        }
                    }
                    
                    break;
                case 3:
                    CategoryEntity categoryEntity = LeilaoRemoteServices.getInstance().getClientItemsServiceRemote().findCategoryByName(input);
                    if (categoryEntity != null) {
                        generatedItemEntity.setCategory(categoryEntity);
                        nextStep();
                    }
                    else
                    {
                        displayMessage("\nA categoria " + input + " nao foi encontrada. Insira por favor o nome da categoria existente na lista", true);
                    }
                    break;
                case 4:
                    if (Utils.isInteger(input) && Integer.parseInt(input) > 0) {
                        int minutes = Integer.parseInt(input);
                        long endDate = Calendar.getInstance().getTimeInMillis() + (minutes * 60000);
                        generatedItemEntity.setEndDate(endDate);
                        nextStep();
                    }
                    else
                    {
                        displayMessage("\nO valor inserido nao é valido. Insira por favor o valor em numerico e superior a zero", true);
                    }
                    break;
                case 5:
                    if (input.equalsIgnoreCase("S")) {
                        ResponseService response = LeilaoRemoteServices.getInstance().getClientUserServiceRemote().addItem(generatedItemEntity);
                        if (response.isSuccess()) {
                            displayMessage("\nO item foi publicado com sucesso!", true);
                        }
                        else
                        {
                            displayMessage("\nNao foi possivel publicar o item - ErrorCode: " + response.getResponseMessage(), true);
                        }
                        
                    }
                    else
                    {
                        displayMessage("\nA publicacao do item foi cancelada. Vai voltar para o ecra anterior", true);
                    }
                    resetMenu();
                    uiMenu = getParentMenu();
                    break;
            }
        }
        
        return uiMenu;
    }

    @Override
    public void resetMenu() {
        super.resetMenu(); 
        generatedItemEntity = null;
    }
    
    
    
}
