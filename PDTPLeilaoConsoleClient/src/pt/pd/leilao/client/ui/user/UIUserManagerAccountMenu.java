/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.user;

import java.util.ArrayList;
import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.client.ui.UIInvalidOptionMenu;
import pt.pd.leilao.server.model.IEntityToString;

/**
 *
 * @author dfortunato
 */
public class UIUserManagerAccountMenu extends UIUserBaseMenu{
    
    public UIUserManagerAccountMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder menuBuilder = new StringBuilder();
        
        // Generate Admin menu
        menuBuilder.append(getHeaderMessage("Gerir Conta"))
                .append(generateOptionLine("Visualizar detalhes da conta", 1))
                .append(generateOptionLine("Editar conta", 2))
                .append(generateOptionLine("Suspender conta", 3))
                .append(generateOptionLine("Efectuar carregamento", 4))
                .append(generateOptionLine("Alterar password", 5))
                .append(generateOptionLine("Pagina anterior", 6))
                .append(getSeparatorLine())
                .append(getMessageSelectOption());
        
        return menuBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);
        
        if (uiMenu == null) {
            // Check if still logged in as admin
            uiMenu = this;
            switch(input)
            {
                case "1":
                    StringBuilder userDetails = new StringBuilder();
                    userDetails.append(getSeparatorLine())
                            .append("\n" + LeilaoRemoteServices.getInstance().getCurrentUser().generateEntityDetailsDescription())
                            .append(getSeparatorLine());
                    displayMessage(userDetails.toString(), true);
                    break;
                case "2":
                    uiMenu = new UIUserEditAccountMenu(this);
                    break;
                case "3":
                    uiMenu = new UIUserOrderSuspendAccountMenu(this);
                    break;
                case "4":
                    uiMenu = new UIUserTransferMoneyMenu(this);
                    break;
                case "5":
                    uiMenu = new UIUserEditPasswordMenu(this);
                    break;
                case "6":
                    uiMenu = getParentMenu();
                    break;
                default:
                    uiMenu = new UIInvalidOptionMenu(this);
            }
        }
        
        return uiMenu;
    }
    
    
    
}
