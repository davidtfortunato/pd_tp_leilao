/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.user;

import pt.pd.leilao.client.Utils;
import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ResponseService;

/**
 *
 * @author dfortunato
 */
public class UIUserOpenItemDetailsMenu extends UIUserBaseMenu{

    public UIUserOpenItemDetailsMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder menuBuilder = new StringBuilder();
    
        menuBuilder.append(getSeparatorLine())
                .append("\nIndique o ID do item que pretende visualizar: ");
        
        return menuBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input); 
        
        if (uiMenu == null) {
            if (input != null && !input.isEmpty() && Utils.isInteger(input)) {
                ResponseService<ItemEntity> response = LeilaoRemoteServices.getInstance().getClientItemsServiceRemote().findItemEntityById(Integer.parseInt(input));
            
                if (response.isSuccess()) {
                    uiMenu = new UIUserItemDetailsMenu(getParentMenu(), response.getDataObject());
                }
                else
                {
                    displayMessage("\nNao foi encontrado o Item pretendido. ErrorCode: " + response.getResponseMessage(), true);
                    uiMenu = getParentMenu();
                }
            }
            else
            {
                displayMessage("\nO ID inserido nao é valido.", true);
                uiMenu = getParentMenu();
            }
        }
        
        return uiMenu;
    }
    
}
