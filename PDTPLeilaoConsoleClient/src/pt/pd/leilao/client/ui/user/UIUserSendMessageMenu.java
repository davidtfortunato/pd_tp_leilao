/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.user;

import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.UserMessageEntity;

/**
 *
 * @author dfortunato
 */
public class UIUserSendMessageMenu extends UIUserBaseMenu{

    // Data
    private String usernameReceiver;
    private boolean alreadyDisplayedHeader;
    
    public UIUserSendMessageMenu(AbsUIMenu parentMenu, String usernameReceiver) {
        super(parentMenu);
        this.usernameReceiver = usernameReceiver;
        this.alreadyDisplayedHeader = false;
        if (usernameReceiver != null && !usernameReceiver.isEmpty()) {
            // Don't need to ask the username
            nextStep();
        }
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder menuBuilder = new StringBuilder();
    
        if (!alreadyDisplayedHeader) {
            menuBuilder.append(getHeaderMessage("Enviar nova mensagem"));
            alreadyDisplayedHeader = true;
        }
        
        switch(stepMenu)
        {
            case 0:
                menuBuilder.append("\nIndique o username para quem quer enviar a mensagem: ");
                break;
            case 1:
                menuBuilder.append("\nEscreva a sua mensagem e enter para enviar: ");
                break;
        }
        
        return menuBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);
    
        if (uiMenu == null) {
            switch(stepMenu)
            {
                case 0:
                    if (input != null && !input.isEmpty()) {
                        usernameReceiver = input;
                        nextStep();
                    }
                    else
                    {
                        displayMessage("\nO Username do destinatario nao pode ser vazio.", true);
                    }
                    uiMenu = this;
                    break;
                case 1:
                    if (input != null && !input.isEmpty()) {
                        ResponseService<UserMessageEntity> response = LeilaoRemoteServices.getInstance().getClientUserServiceRemote().sendUserMessage(usernameReceiver, input);
                        if (response.isSuccess()) {
                            StringBuilder responseMessage = new StringBuilder();
                            responseMessage.append(getSeparatorLine())
                                    .append("\nA seguinte mensagem foi enviada com sucesso:")
                                    .append(getSeparatorLine())
                                    .append("\n" + response.getDataObject().generateEntityDetailsDescription())
                                    .append(getSeparatorLine());
                            displayMessage(responseMessage.toString(), true);
                        }
                        else
                        {
                            displayMessage("\nNao foi possivel enviar a mensagem. ErrorCode: " + response.getResponseMessage(), true);
                        }
                        uiMenu = getParentMenu();
                        resetMenu();
                    }
                    else
                    {
                        displayMessage("\nO corpo da mensagem nao pode ser vazio.", true);
                        uiMenu = this;
                    }
                    break;
            }
        }
        
        return uiMenu;
    }

    @Override
    public void resetMenu() {
        super.resetMenu();
        alreadyDisplayedHeader = false;
        
    }
    
    
    
}
