/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.admin;

import pt.pd.leilao.client.Utils;
import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.UserActivationOrderEntity;
import pt.pd.leilao.server.model.enums.AdminOrderResponseType;

/**
 *
 * @author dfortunato
 */
public class UIAdminCategoriesEditMenu extends UIAdminBaseMenu {

    private static final int STEP_MENU_SELECT_CATEGORY = 0;
    private static final int STEP_MENU_SET_NEW_NAME = 1;

    // Temp Data
    private int categoryId = -1;

    public UIAdminCategoriesEditMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(getSeparatorLine());
        switch (stepMenu) {
            case STEP_MENU_SELECT_CATEGORY:
                stringBuilder.append("\nIndique o ID da categoria que pretende alterar o nome: ");
                break;
            case STEP_MENU_SET_NEW_NAME:
                stringBuilder.append("\nIndique o novo nome que pretende atribuir: ");
                break;
        }

        return stringBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);

        if (uiMenu == null) {
            switch (stepMenu) {
                case STEP_MENU_SELECT_CATEGORY:
                    // Try to get Order
                    if (Utils.isInteger(input)) {
                        categoryId = Integer.parseInt(input);
                        nextStep();
                    } else {
                        displayMessage("\nID invalido. Tente novamente", true);
                    }
                    uiMenu = this;
                    break;
                case STEP_MENU_SET_NEW_NAME:
                    ResponseService result = LeilaoRemoteServices.getInstance().getAdminUserServiceRemote().changeCategoryName(categoryId, input);
                    
                    if (result.isSuccess()) {
                        displayMessage("\nOperacao exectuada com sucesso.", true);
                    } else {
                        displayMessage("\nOperacao falhou por dados incorrectos ou nome ja atribuido. Codigo: " + result.getResponseMessage(), true);
                    }
                    resetMenu();
                    uiMenu = getParentMenu();
                    break;
            }
        }

        return uiMenu;
    }

    @Override
    public void resetMenu() {
        super.resetMenu();

        categoryId = -1;
    }

}
