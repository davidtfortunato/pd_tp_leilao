/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.admin;

import pt.pd.leilao.client.Utils;
import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.UserActivationOrderEntity;
import pt.pd.leilao.server.model.enums.AdminOrderResponseType;

/**
 *
 * @author dfortunato
 */
public class UIAdminActiveOrderReplyMenu extends UIAdminBaseMenu {

    private static final int STEP_MENU_SET_ORDER_ID = 0;
    private static final int STEP_MENU_SET_RESPONSE_TYPE = 1;

    // Temp Data
    private int orderId = -1;

    public UIAdminActiveOrderReplyMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(getSeparatorLine());
        switch (stepMenu) {
            case STEP_MENU_SET_ORDER_ID:
                stringBuilder.append("\nIndique o ID do pedido que pretende responder: ");
                break;
            case STEP_MENU_SET_RESPONSE_TYPE:
                stringBuilder.append(generateOptionLine("Aprovar pedido", 1));
                stringBuilder.append(generateOptionLine("Rejeitar pedido", 2));
                break;
        }

        return stringBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);

        if (uiMenu == null) {
            switch (stepMenu) {
                case STEP_MENU_SET_ORDER_ID:
                    // Try to get Order
                    if (Utils.isInteger(input)) {
                        orderId = Integer.parseInt(input);
                        nextStep();
                    } else {
                        displayMessage("\nID invalido. Tente novamente", true);
                    }
                    uiMenu = this;
                    break;
                case STEP_MENU_SET_RESPONSE_TYPE:
                    ResponseService result = new ResponseService();
                    switch(input)
                    {
                        case "1":
                            // Approve it
                            result = LeilaoRemoteServices.getInstance().getAdminUserServiceRemote().replyUserActivationOrder(orderId, AdminOrderResponseType.APPROVED);
                            break;
                        case "2":
                            // Decline it
                            result = LeilaoRemoteServices.getInstance().getAdminUserServiceRemote().replyUserActivationOrder(orderId, AdminOrderResponseType.DECLINED);
                            break;
                        default:
                            result.setResponseCode(ResponseService.RESPONSE_CODE_INVALID_DATA);
                            break;
                    }
                    if (result.isSuccess()) {
                        displayMessage("\nOperacao exectuada com sucesso.", true);
                    }
                    else
                    {
                        displayMessage("\nOperacao falhou por dados incorrectos ou pedido ja inexistente. Codigo: " + result.getResponseMessage(), true);
                    }
                    resetMenu();
                    uiMenu = getParentMenu();
                    break;
            }
        }

        return uiMenu;
    }

    @Override
    public void resetMenu() {
        super.resetMenu();

        orderId = -1;
    }

}
