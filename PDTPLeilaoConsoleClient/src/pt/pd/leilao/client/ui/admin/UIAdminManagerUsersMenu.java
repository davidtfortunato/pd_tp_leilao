/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.admin;

import java.util.ArrayList;
import java.util.List;
import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.server.model.IEntityToString;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.UserEntity;

/**
 *
 * @author dfortunato
 */
public class UIAdminManagerUsersMenu extends UIAdminBaseMenu{

    public UIAdminManagerUsersMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder menuBuilder = new StringBuilder();
        
        menuBuilder.append(getHeaderMessage("Gestao de Utilizadores"))
                .append(generateOptionLine("Listar Utilizadores", 1))
                .append(generateOptionLine("Suspender Utilizador", 2))
                .append(generateOptionLine("Reactivar Utilizador", 3))
                .append(generateOptionLine("Detalhes de user", 4))
                .append(generateOptionLine("Pagina anterior", 5))
                .append(getSeparatorLine())
                .append(getMessageSelectOption());
        
        return menuBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);
        
        if (uiMenu == null) {
            switch(input)
            {
                case "1":
                    displayMessage(generateListEntities(new ArrayList<IEntityToString>(LeilaoRemoteServices.getInstance().getAdminUserServiceRemote().getListUsers().getDataObject()), "\nNao existem utilizadores"), true);
                    uiMenu = this;
                    break;
                case "2":
                    uiMenu = new UIAdminUserSuspendMenu(this);
                    break;
                case "3":
                    uiMenu = new UIAdminUserReactivateMenu(this);
                    break;
                case "4":
                    uiMenu = new UIAdminUserDetailsMenu(this);
                    break;
                case "5":
                    uiMenu = getParentMenu();  
                    break;
            }
            
        }
        
        return uiMenu;
    }
    
}
