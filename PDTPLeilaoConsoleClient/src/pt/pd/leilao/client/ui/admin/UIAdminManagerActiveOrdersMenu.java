/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.admin;

import java.util.ArrayList;
import java.util.List;
import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.server.model.IEntityToString;
import pt.pd.leilao.server.model.UserActivationOrderEntity;
import pt.pd.leilao.server.model.enums.AdminOrderResponseType;

/**
 *
 * @author dfortunato
 */
public class UIAdminManagerActiveOrdersMenu extends UIAdminBaseMenu{

    public UIAdminManagerActiveOrdersMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder menuBuilder = new StringBuilder();
        
        menuBuilder.append(getHeaderMessage("Gerir pedidos de activacao ou suspensao de users"))
                .append(generateOptionLine("Listar pedidos", 1))
                .append(generateOptionLine("Responder a pedido", 2))
                .append(generateOptionLine("Pagina Anterior", 3))
                .append(getSeparatorLine())
                .append(getMessageSelectOption());
        
        return menuBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uIMenu = super.optionSelect(stepMenu, input);
        
        if (uIMenu == null) {
            switch(input)
            {
                case "1":
                    displayMessage(generateListOrders(), true);
                    uIMenu = this;
                    break;
                case "2":
                    uIMenu = new UIAdminActiveOrderReplyMenu(this);
                    break;
                case "3":
                    uIMenu = getParentMenu();
                    break;
            }
        }
        
        return uIMenu;
    }
    
    private String generateListOrders()
    {
        StringBuilder stringBuilder = new StringBuilder();
        
        List<UserActivationOrderEntity> listOrders = LeilaoRemoteServices.getInstance().getAdminUserServiceRemote().getListUserActivationOrderEntity().getDataObject();
        List<IEntityToString> listEntities = new ArrayList<>();
        
        if (listOrders != null && !listOrders.isEmpty()) {
            for(UserActivationOrderEntity order : listOrders)
            {
                if (order != null && order.getAdminOrderResponseType() == AdminOrderResponseType.PENDING) {
                    listEntities.add(order);
                }
            }
        }
        return generateListEntities(listEntities, "Nao existem pedidos pendentes");
    }
    
}
