/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.admin;

import java.util.ArrayList;
import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.server.model.IEntityToString;

/**
 *
 * @author dfortunato
 */
public class UIAdminManagerCategoriesMenu extends UIAdminBaseMenu{

    public UIAdminManagerCategoriesMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder menuBuilder = new StringBuilder();
        
        menuBuilder.append(getHeaderMessage("Gestao de Categorias"))
                .append(generateOptionLine("Listar Categorias", 1))
                .append(generateOptionLine("Adicionar Categoria", 2))
                .append(generateOptionLine("Alterar nome categoria", 3))
                .append(generateOptionLine("Pagina anterior", 4))
                .append(getSeparatorLine())
                .append(getMessageSelectOption());
        
        return menuBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);
        
        if (uiMenu == null) {
            switch(input)
            {
                case "1":
                    displayMessage(generateListEntities(new ArrayList<IEntityToString>(LeilaoRemoteServices.getInstance().getAdminUserServiceRemote().getListCategories().getDataObject()), "\nNao existem categorias disponiveis"), true);
                    uiMenu = this;
                    break;
                case "2":
                    uiMenu = new UIAdminCategoriesAddMenu(this);
                    break;
                case "3":
                    uiMenu = new UIAdminCategoriesEditMenu(this);
                    break;
                case "4":
                    uiMenu = getParentMenu();  
                    break;
            }
            
        }
        
        return uiMenu;
    }
    
    
}
