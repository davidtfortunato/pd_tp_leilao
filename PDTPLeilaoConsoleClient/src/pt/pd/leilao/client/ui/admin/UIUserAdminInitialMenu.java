/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.admin;

import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.client.ui.UIInitialMenu;
import pt.pd.leilao.client.ui.UIInvalidOptionMenu;
import pt.pd.leilao.client.ui.UINewsletterViewMenu;
import pt.pd.leilao.client.ui.user.UIUserEditPasswordMenu;
import pt.pd.leilao.client.ui.user.UIUserManagerMessagesMenu;
import pt.pd.leilao.server.model.enums.UserType;

/**
 *
 * @author dfortunato
 */
public class UIUserAdminInitialMenu extends UIAdminBaseMenu{

    public UIUserAdminInitialMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder menuBuilder = new StringBuilder();
        
        // Generate Admin menu
        menuBuilder.append(getHeaderMessage("Bem Vindo " + LeilaoRemoteServices.getInstance().getCurrentUser().getNome()))
                .append(generateOptionLine("Gerir pedidos de activacao/suspensao", 1))
                .append(generateOptionLine("Gerir items", 2))
                .append(generateOptionLine("Gerir utilizadores", 3))
                .append(generateOptionLine("Gerir categorias", 4))
                .append(generateOptionLine("Gerir mensagens", 5))
                .append(generateOptionLine("Ver Newsletter", 6))
                .append(generateOptionLine("Alterar password", 7))
                .append(generateOptionLine("Logout", 8))
                .append(getSeparatorLine())
                .append(getMessageSelectOption());
        
        return menuBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);
        
        if (uiMenu == null) {
            // Check if still logged in as admin
            switch(input)
            {
                case "1":
                    uiMenu = new UIAdminManagerActiveOrdersMenu(this);
                    break;
                case "2":
                    uiMenu = new UIAdminManagerItemsMenu(this);
                    break;
                case "3":
                    uiMenu = new UIAdminManagerUsersMenu(this);
                    break;
                case "4":
                    uiMenu = new UIAdminManagerCategoriesMenu(this);
                    break;
                case "5":
                    uiMenu = new UIUserManagerMessagesMenu(this);
                    break;
                case "6":
                    uiMenu = new UINewsletterViewMenu(this);
                    break;
                case "7":
                    uiMenu = new UIUserEditPasswordMenu(this);
                    break;
                case "8":
                    LeilaoRemoteServices.getInstance().getClientUserServiceRemote().logout();
                    uiMenu = getParentMenu();
                    break;
                default:
                    uiMenu = new UIInvalidOptionMenu(this);
            }
        }
        
        return uiMenu;
    }
    
}
