/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.admin;

import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.UserEntity;

/**
 *
 * @author dfortunato
 */
public class UIAdminUserDetailsMenu extends UIAdminBaseMenu{

    public UIAdminUserDetailsMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder stringBuilder = new StringBuilder();
        
        stringBuilder.append(getHeaderMessage("Detalhes de utilizador"))
                .append("\nIndique o Username do utilizador que pretende visualizar: ");
        
        return stringBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);
        
        if (uiMenu == null) {
            ResponseService<UserEntity> response = LeilaoRemoteServices.getInstance().getAdminUserServiceRemote().findUser(input);
            if(response.isSuccess())
            {
                displayMessage(getSeparatorLine() + "\n" + response.getDataObject().generateEntityDetailsDescription() + getSeparatorLine(), true);
            }
            else
            {
                displayMessage("\nO utilizador " + input + " nao foi encontrado.  CodigoErro: " + response.getResponseMessage(), true);
            }
            uiMenu = getParentMenu();
        }
        
        return uiMenu;
    }
    
}
