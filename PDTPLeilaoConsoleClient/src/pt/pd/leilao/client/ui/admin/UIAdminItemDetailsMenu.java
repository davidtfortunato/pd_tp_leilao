/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.admin;

import pt.pd.leilao.client.Utils;
import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.client.ui.UIInitialMenu;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.UserEntity;

/**
 *
 * @author dfortunato
 */
public class UIAdminItemDetailsMenu extends UIAdminBaseMenu{

    public UIAdminItemDetailsMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder stringBuilder = new StringBuilder();
        
        stringBuilder.append(getHeaderMessage("Detalhes de Item"))
                .append("\nIndique o ID do item que pretende visualizar: ");
        
        return stringBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);
        
        if (uiMenu == null) {
            if (Utils.isInteger(input)) {
                ResponseService<ItemEntity> response = LeilaoRemoteServices.getInstance().getAdminUserServiceRemote().findItemById(Integer.parseInt(input));
                if(response.isSuccess())
                {
                    displayMessage(getSeparatorLine() + "\n" + response.getDataObject().generateEntityDetailsDescription() + getSeparatorLine(), true);
                }
                else
                {
                    displayMessage("\nO item #" + input + " nao foi encontrado.  CodigoErro: " + response.getResponseMessage(), true);
                }
                uiMenu = getParentMenu();
            }
            else
            {
                uiMenu = getParentMenu();
            }
            
        }
        
        return uiMenu;
    }
    
}
