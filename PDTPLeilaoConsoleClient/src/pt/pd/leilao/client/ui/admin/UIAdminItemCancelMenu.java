/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.admin;

import pt.pd.leilao.client.Utils;
import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.client.ui.UIInvalidOptionMenu;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.enums.ItemStateType;

/**
 *
 * @author dfortunato
 */
public class UIAdminItemCancelMenu extends UIAdminBaseMenu{

    public UIAdminItemCancelMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder stringBuilder = new StringBuilder();
        
        stringBuilder.append(getHeaderMessage("Cancelar item"))
                .append("\nIndique o ID do item que pretende cancelar: ");
        
        return stringBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);
        
        if (uiMenu == null) 
        {
            if (Utils.isInteger(input)) {
            
                ResponseService response = LeilaoRemoteServices.getInstance().getAdminUserServiceRemote().updateItemState(Integer.parseInt(input), ItemStateType.CANCELLED);
                if(response.isSuccess())
                {
                    displayMessage("\nItem com o #" + input + " foi cancelado com sucesso", true);
                }
                else
                {
                    displayMessage("\nNao foi possivel cancelar o item #" + input + " - CodigoErro: " + response.getResponseMessage(), true);
                }
                uiMenu = getParentMenu();
            }
            else
            {
                uiMenu = new UIInvalidOptionMenu(getParentMenu());
            }
        }
        
        return uiMenu;
    }
    
    
    
}
