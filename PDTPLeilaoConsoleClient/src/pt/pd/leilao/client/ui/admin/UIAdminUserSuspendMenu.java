/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.admin;

import javax.rmi.CORBA.Util;
import pt.pd.leilao.client.Utils;
import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.server.model.ResponseService;

/**
 *
 * @author dfortunato
 */
public class UIAdminUserSuspendMenu extends UIAdminBaseMenu {

    // Input Data
    private String username;

    public UIAdminUserSuspendMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder stringBuilder = new StringBuilder();

        switch (stepMenu) {
            case 0:
                stringBuilder.append(getHeaderMessage("Suspender utilizador"))
                        .append("\nIndique o Username do utilizador que pretende suspender: ");
                break;
            case 1:
                stringBuilder.append("\nIndique o motivo da suspensao: ");
                break;
        }

        return stringBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);

        if (uiMenu == null) {
            
            switch(stepMenu)
            {
                case 0:
                    if (!pt.pd.leilao.server.services.utils.Utils.isStringOnlySpacesOrEmpty(input)) {
                        username = input;
                        nextStep();
                    }
                    else
                    {
                        displayMessage("\nO Username inserido nao é valido. Insira por favor um username valido.", true);
                    }
                    uiMenu = this;
                    break;
                case 1:
                    ResponseService response = LeilaoRemoteServices.getInstance().getAdminUserServiceRemote().suspendUser(username, input);
                    if (response.isSuccess()) {
                        displayMessage("\nUtilizador " + username + " foi suspenso com sucesso", true);
                    } else {
                        displayMessage("\nNao foi possivel suspender o utilizador " + input + "  CodigoErro: " + response.getResponseMessage(), true);
                    }
                    resetMenu();
                    uiMenu = getParentMenu();
                    break;
            }
            
        }

        return uiMenu;
    }

}
