/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.admin;

import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.client.ui.UIInitialMenu;
import pt.pd.leilao.server.model.enums.UserType;

/**
 *
 * @author dfortunato
 */
public abstract class UIAdminBaseMenu extends AbsUIMenu{
    
    public UIAdminBaseMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input)
    {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);
        
        if (uiMenu == null) {
            if (LeilaoRemoteServices.getInstance().getCurrentUser().getUserType() != UserType.ADMIN) {
                displayMessage("A sua sessao expirou. Sera redireccionado para o menu inicial", true);

                // It is not an admin anymore. Should return to the Initial menu
                uiMenu = new UIInitialMenu();
            }
        }
        
        return uiMenu;
    }
}
