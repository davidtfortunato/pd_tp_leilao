/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.admin;

import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.server.model.ResponseService;

/**
 *
 * @author dfortunato
 */
public class UIAdminUserReactivateMenu extends UIAdminBaseMenu{

    public UIAdminUserReactivateMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder stringBuilder = new StringBuilder();
        
        stringBuilder.append(getHeaderMessage("Suspender utilizador"))
                .append("\nIndique o Username do utilizador que pretende reactivar: ");
        
        return stringBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);
        
        if (uiMenu == null) {
            ResponseService response = LeilaoRemoteServices.getInstance().getAdminUserServiceRemote().reactivateUser(input);
            if(response.isSuccess())
            {
                displayMessage("\nUtilizador " + input + " foi reactivado com sucesso", true);
            }
            else
            {
                displayMessage("\nNao foi possivel reactivar o utilizador " + input + "  CodigoErro: " + response.getResponseMessage(), true);
            }
            uiMenu = getParentMenu();
        }
        
        return uiMenu;
    }
    
    
    
}
