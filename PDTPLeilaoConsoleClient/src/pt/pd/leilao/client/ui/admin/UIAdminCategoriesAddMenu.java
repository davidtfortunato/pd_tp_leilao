/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.admin;

import pt.pd.leilao.client.Utils;
import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.client.ui.UIInvalidOptionMenu;
import pt.pd.leilao.server.model.ResponseService;
import pt.pd.leilao.server.model.enums.ItemStateType;

/**
 *
 * @author dfortunato
 */
public class UIAdminCategoriesAddMenu extends UIAdminBaseMenu{

    public UIAdminCategoriesAddMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder stringBuilder = new StringBuilder();
        
        stringBuilder.append(getHeaderMessage("Adicionar Categoria"))
                .append("\nIndique o nome que pretende atribuir à categoria: ");
        
        return stringBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);
        
        if (uiMenu == null) 
        {
            ResponseService response = LeilaoRemoteServices.getInstance().getAdminUserServiceRemote().addCategory(input);
            if(response.isSuccess())
            {
                displayMessage("\nA categoria " + input + " foi adicionada com sucesso", true);
            }
            else
            {
                displayMessage("\nNao foi possivel adicionar a categoria " + input + " - CodigoErro: " + response.getResponseMessage(), true);
            }
            uiMenu = getParentMenu();
        }
        
        return uiMenu;
    }
    
    
    
}
