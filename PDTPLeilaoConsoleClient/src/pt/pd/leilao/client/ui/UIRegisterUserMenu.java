/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui;

import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.model.enums.responses.ResponseUserRegisterType;

/**
 *
 * @author dfortunato
 */
public class UIRegisterUserMenu extends AbsUIMenu{

    // User Data
    private UserEntity userData;
    
    public UIRegisterUserMenu(AbsUIMenu parentMenu) {
        super(parentMenu, true);
        userData = new UserEntity();
    }

    @Override
    protected UIMenuInputType getStepMenuInputType(int stepMenu) {
        switch(stepMenu)
        {
            case 1:
                return UIMenuInputType.PASSWORD_INPUT;
            default:
                return super.getStepMenuInputType(stepMenu);
        }
    }
    
    @Override
    protected String generateMenuUI(int stepMenu) {
        
        StringBuilder stringBuilder = new StringBuilder();
        switch(stepMenu)
        {
            case 0:
                // First Menu - Insert UserName
                stringBuilder
                        .append(getHeaderMessage("Registo de novo utilizador"))
                        .append("\nInsira o username que pretende: ");
                break;
            case 1:
                // Insert Password
                stringBuilder
                        .append("\nInsira a Password que pretende: ");
                break;
            default:
                // Insert Password
                stringBuilder
                        .append("\nMenu Step invalido")
                        .append(getSeparatorLine());
                
        }
        return stringBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);
        
        if (uiMenu == null) {
            switch(stepMenu)
            {
                case 0:
                    // Set Username
                    if (input != null && !input.isEmpty()) {
                        userData.setUserName(input);
                        nextStep();
                        uiMenu = this;
                    } 
                    else
                    {
                        displayMessage("\nO nome de utilizador nao pode ser vazio. Insira de novo.", true);
                        uiMenu = this;
                    }
                    break;
                case 1:
                    // Set Password
                    if (input != null && !input.isEmpty()) {
                        userData.setPassword(input);

                        // Try to register User
                        ResponseUserRegisterType response = LeilaoRemoteServices.getInstance().getClientUserServiceRemote().registerUser(userData);
                        switch (response)
                        {
                            case USERDATA_NOT_VALID:
                                displayMessage("\nDados de utilizador invalidos", true);
                                break;
                            case USERNAME_ALREADY_EXISTS:
                                displayMessage("\nNome de utilizador ja existe. Tente um nome diferente", true);
                                break;
                            case USER_REGISTER_PENDING_APPROVAL:
                                displayMessage("\nUtilizador registado com sucesso.\nEstá pendente de aprovacao por parte do admin.", true);
                                break;
                            case USER_REGISTERED:
                                displayMessage("\nUtilizador registado com sucesso.\nPode efectuar o login.", true);
                                break;
                        }
                        // Go to parent
                        uiMenu = getParentMenu();
                    } 
                    else
                    {
                        displayMessage("\nThe Password cannot be empty. Please insert the Password again", true);
                        uiMenu = this;
                    }
                    break;
            }
        }
        
        return uiMenu;
    }
    
}
