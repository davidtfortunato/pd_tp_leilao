/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui;

import pt.pd.leilao.client.ui.guest.UIUserGuestInitialMenu;
import pt.pd.leilao.client.ui.user.UIUserRegisteredInitialMenu;
import pt.pd.leilao.client.ui.admin.UIUserAdminInitialMenu;
import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.server.model.responses.ResponseUserLoginData;

/**
 *
 * @author dfortunato
 */
public class UILoginMenu extends AbsUIMenu {

    private static final int STEP_INPUT_INSERT_USERNAME = 0;
    private static final int STEP_INPUT_INSERT_PASSWORD = 1;

    // Input Data
    private String username;
    private String password;

    public UILoginMenu(AbsUIMenu parentMenu) {
        super(parentMenu, true);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder stringBuilder = new StringBuilder();
        switch (stepMenu) {
            case STEP_INPUT_INSERT_USERNAME:
                stringBuilder.append(getSeparatorLine())
                        .append("\nNome Utilizador: ");
                break;
            case STEP_INPUT_INSERT_PASSWORD:
                stringBuilder.append("\nPassword: ");
                break;
        }
        return stringBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);
        
        if (uiMenu == null) {
            uiMenu = getParentMenu();
            switch (stepMenu) {
                case STEP_INPUT_INSERT_USERNAME:
                    username = input;
                    if (username != null && !username.isEmpty()) {
                        nextStep();
                        uiMenu = this;
                    } else {
                        displayMessage("\nO nome de utilizador nao pode ser vazio. Insira de novo.", true);
                        uiMenu = this;
                    }
                    break;
                case STEP_INPUT_INSERT_PASSWORD:
                    password = input;
                    // Try to Login
                    uiMenu = proceedLogin(username, password);
                    break;
            }
        }
        return uiMenu;
    }

    private AbsUIMenu proceedLogin(String username, String password) {
        ResponseUserLoginData responseUserLoginData = LeilaoRemoteServices.getInstance().getClientUserServiceRemote().loginUser(username, password);
        switch(responseUserLoginData.getResponseUserLoginType())
        {
            case LOGIN_SUCCESS:
                // TODO Switch if is REGISTERED, GUEST or Admin
                switch(responseUserLoginData.getUserData().getUserType())
                {
                    case ADMIN:
                        return new UIUserAdminInitialMenu(getParentMenu());
                    case REGISTERED:
                        return new UIUserRegisteredInitialMenu(getParentMenu());
                    case GUEST:
                    case UNKNOWN:
                        return new UIUserGuestInitialMenu(getParentMenu());
                }
                break;
            case FAILED_USERNAME_NOT_FOUND:
                displayMessage("\nO Username inserido nao existe.", true);
                return getParentMenu();
            case FAILED_NOT_ACTIVE:
                displayMessage("\nO Utilizador ainda nao foi aprovado pelo Administrador", true);
                return getParentMenu();
            case FAILED_PASSWORD_WRONG:
                displayMessage("\nA password esta incorrecta", true);
                return getParentMenu();
        }
        return getParentMenu();
    }

    @Override
    protected UIMenuInputType getStepMenuInputType(int stepMenu) {
        switch (stepMenu) {
            case STEP_INPUT_INSERT_PASSWORD:
                return UIMenuInputType.PASSWORD_INPUT;
            default:
                return super.getStepMenuInputType(stepMenu);
        }
    }

}
