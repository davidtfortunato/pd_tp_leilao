/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui.guest;

import java.util.List;
import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.AbsUIMenu;
import pt.pd.leilao.client.ui.UIInvalidOptionMenu;
import pt.pd.leilao.client.ui.UINewsletterViewMenu;
import pt.pd.leilao.client.ui.UIRequestReactivationUserMenu;
import pt.pd.leilao.server.model.IEntityToString;
import pt.pd.leilao.server.model.ItemEntity;
import pt.pd.leilao.server.model.ResponseService;

/**
 *
 * @author dfortunato
 */
public class UIUserGuestInitialMenu extends AbsUIMenu{

    public UIUserGuestInitialMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder menuBuilder = new StringBuilder();
        
        menuBuilder.append(getHeaderMessage("Bem vindo visitante"))
                .append(generateOptionLine("Pedir reactivacao de conta", 1))
                .append(generateOptionLine("Listar ultimos artigos vendidos", 2))
                .append(generateOptionLine("Visualizar Newsletter", 3))
                .append(generateOptionLine("Voltar para a pagina inicial", 4))
                .append(getSeparatorLine())
                .append(getMessageSelectOption());
        
        return menuBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, input);
        
        if (uiMenu == null) {
            switch(input)
            {
                case "1":
                    uiMenu = new UIRequestReactivationUserMenu(this);
                    break;
                case "2":
                    displayMessage(generateListItems(LeilaoRemoteServices.getInstance().getClientGuestServiceRemote().getLastSoldItems(3)), true);
                    uiMenu = this;
                    break;
                case "3":
                    uiMenu = new UINewsletterViewMenu(this);
                    break;
                case "4":
                    uiMenu = getParentMenu();
                    break;
                default:
                    uiMenu = new UIInvalidOptionMenu(this);
            }
        }
        
        return uiMenu;
    }
    
    private String generateListItems(ResponseService<List<ItemEntity>> responseService)
    {
        StringBuilder stringBuilder = new StringBuilder();
        
        List<ItemEntity> listItems = responseService.getDataObject();

        stringBuilder.append(getSeparatorLine());
        
        if (listItems == null || listItems.isEmpty()) {
            // The List is empty
            stringBuilder.append("\nAinda nao existem items vendidos");
        }
        else
        {
            for(ItemEntity entity : listItems)
            {
                stringBuilder.append("\n" + entity.generateListLineDescriptionGuest());
            }
        }
        stringBuilder.append(getSeparatorLine());
        
        return stringBuilder.toString();
    }
    
}
