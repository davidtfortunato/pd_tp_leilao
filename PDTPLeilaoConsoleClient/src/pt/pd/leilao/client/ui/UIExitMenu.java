/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui;

/**
 *
 * @author dfortunato
 */
public class UIExitMenu extends AbsUIMenu{

    public UIExitMenu(AbsUIMenu parentMenu) {
        super(null, false);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        return getSeparatorLine() + "\nAdeus e obrigado por utilizar o nosso servico de leiloes." + getSeparatorLine();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String option) {
        return null;
    }
    
}
