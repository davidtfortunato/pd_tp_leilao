/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui;

import java.util.ArrayList;
import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.server.model.IEntityToString;

/**
 *
 * @author dfortunato
 */
public class UINewsletterViewMenu extends AbsUIMenu{

    public UINewsletterViewMenu(AbsUIMenu parentMenu) {
        super(parentMenu);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        StringBuilder stringBuilder = new StringBuilder();
        
        stringBuilder.append(getHeaderMessage("Newsletter"))
                .append(generateListEntities(new ArrayList<IEntityToString>(LeilaoRemoteServices.getInstance().getNewsletterServiceRemote().getListNewsletters()), "Ainda nao existem mensagens na newsletter"));
        
        return stringBuilder.toString();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String input) {
        return getParentMenu();
    }
    
}
