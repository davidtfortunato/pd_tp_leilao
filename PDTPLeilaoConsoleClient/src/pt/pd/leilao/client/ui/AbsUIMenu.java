/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui;

import java.io.Console;
import java.util.List;
import java.util.Scanner;
import java.util.logging.ConsoleHandler;
import pt.pd.leilao.client.Utils;
import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.server.model.IEntityToString;
import pt.pd.leilao.server.model.UserActivationOrderEntity;
import pt.pd.leilao.server.model.enums.AdminOrderResponseType;

/**
 *
 * @author dfortunato
 */
public abstract class AbsUIMenu {
    
    protected static final String COMMAND_INPUT_BACK_PAGE = "/back";
    protected static final String COMMAND_INPUT_EXIT_APP = "/exit";
    protected static final String COMMAND_INPUT_HELP = "/help";
    protected static final String COMMAND_INPUT_NEWSLETTER = "/newsletter";
    protected static final String COMMAND_INPUT_LOGOUT = "/logout";
    
    // Parent menu
    private AbsUIMenu parentMenu;
    private boolean shouldScanOption;
    
    // Data
    private int currentStep;
    
    public AbsUIMenu(AbsUIMenu parentMenu)
    {
        this.parentMenu = parentMenu;
        this.shouldScanOption = true;
        this.currentStep = 0;
    }
    
    public AbsUIMenu(AbsUIMenu parentMenu, boolean shouldScanOption)
    {
        this.parentMenu = parentMenu;
        this.shouldScanOption = shouldScanOption;
        this.currentStep = 0;
    }
    
    public void displayMenu()
    {
        System.out.println("\n" + generateMenuUI(currentStep));
        
        // Get Option
        if (shouldScanOption) {
            String inputValue = null;
            Scanner scanner = new Scanner(System.in);
            
            switch(getStepMenuInputType(currentStep))
            {
                case TEXT_INPUT:
                    if (System.console() != null) {
                        inputValue = System.console().readLine();
                    }
                    else
                    {
                        inputValue = scanner.nextLine();
                    }
                    break;
                case PASSWORD_INPUT:
                    if (System.console() != null) {
                        char[] passArr = System.console().readPassword();
                        inputValue = new String(passArr);
                    }
                    else
                    {
                        inputValue = scanner.nextLine();
                    }
                    break;
            }
            
            // validate if is valid
            AbsUIMenu uiNextMenu = optionSelect(currentStep, inputValue);
            if (uiNextMenu != null) {
                uiNextMenu.displayMenu();

                // Check if should repeat the menu
                if (uiNextMenu instanceof UIInvalidOptionMenu) {
                    displayMenu();
                }
            } else if (parentMenu != null) {
                parentMenu.resetMenu();
                parentMenu.displayMenu();
            }
            
        }
        
    }
    
    protected abstract String generateMenuUI(int stepMenu);
    
    protected AbsUIMenu optionSelect(int stepMenu, String input)
    {
        if (input != null) {
            if (input.equals(COMMAND_INPUT_BACK_PAGE)) {
                return getParentMenu();
            } else if (input.equals(COMMAND_INPUT_EXIT_APP)) {
                return new UIExitMenu(null);
            } else if (input.equals(COMMAND_INPUT_HELP)) {
                return new UIHelpCommandsMenu(this);
            } else if (input.equals(COMMAND_INPUT_NEWSLETTER)) {
                return new UINewsletterViewMenu(this);
            } else if (input.equals(COMMAND_INPUT_LOGOUT)) {
                LeilaoRemoteServices.getInstance().getClientUserServiceRemote().logout();
                return new UIInitialMenu();
            }
        }
        return null;
    }
 
    protected UIMenuInputType getStepMenuInputType(int stepMenu)
    {
        return UIMenuInputType.TEXT_INPUT;
    }
    
    protected void nextStep()
    {
        currentStep ++;
    }
    
    public void resetMenu()
    {
        currentStep = 0;
    }
    
    protected String getSeparatorLine()
    {
        return "\n****************************************************************";
    }
    
    protected String getHeaderMessage(String headerMessage)
    {
        StringBuilder headerBuilder = new StringBuilder();
        headerBuilder.append(getSeparatorLine());
        headerBuilder.append("\n");
        // Sides Spaces
        int separatorLineSize = getSeparatorLine().length();
        int sidesSize = separatorLineSize - headerMessage.length();
        for(int i = 0; i < (sidesSize/2); i++)
        {
            headerBuilder.append(" ");
        }
        headerBuilder.append(headerMessage);
        for(int i = 0; i < (sidesSize/2); i++)
        {
            headerBuilder.append(" ");
        }
        headerBuilder.append(getSeparatorLine());
        
        return headerBuilder.toString();
    }
    
    protected String generateOptionLine(String optionDescription, int optionNumber)
    {
        return "\nOpcao " + optionNumber + " : " + optionDescription;
    }
    
    protected String getMessageSelectOption()
    {
        return "\nEscolha uma opcao: ";
    }
    
    
    protected void displayMessage(String lineToDisplay, boolean waitEnter)
    {
        System.out.println(lineToDisplay);
        if (waitEnter) {
            Scanner scanner = new Scanner(System.in);
            scanner.nextLine();
        }
    }

    public AbsUIMenu getParentMenu() {
        return parentMenu;
    }

    public boolean isShouldScanOption() {
        return shouldScanOption;
    }

    public int getCurrentStep() {
        return currentStep;
    }
    
    protected String generateListEntities(List<IEntityToString> listEntities, String emptyMessage)
    {
        StringBuilder stringBuilder = new StringBuilder();
        

        stringBuilder.append(getSeparatorLine());
        
        if (listEntities == null || listEntities.isEmpty()) {
            // The List is empty
            stringBuilder.append("\n" + emptyMessage);
        }
        else
        {
            for(IEntityToString entity : listEntities)
            {
                stringBuilder.append("\n" + entity.generateListLineDescription());
            }
        }
        stringBuilder.append(getSeparatorLine());
        
        return stringBuilder.toString();
    }
    
    public enum UIMenuInputType
    {
        TEXT_INPUT, PASSWORD_INPUT;
    }
}
