/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui;

/**
 *
 * @author dfortunato
 */
public class UIInvalidOptionMenu extends AbsUIMenu{

    public UIInvalidOptionMenu(AbsUIMenu parentMenu) {
        super(parentMenu, true);
    }

    @Override
    protected String generateMenuUI(int stepMenu) {
        return getSeparatorLine() + "\nOpcao invalida.\nEnter para prosseguir." + getSeparatorLine();
    }

    @Override
    protected AbsUIMenu optionSelect(int stepMenu, String option) {
        return getParentMenu();
    }
    
}
