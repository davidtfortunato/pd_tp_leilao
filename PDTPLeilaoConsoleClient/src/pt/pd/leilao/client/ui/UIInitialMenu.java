/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.ui;

import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.guest.UIUserGuestInitialMenu;

/**
 *
 * @author dfortunato
 */
public class UIInitialMenu extends AbsUIMenu{

    public UIInitialMenu() {
        super(null, true);
    }

    @Override
    public String generateMenuUI(int stepMenu) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getHeaderMessage("Bem Vindo. Esta no menu inicial"))
                .append(generateOptionLine("Registar novo utilizador.", 1))
                .append(generateOptionLine("Login", 2))
                .append(generateOptionLine("Entrar como Guest", 3))
                .append(generateOptionLine("Lista de comandos extra", 4))
                .append(generateOptionLine("Sair", 5))
                .append(getSeparatorLine())
                .append(getMessageSelectOption());
        
        return stringBuilder.toString();
    }

    @Override
    public AbsUIMenu optionSelect(int stepMenu, String option) {
        AbsUIMenu uiMenu = super.optionSelect(stepMenu, option);
        
        if (uiMenu == null) {
            switch (option)
            {
                case "1":
                    uiMenu = new UIRegisterUserMenu(this);
                    break;
                case "2":
                    uiMenu = new UILoginMenu(this);
                    break;
                case "3":
                    uiMenu = new UIUserGuestInitialMenu(this);
                    break;
                case "4":
                    uiMenu = new UIHelpCommandsMenu(this);
                    break;
                case "5":
                    uiMenu = new UIExitMenu(null);
                    break;
                default:
                    uiMenu = new UIInvalidOptionMenu(this);
            }
        }
        return uiMenu;
    }
    
}
