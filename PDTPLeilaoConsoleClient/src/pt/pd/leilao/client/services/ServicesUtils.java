/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.services;

import java.util.Properties;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author dfortunato
 */
public class ServicesUtils {

    // Server Connection Config
    public static final String SERVER_IP = "192.168.56.175";
    public static final String SERVER_PORT = "3700";

    // Server Project Configs
    public static final String SERVER_PROJECT_NAME = "PDTPLeilaoWebApp";

    // Initial context
    private static InitialContext staticInitialContext;

    /**
     * Generate the lookup name to get the Remote Class
     *
     * @param classEJB Real class identification
     * @param remoteClass Remote class
     * @return Generated string
     */
    public static String generateClassLookupName(String classEJBName, String remoteClassName) {
        return "java:global/"
                + SERVER_PROJECT_NAME
                + "/"
                + classEJBName
                + "!"
                + remoteClassName;
    }

    /**
     * Generate the initial context to then lookup the remote services
     *
     * @return
     */
    public static InitialContext getInitialContext() {
        if (staticInitialContext == null) {
            Properties prop = new Properties();

            prop.setProperty("java.naming.factory.initial",
                    "com.sun.enterprise.naming.SerialInitContextFactory");
            prop.setProperty("java.naming.factory.url.pkgs",
                    "com.sun.enterprise.naming");
            prop.setProperty("java.naming.factory.state",
                    "com.sun.corba.ee.impl.presentation.rmi.JNDIStateFactoryImpl");
            prop.setProperty("org.omg.CORBA.ORBInitialHost", ServicesUtils.SERVER_IP);
            prop.setProperty("org.omg.CORBA.ORBInitialPort", ServicesUtils.SERVER_PORT);

            try {
                staticInitialContext = new InitialContext(prop);
            } catch (NamingException e) {
                System.out.println(e.getMessage());
                System.exit(1);
            }
            System.out.println("InitialContext Criado");
        }

        return staticInitialContext;
    }

}
