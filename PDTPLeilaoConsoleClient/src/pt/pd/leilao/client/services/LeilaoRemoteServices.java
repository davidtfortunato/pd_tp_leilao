/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client.services;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import pt.pd.leilao.server.model.UserEntity;
import pt.pd.leilao.server.services.ClientUserServiceRemote;
import pt.pd.leilao.server.services.ClientAdminUserServiceRemote;
import pt.pd.leilao.server.services.ClientGuestServiceRemote;
import pt.pd.leilao.server.services.ClientItemsServiceRemote;
import pt.pd.leilao.server.services.ClientNewsletterServiceRemote;

/**
 *
 * @author dfortunato
 */
public class LeilaoRemoteServices {

    private static final String TAG = LeilaoRemoteServices.class.getSimpleName();

    // Initial Context and Lookup Remote Classes
    {

        try {
            // Lookup
            clientUserServiceRemote = (ClientUserServiceRemote) ServicesUtils.getInitialContext().lookup(ServicesUtils.generateClassLookupName("ClientUserService", ClientUserServiceRemote.class.getName()));
            adminUserServiceRemote = (ClientAdminUserServiceRemote) ServicesUtils.getInitialContext().lookup(ServicesUtils.generateClassLookupName("ClientAdminUserService", ClientAdminUserServiceRemote.class.getName()));
            newsletterServiceRemote = (ClientNewsletterServiceRemote) ServicesUtils.getInitialContext().lookup(ServicesUtils.generateClassLookupName("ClientNewsletterService", ClientNewsletterServiceRemote.class.getName()));
            clientItemsServiceRemote = (ClientItemsServiceRemote) ServicesUtils.getInitialContext().lookup(ServicesUtils.generateClassLookupName("ClientItemsService", ClientItemsServiceRemote.class.getName()));
            clientGuestServiceRemote = (ClientGuestServiceRemote) ServicesUtils.getInitialContext().lookup(ServicesUtils.generateClassLookupName("ClientGuestService", ClientGuestServiceRemote.class.getName()));
            
        } catch (NamingException ex) {
            Logger.getLogger(TAG).log(Level.SEVERE, null, ex);
        }

    }
    
    // Services
    private ClientUserServiceRemote clientUserServiceRemote;
    private ClientAdminUserServiceRemote adminUserServiceRemote;
    private ClientNewsletterServiceRemote newsletterServiceRemote;
    private ClientItemsServiceRemote clientItemsServiceRemote;
    private ClientGuestServiceRemote clientGuestServiceRemote;
    
    // Singleton Instance
    private static LeilaoRemoteServices instance;
    
    private LeilaoRemoteServices()
    {
        
    }

    public static LeilaoRemoteServices getInstance()
    {
        if (instance == null) {
            instance = new LeilaoRemoteServices();
        }
        
        return instance;
    }

    public ClientUserServiceRemote getClientUserServiceRemote() {
        return clientUserServiceRemote;
    }

    public ClientAdminUserServiceRemote getAdminUserServiceRemote() {
        return adminUserServiceRemote;
    }

    public ClientNewsletterServiceRemote getNewsletterServiceRemote() {
        return newsletterServiceRemote;
    }

    public ClientItemsServiceRemote getClientItemsServiceRemote() {
        return clientItemsServiceRemote;
    }

    public ClientGuestServiceRemote getClientGuestServiceRemote() {
        return clientGuestServiceRemote;
    }
    
    public UserEntity getCurrentUser()
    {
        return getClientUserServiceRemote().getCurrentUser();
    }
    
}
