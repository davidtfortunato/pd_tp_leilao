/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.pd.leilao.client;

import pt.pd.leilao.client.services.LeilaoRemoteServices;
import pt.pd.leilao.client.ui.UIInitialMenu;

/**
 *
 * @author dfortunato
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // init Services
        LeilaoRemoteServices.getInstance().getClientUserServiceRemote();
        
        // Initial Menu
        UIInitialMenu uiMenu = new UIInitialMenu();
        uiMenu.displayMenu();
        
    }
    
}
